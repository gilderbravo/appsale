appangular.controller('reportclosesController', function($scope,$filter, $timeout,reportService) {
    
    $scope.transactions = {list:[],filters:{},listall:[]};
    $scope.payment={list:[]};
    $scope.totalamount=0;
    var dateActualy = new Date();
    var fechaActual = new Date(dateActualy.getFullYear(), dateActualy.getMonth(), dateActualy.getDate());
    

    $scope.transactions.filters.dateinit = $filter('date')(fechaActual,'dd/MM/yyyy');
    $scope.transactions.filters.dateend = $filter('date')(new Date(fechaActual.getFullYear(), fechaActual.getMonth()+1, 0), 'dd/MM/yyyy');

    $scope.setPagina=function(object,filters,page){
        $scope[object][filters].pagina=page;
    }
    $scope.listReportPayments=function($event){
        var $btn;
        if(typeof($event) != 'undefined' ) $btn = $($event.currentTarget).button('loading');        
        reportService.listReportTransactionCloses($scope.transactions.filters).then(function(data){
            if(typeof($event) != 'undefined' ) $btn.button('reset');  
            $scope.transactions.list=data.data.list;
            $scope.totalamount=data.data.totalamount;
            if(data.data.total > 0){
				var params={dom:"#paginador_Table", onclickaccion:'list' ,pagina:data.data.pagina,total:data.data.total, xpagina:data.data.xpagina, accion:null};
				configurar_paginador(params);
			}
        });        
    }


    $scope.exportAll = function(idtable,name,$event){
        var $btn;
        if(typeof($event) != 'undefined' ) $btn = $($event.currentTarget).button('loading');
        $scope.transactions.filters.all=1;
        delete $scope.transactions.filters.pagina;

        reportService.listReportTransactionCloses($scope.transactions.filters).then(function(data){
            if(typeof($event) != 'undefined' ) $btn.button('reset');
            delete $scope.transactions.filters.all;
            $scope.transactions.filters.pagina=1;
            $scope.transactions.listall=data.data;
            $scope.totalamount=data.totalamount;
            $timeout(function(){exportTableToExcel(idtable,name);},1000);

        });

    }
    $scope.preparePrint=function(){

        $("#modal-sale-print").modal("show");

    }
    $scope.prepareDelete=function(data){
        
        alertify.prompt('Confirme:', function (e, str) {
            if (e) {
                    reportService.updateCreditReversal({id:data.id,subtransactionid:data.subtransactionid}).then(function(data){
                        alertify.success('Se Revertió Correctamente');
                        $scope.listReportPayments();
                    },function(reason) {
                        $scope.listReportPayments();
                        alertify.error('Surgio un problema en sistema por favor contacte con soporte...');
                    });
            } 
        }, 'Desea Revertir el Cierre ... ??');
                
    }
    $scope.listReportPayments();
});
