appangular.controller('cashboxController', function($scope,$filter, reportService,$timeout,transactionService) {
    
    $scope.transactions = {list:[],filters:{}};
    $scope.transaction={details:[]};
    $scope.totalMoney = 0; 
    $scope.ticketing=[{money:"0.10", amount:"", total: ""},{money:"0.20", amount:"", total: ""},{money:"0.50", amount:"", total: ""},{money:"1", amount:"", total: ""},{money:"2", amount:"", total: ""},{money:"5", amount:"", total: ""},{money:"10", amount:"", total: ""},{money:"20", amount:"", total: ""},{money:"50", amount:"", total: ""},{money:"100", amount:"", total: ""},{money:"200", amount:"", total: ""}];
    var dateActualy = new Date();
	var fechaActual = new Date(dateActualy.getFullYear(), dateActualy.getMonth(), dateActualy.getDate());	
	
	$scope.transactions.dateinit = $filter('date')(fechaActual,'dd/MM/yyyy');
    $scope.transactions.dateend = $filter('date')(fechaActual, 'dd/MM/yyyy');

    $scope.transactions.filters.dateinit = $filter('date')(new Date(dateActualy.getFullYear(), dateActualy.getMonth()),'dd/MM/yyyy');
    $scope.transactions.filters.dateend = $filter('date')(new Date(dateActualy.getFullYear(), dateActualy.getMonth()+1, 0), 'dd/MM/yyyy');
        
    $scope.newCashbox = function (){
        $scope.transaction={details:[]};
        $scope.transaction.openingdate=$filter('date')(fechaActual,'dd/MM/yyyy');
        $scope.transaction.open=true;
    }
    $scope.calculateMoney = function(){
        for (let i = 0; i < $scope.ticketing.length; i++) {
            if ($scope.ticketing[i].amount != '') {
                $scope.ticketing[i].total=((Number($scope.ticketing[i].amount)*Number($scope.ticketing[i].money*100))/100); 
                $scope.totalMoney = ((Number($scope.totalMoney)*100)/100) + ((Number($scope.ticketing[i].total)*100)/100);
            }else{
                $scope.ticketing[i].total=0;
            }
            
        }
    }
    $scope.loadCashbox=function(data){
        $scope.transaction={details:[]};
        $scope.transaction=data;
        if($scope.transaction.closingdate==null){
            $scope.transaction.closingdate=$filter('date')(fechaActual,'dd/MM/yyyy');
        }
        $scope.transaction.open=false;

        reportService.listCashboxSummary({dateinit:$filter('date')(fechaActual, 'dd/MM/yyyy'),dateend:$filter('date')(fechaActual, 'dd/MM/yyyy'),all:1}).then(function(data){
            $scope.transaction.totalfeeamount=data.data.totalfeeamount;
            $scope.transaction.totalexpenseamount=data.data.totalexpenseamount;
            $scope.transaction.totalpaymentamount=data.data.totalpaymentamount;
            $scope.transaction.totaldebitamount=data.data.totaldebitamount;
            $scope.transaction.totalqualificationamount=data.data.totalqualificationamount;
            
            $scope.transaction.finalamount= ($scope.transaction.totalfeeamount +$scope.transaction.totalpaymentamount+ $scope.transaction.initamount + $scope.transaction.totalqualificationamount)-( $scope.transaction.totalexpenseamount+$scope.transaction.totaldebitamount);
        },function(reason) {
            alertify.error('Surgio un problema en sistema por favor contacte con soporte...');
        });

    }

    $scope.saveCashbox=function($event){
        var $btn;
        if(typeof($event) != 'undefined' ) $btn = $($event.currentTarget).button('loading');
        if($scope.transaction.open){
            var datesplit=$scope.transaction.openingdate.split("/");
            $scope.transaction.openingdate=datesplit[2]+'-'+datesplit[1]+'-'+datesplit[0];
        }else{
            var datesplit=$scope.transaction.closingdate.split("/");
            $scope.transaction.closingdate=datesplit[2]+'-'+datesplit[1]+'-'+datesplit[0];
        }
        $scope.transaction.ticketing=JSON.stringify($scope.ticketing);
       
        transactionService.saveCashbox($scope.transaction).then(function(data){
            if(typeof($event) != 'undefined' ) $btn.button('reset');
           $scope.listCashboxs();
        },function(reason) {
            if(typeof($event) != 'undefined' ) $btn.button('reset');
            alertify.error('Surgio un problema en sistema por favor contacte con soporte...');
        });
    }

    $scope.setPagina=function(object,filters,page){
        $scope[object][filters].pagina=page;
    }
    $scope.listCashboxs=function($event){
        var $btn;
		 if(typeof($event) != 'undefined' ) $btn = $($event.currentTarget).button('loading');
        transactionService.listCashbox($scope.transactions.filters).then(function(data){
            if(typeof($event) != 'undefined' ) $btn.button('reset');  
            $scope.transactions.list=data.data.list;
            if(data.data.total > 0){
				var params={dom:"#paginador_Table", onclickaccion:'list' ,pagina:data.data.pagina,total:data.data.total, xpagina:data.data.xpagina, accion:null};
				configurar_paginador(params);
			}
        },function(reason) {
            if(typeof($event) != 'undefined' ) $btn.button('reset');
            alertify.error('Surgio un problema en sistema por favor contacte con soporte...');
        });      
    }
 
    $scope.listCashboxs();
});
