appangular.controller('feesController', function($scope,$filter, $timeout, feesService,transactionService,subjectService) {
    
    $scope.fees = {list:[],filters:{}};
    $scope.fee = { details:[] };
    $scope.pendingPayment={list:[]};
    $scope.feetransaction={details:[]};
    $scope.payments={list:[]};
    var dateActualy = new Date();
	var fechaActual = new Date(dateActualy.getFullYear(), dateActualy.getMonth(), dateActualy.getDate());
    var lastDay = new Date(dateActualy.getFullYear(), dateActualy.getMonth() + 1, 0);
    var firstDay = new Date(dateActualy.getFullYear(), dateActualy.getMonth(), 1);

	$scope.fees.dateinit = $filter('date')(fechaActual,'dd/MM/yyyy');
    $scope.fees.dateend = $filter('date')(fechaActual, 'dd/MM/yyyy');
    
    $scope.fees.filters.dateinit = $filter('date')(new Date(dateActualy.getFullYear(), dateActualy.getMonth()),'dd/MM/yyyy');
    $scope.fees.filters.dateend = $filter('date')(new Date(dateActualy.getFullYear(), dateActualy.getMonth()+1, 0), 'dd/MM/yyyy');
    
    
    $scope.loadFees=function(item){
        $scope.fee={details:[]};
        $scope.pendingPayment={list:[]};
        $scope.fee.accountant=1;
        $scope.fee.subtransactionid=item.subtransactionid; $scope.fee.dues=1; $scope.fee.interest=0;
        $scope.fee.transactiontypeid=item.transactiontypeid;
        $scope.fee.date = $filter('date')(fechaActual, 'dd/MM/yyyy');
        $scope.fee.clientname=item.clientname; $scope.fee.identitynumber=item.identitynumber;
        $scope.fee.paymentfees=item.paymentfees; $scope.fee.subtransactionstateid=item.subtransactionstateid;
        feesService.listpaymentschedule({subtransactionid:item.subtransactionid,pendingPayment:1 }).then(function(data){
            $scope.pendingPayment.list=data.data.list;
            if($scope.pendingPayment.list.length>0){
                $scope.fee.details.push($scope.pendingPayment.list[0]);
                $scope.calculateTotalFees();       
            }else{
                $scope.fee.details.push({advancement: 0,businesssubjectid:0, capital:(item.total-item.totalpayment),day: "1",deleted_at: null,feeamount:(item.total-item.totalpayment),id: 0,interest: 0,numberdues: 0,status: 0
                    ,subtransactionid:0,time: "26/04/2021"});
                    $scope.pendingPayment.list.push({advancement: 0,businesssubjectid:0, capital:(item.total-item.totalpayment),day: "1",deleted_at: null,feeamount:(item.total-item.totalpayment),id: 0,interest: 0,numberdues: 0,status: 0
                    ,subtransactionid:0,time: "26/04/2021"});
                    $scope.calculateTotalFees();
            }
        });   
        
    }

    $scope.prepareDelete=function(data){
        
        alertify.prompt('Confirme:', function (e, str) {
            if (e) {
                    feesService.removepayment({subtransactionid:data.subtransactionid,pendingPayment:1 }).then(function(data){
                        alertify.success('Se elimino Correctamente');
                    },function(reason) {
                        alertify.error('Surgio un problema en sistema por favor contacte con soporte...');
                    });
            } 
        }, 'Desea Eliminar el Pago ... ??');
                
    }

    $scope.loadPayments=function(data){
        $scope.payments={list:[]};
        feesService.listpaymentschedule({id:data.referencespayment }).then(function(data){            
                $scope.payments.list=data.data.list;
                $scope.payments.feeamount = $scope.payments.list.reduce((sum, payment) => {return sum + (payment.advancement>0?payment.advancement:payment.feeamount);},0);
            
        });

    }
    // 50 42.5 42.3
    // 
    $scope.addDuestoFee=function(totalpay){
        $scope.fee.details=[];
        let pendingPayment = angular.copy($scope.pendingPayment.list);
        let totalpaytemp=totalpay;
        for (let index = 0; index < pendingPayment.length ; index++) {
            
            if(Number(pendingPayment[index].feeamount)==Number(totalpaytemp)){
                pendingPayment[index].paymentcanceled=1;
                $scope.fee.details.push(pendingPayment[index]);
                break;
            }else if (Number(totalpaytemp) > Number(pendingPayment[index].feeamount) ) {
                totalpaytemp=Number(totalpaytemp) - Number(pendingPayment[index].feeamount);      
                pendingPayment[index].paymentcanceled=1;          
                $scope.fee.details.push(pendingPayment[index]);
            }else{               
                pendingPayment[index].feeamount=Number(totalpaytemp);
                pendingPayment[index].advancement=Number( pendingPayment[index].advancement) + Number(totalpaytemp);
                pendingPayment[index].paymentcanceled=0;
                $scope.fee.details.push(pendingPayment[index]);
                break;
            }
            
        }
        $scope.calculateTotalFees();
    }
    $scope.saveFees=function(){
        var datesplit=$scope.fee.date.split("/");
        $scope.fee.date=datesplit[2]+'-'+datesplit[1]+'-'+datesplit[0];

        let feeamount = $scope.fee.details[0].feeamount;
        $scope.fee.details = $scope.fee.details.filter(e=> e.feeamount==feeamount);
        $scope.fee.referencespayment= ($scope.fee.details.map(det=>det.id)).toString();
        feesService.saveFee($scope.fee).then(function(data){
            $scope.listFees();
            alertify.success("Se Guardo Correctamente");
            $("#modal-fee-print").modal("show");
        },function(reason) {
            if(typeof($event) != 'undefined' ) $btn.button('reset');
            alertify.error('Surgio un problema en sistema por favor contacte con soporte...');
        });
    }

    $scope.setPagina=function(object,filters,page){
        $scope[object][filters].pagina=page;
    }
    $scope.listFees=function($event){
        var $btn;
         if(typeof($event) != 'undefined' ) $btn = $($event.currentTarget).button('loading');
         $scope.fees.filters.transactiontypeid=1;
        feesService.listFeesClient($scope.fees.filters).then(function(data){
            if(typeof($event) != 'undefined' ) $btn.button('reset');  
            $scope.fees.list=data.data.list;
            if(data.data.total > 0){
				var params={dom:"#paginador_Table", onclickaccion:'list' ,pagina:data.data.pagina,total:data.data.total, xpagina:data.data.xpagina, accion:null};
				configurar_paginador(params);
			}
        });        
    }
    $scope.calculateTotalFees=function(){
        $scope.fee.total=0;
		var details = $scope.fee.details;
        var totalreal = 0;
        for(i=0;i<details.length;i++){
            totalreal += Number(details[i].feeamount);
        }
        $scope.fee.total = totalreal+Number($scope.fee.interest);
        $scope.fee.totalneto=totalreal;
    }
    
    $scope.removeDetail=function(index){
		var detail=$scope.fee.details[index];
		$scope.fee.details.splice(index,1);	
	     $scope.calculateTotalFees();
    }
    $scope.onPaymentClick= function(item){
        const flag =  $scope.fee.details.filter(items => items.id === item.id);
        if (flag.length>0){
            alertify.error("El Pago de letra ya existe en el detalle, por favor selecciona otro pago (Duplicado) ...")
        }else {
            $scope.addPäymentDetails(item);
        }

    }
    $scope.addPäymentDetails= function(item){
		var payment={};
        payment=angular.copy(item);

        $scope.fee.details.push({id:payment.id,time:payment.time,businesssubjectid:payment.businesssubjectid,  subtransactionid:payment.subtransactionid, capital:payment.capital,interest:payment.interest,feeamount:payment.feeamount,day:payment.day,status:payment.status, deleted_at:payment.deleted_at,numberdues:payment.numberdues});
                
		$scope.calculateTotalFees();
    }
    $scope.listFees();
});
