appangular.controller('categoryObjectsController', function($scope, objectService) {
    
    $scope.category = {};
    $scope.categories = {list: []};

    $scope.newCategoryObjects = function (){
        $scope.category = {};
    }
    $scope.loadCategoryObjects = function (item){
        $scope.category = item;
    }
    $scope.listcategory = function (){
        objectService.listcategory({}).then(function(data){
            $scope.categories.list=data.data;
        }); 
    }
    $scope.saveCategoryObjects=function(){
        objectService.saveCategoryObjects($scope.category).then(function(data){
            $scope.listcategory();
        }); 
    }
    $scope.listcategory();
    
});
