appangular.controller('balancesheetController', function($scope,$filter, $timeout,reportService,transactionService ) {
    
    $scope.transactions = {list:[],filters:{},listall:[]};
    $scope.transaction={details:[]};
    $scope.payment={list:[]};
    $scope.totalfeeamount=0;
    $scope.totaljuntamasfeeamount=0;
    $scope.totalcanastafeeamount=0;
    $scope.totalcreditfeeamount=0;
    $scope.totalotrosamount=0;
    $scope.totaldevamount=0;
    $scope.totaldesamount=0;
    $scope.finalamount = 0;
    $scope.transactioncloseamount = 0;
    $scope.totalotrosegresosnamount=0;
    var dateActualy = new Date();
    var fechaActual = new Date(dateActualy.getFullYear(), dateActualy.getMonth(), dateActualy.getDate());
    	


    $scope.transactions.filters.dateinit = $filter('date')(fechaActual,'dd/MM/yyyy');
    $scope.transactions.filters.dateend = $filter('date')(new Date(fechaActual.getFullYear(), fechaActual.getMonth()+1, 0), 'dd/MM/yyyy');


    $scope.loadSale=function(data){
        $scope.transaction={details:[]};
        $scope.transaction=data;
        transactionService.listSubtransactiondetails($scope.transaction).then(function(data){
            $scope.transaction.details=data.data;
        });
        $timeout(function(){$("#cmbCliente").val($scope.transaction.clientid).change();},0);

    }

    $scope.setPagina=function(object,filters,page){
        $scope[object][filters].pagina=page;
    }
    $scope.listReportPayments=function($event){
        var $btn;
        if(typeof($event) != 'undefined' ) $btn = $($event.currentTarget).button('loading');
       
        reportService.listReportpayments($scope.transactions.filters).then(function(data){
            if(typeof($event) != 'undefined' ) $btn.button('reset');  
            $scope.transactions.list=data.data.list;
            $scope.totalfeeamount=data.data.totalfeeamount;
            if(data.data.total > 0){
				var params={dom:"#paginador_Table", onclickaccion:'list' ,pagina:data.data.pagina,total:data.data.total, xpagina:data.data.xpagina, accion:null};
				configurar_paginador(params);
			}
        });        
    }


    $scope.exportAll = function(idtable,name,$event){
        var $btn;
        if(typeof($event) != 'undefined' ) $btn = $($event.currentTarget).button('loading');
        $scope.transactions.filters.all=1;
        delete $scope.transactions.filters.pagina;
        $scope.transactions.filters.season='JuntaMas';
        reportService.listReportpayments($scope.transactions.filters).then(function(data){
            $scope.transactions.juntamas=data.data;
            $scope.totaljuntamasfeeamount= data.data.reduce((acc, d) => acc + d.cash, 0);
            $scope.transactions.filters.season='Canastas';
            reportService.listReportpayments($scope.transactions.filters).then(function(data1){
                $scope.transactions.canasta=data1.data;
                $scope.totalcanastafeeamount = data1.data.reduce((acc, d) => acc + d.cash, 0);
                $scope.transactions.filters.season='';
                    reportService.listReportpayments($scope.transactions.filters).then(function(data2){
                        
                        $scope.transactions.listall=data2.data;
                        $scope.totalcreditfeeamount = data2.data.reduce((acc, d) => acc + d.cash, 0);
                        //-----------------------
                        $scope.transactions.filters.transactiontypeid=6;
                        transactionService.listExpenseAndPayment($scope.transactions.filters).then(function(data3){
                            
                            $scope.transactions.listOtros=data3.data;
                            $scope.totalotrosamount=data3.data.reduce((acc, d) => acc + d.total, 0);
                            $scope.transactions.filters.transactiontypeid=5;
                            transactionService.listExpenseAndPayment($scope.transactions.filters).then(function(data4){
                                
                               
                                $scope.transactions.listdevolucion= data4.data.filter(f => f.commenttransaction === "Salida JuntaMas"); 
                                $scope.transactions.listdesembolso= data4.data.filter(f => f.commenttransaction === "Salida Desembolso"); 
                                $scope.transactions.listegresosotros= data4.data.filter(f => f.commenttransaction === "Otros"); 

                                $scope.totaldevamount=$scope.transactions.listdevolucion.reduce((acc, d) => acc + d.total, 0);
                                $scope.totaldesamount=$scope.transactions.listdesembolso.reduce((acc, d) => acc + d.total, 0);
                                $scope.totalotrosegresosnamount=$scope.transactions.listegresosotros.reduce((acc, d) => acc + d.total, 0);
                                reportService.listCashboxSummary($scope.transactions.filters).then(function(data5){
                                    if(typeof($event) != 'undefined' ) $btn.button('reset');
                                    delete $scope.transactions.filters.all;
                                    $scope.transactions.filters.pagina=1;
                                    $scope.transactions.totalfeeamountcash=data5.data.totalfeeamount;
                                    $scope.transactions.totalexpenseamount=data5.data.totalexpenseamount;
                                    $scope.transactions.totalpaymentamount=data5.data.totalpaymentamount;
                                    $scope.transactions.totaldebitamount=data5.data.totaldebitamount;
                                    $scope.transactions.totalqualificationamount=data5.data.totalqualificationamount;
                                    $scope.transactions.initamount=data5.data.initamount;
                                    $scope.transactions.money = JSON.parse(data5.data.money);                                    
                                    $scope.transactions.summoney = $scope.transactions.money!=null?$scope.transactions.money.reduce((acc, d) => acc + d.total*1, 0):0;
                                    $scope.finalamount=($scope.totalotrosamount+$scope.totalcanastafeeamount + $scope.totaljuntamasfeeamount + $scope.totalcreditfeeamount) - ($scope.totaldevamount+$scope.totaldesamount+$scope.totalotrosegresosnamount);
                                    $scope.finalamount=  $scope.finalamount + ( $scope.transactions.initamount + $scope.transactions.totalqualificationamount)-($scope.transactions.totaldebitamount);
                                    $scope.transactions.filters.all=1;
                                    delete $scope.transactions.filters.pagina;
                                    reportService.listReportTransactionCloses($scope.transactions.filters).then(function(data){
                                        $scope.transactions.transactioncloses=data.data.list;
                                        $scope.transactioncloseamount= data.data.totalamount;
                                        $timeout(function(){exportTableToExcel(idtable,name);},5000);
                                    });                                    
                                    
                                });
                                
                            });  
                        });                      
                        
                    });                    
            });

        });

    }
    $scope.preparePrint=function(){

        $("#modal-sale-print").modal("show");

    }
    $scope.prepareDelete=function(data){
        
        alertify.prompt('Confirme:', function (e, str) {
            if (e) {
                    reportService.deletefee({feeid:data.feeid,referencespayment:data.referencespayment}).then(function(data){
                        alertify.success('Se elimino Correctamente');
                        $scope.listReportPayments();
                    },function(reason) {
                        $scope.listReportPayments();
                        alertify.error('Surgio un problema en sistema por favor contacte con soporte...');
                    });
            } 
        }, 'Desea Eliminar el Pago ... ??');
                
    }
    $scope.listReportPayments();
});
