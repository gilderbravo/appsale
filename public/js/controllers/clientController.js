appangular.controller('clientController', function($scope,$timeout, subjectService,Alertify,$filter) {
    
    $scope.clients = {list:[],filters:{}};    
    $scope.client = {};  
    $scope.identityDocuments ={list:[]};

    var dateActualy = new Date();
    var fechaActual = new Date(dateActualy.getFullYear(), dateActualy.getMonth(), dateActualy.getDate());

    $scope.clients.filters.dateinit = $filter('date')(fechaActual,'dd/MM/yyyy');
    $scope.clients.filters.dateend = $filter('date')(fechaActual, 'dd/MM/yyyy');

    $scope.newClient = function (){
        $scope.client = {};
        $timeout(function(){$("#cmbIdentityDocument").val(1).change()},20);	
    }
    $scope.validateTypeIdentity=function(item){
        if(item==1){
            $("#lblName").text('Nombre'+":");
			$("#lblfirstname").text('Apellido'+":");
            document.getElementById("txtIdentityNumber").maxLength = "8";
        }else{
            $("#lblName").text('Nombre Comercial'+":");
            $("#lblfirstname").text('Razon Social'+":");            
            document.getElementById("txtIdentityNumber").maxLength = "11";
        }
    }

    $scope.loadClient = function (item){
        $scope.client = item;
        $timeout(function(){$("#cmbIdentityDocument").val($scope.client.identitydocumentid).change()},20);	
    }
    $scope.searchDNIRUC= function(number,type){
        subjectService.searchdniruc({number:number,type:type}).then(function(data) {
            // if (data.data.length > 0){
                $scope.client.name = data.data.firstname;
                $scope.client.firstname = data.data.secondname ;
                $scope.client.address = data.data.address ;
            // }else{alertify.error("no se pudo consultar...");}
        });
    }
    $scope.saveClient=function($event){
        var $btn;
        if(typeof($event) != 'undefined' ) $btn = $($event.currentTarget).button('loading');
        var validate=true;
        validate=validate_field(['#cmbIdentityDocument','#txtFirstname','#txtIdentityNumber','#txtname']);
        if(validate){
            subjectService.saveClient($scope.client).then(function(data){
                if(typeof($event) != 'undefined' ) $btn.button('reset');
                $('#modal-client').modal('hide');
                $scope.listClient();
                alertify.success("Transaccion Realizado Correctamente...");
            },function(reason) {
                if(typeof($event) != 'undefined' ) $btn.button('reset');
                alertify.error('Surgio un problema en sistema por favor contacte con soporte...');
            });      
        }else{
            alertify.error("Campos Obligatorios");
        }
        
    }
    $scope.setPagina=function(object,filters,page){
        $scope[object][filters].pagina=page;
    }
    $scope.listClient=function($event){
        var $btn;
        if(typeof($event) != 'undefined' ) $btn = $($event.currentTarget).button('loading');
        subjectService.listClient($scope.clients.filters).then(function(data){
            if(typeof($event) != 'undefined' ) $btn.button('reset');
            $scope.clients.list=data.data.list;
            if(data.data.total > 0){
                var params={dom:"#paginador_Table", onclickaccion:'list' ,pagina:data.data.pagina,total:data.data.total, xpagina:data.data.xpagina, accion:null};
                configurar_paginador(params);
            }
        });        
    }
    $scope.listIdentityDocument=function(){
        subjectService.listIdentityDocument({}).then(function(data){
            $scope.identityDocuments.list=data.data;
        });
    }
    $scope.listClient();
    $scope.listIdentityDocument();
});
