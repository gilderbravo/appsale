appangular.controller('propertyObjectsController', function($scope, objectService) {
    
    $scope.property = {};

    $scope.properties = {list: []};

    $scope.newPropertyObjects = function (){
        $scope.property = {};
    }
    $scope.loadPropertyObjects = function (item){
        $scope.property = item;
    }
    $scope.listproperty = function (){
        objectService.listproperty({}).then(function(data){
            $scope.properties.list=data.data;
        }); 
    }
    $scope.savePropertyObjects = function (){
        objectService.savePropertyObjects($scope.property).then(function(data){
            $scope.listproperty();
        }); 
    }
    $scope.listproperty();
});
