appangular.controller('objectController', function($scope, objectService,Alertify) {
    
    $scope.listbrandobject = {list:[],filters:{}};
    $scope.brand={};
    $scope.object={};
    $scope.presentations = {list: []};
    $scope.objectModels = {list: []};
    $scope.objects={list:[],filters:{}};
    $scope.categories = {list: []};

    $scope.listpresentation = function (){
        objectService.listpresentation({}).then(function(data){
            $scope.presentations.list=data.data;
        }); 
    }
    $scope.listObjectModel = function (){
        objectService.listObjectModel({}).then(function(data){
            $scope.objectModels.list=data.data;
        }); 
    }
    $scope.listcategory = function (){
        objectService.listcategory({}).then(function(data){
            $scope.categories.list=data.data;
        }); 
    }
    $scope.newObject=function(){
        $scope.object={};     
    }
    $scope.loadObject=function(data){
        $scope.object=data;
    }
    $scope.listObject=function(){
        objectService.listObject({}).then(function(data){
            $scope.objects.list=data.data;
        });        
    }

    $scope.saveObject=function(){
        objectService.saveObject($scope.object).then(function(data){
            $scope.listObject();
        });        
    }
    $scope.allBrandObject = function() {   
        objectService.allBrandObject().then(function(data){            
            $scope.listbrandobject.list = data.data;
        });
    } 
    $scope.setPagina=function(variable,page){
		$scope.listbrandobject[variable].pagina=page;
    }
    
    $scope.newBrandObject=function(){
        $scope.brand={};     
    }
    $scope.loadBrandObject=function(data){
        $scope.brand=data;
    }
    $scope.saveBrandObject=function(){        
        $scope.listbrandobject.list
        var encontrado=true;
        for(var i=0;i<$scope.listbrandobject.list.length;i++){
            if($scope.brand.name==$scope.listbrandobject.list[i].name){
                encontrado=false;
            }
        }
        if(encontrado){
            alertify.success("no existe");
            // objectService.saveBrandObject($scope.brand).then(function(){                        
            //     $('#modal-brandObject').modal('hide');
            //     $scope.allBrandObject();                
            // });            
        }else{
            alertify.error("si existe");

        }
    }
    $scope.deleteBrandObject=function(){
        objectService.deleteBrandObject({id:$scope.brand.id}).then(function(){
            $scope.allBrandObject();
        }); 
    }
    $scope.allBrandObject();
    $scope.listpresentation();
    $scope.listObject();   
    $scope.listObjectModel(); 
    $scope.listcategory();
    
});
