appangular.controller('savingmoneyController', function($scope,$filter, $timeout, feesService,transactionService,objectService,subjectService) {
    
    $scope.transactions = {list:[],filters:{transactiontypeid:3},listall:{}};
    $scope.objects = {list:[],filters:{}};
    $scope.clients = {list:[]};
    $scope.employeeds={list:[]};
    $scope.transaction={details:[],transactiontypeid:3};
    $scope.payment={list:[]};
    var dateActualy = new Date();
	var fechaActual = new Date(dateActualy.getFullYear(), dateActualy.getMonth(), dateActualy.getDate());	
	
	$scope.transactions.dateinit = $filter('date')(fechaActual,'dd/MM/yyyy');
    $scope.transactions.dateend = $filter('date')(fechaActual, 'dd/MM/yyyy');

    $scope.transactions.filters.dateinit = $filter('date')(new Date(dateActualy.getFullYear(), dateActualy.getMonth()),'dd/MM/yyyy');
    $scope.transactions.filters.dateend = $filter('date')(new Date(dateActualy.getFullYear(), dateActualy.getMonth()+1, 0), 'dd/MM/yyyy');
    
    $scope.newSale = function (){
        $scope.transaction={details:[],transactiontypeid:3};
        $scope.transaction.date=$filter('date')(fechaActual,'dd/MM/yyyy');

        $scope.payment={list:[]};
        $scope.payment.date=$scope.transaction.date;

    }
    $scope.changeAmount=function(){
        if ($scope.transaction.details.length>0)delete $scope.transaction.details;$scope.transaction.details=[];
        $scope.transaction.details.push({amount:1,unitmeasure:1,product:'Prestamo Monetario',stock:0,objectid:1,valuedobjectid:1,unitpricesale:$scope.payment.amount,unitpricereal:$scope.payment.amount,totalmoney:$scope.payment.amount,subtransactionid:0,id:0});
        $scope.calculateTotal();
    }
    $scope.loadSale=function(data){
        $scope.transaction={details:[],transactiontypeid:3};
        $scope.transaction=data;
        transactionService.listSubtransactiondetails($scope.transaction).then(function(data){
            $scope.transaction.details=data.data;
        });  
        $timeout(function(){$("#cmbCliente").val($scope.transaction.clientid).change();},0);	
        
    }

    $scope.saveSale=function($event){
        var $btn;
        if(typeof($event) != 'undefined' ) $btn = $($event.currentTarget).button('loading');
        var datesplit=$scope.transaction.date.split("/");
        $scope.transaction.date=datesplit[2]+'-'+datesplit[1]+'-'+datesplit[0];
        $scope.transaction.comment="viewCredit";
        $scope.transaction.comment="viewCredit";        
        transactionService.saveSale($scope.transaction).then(function(data){
            $scope.payment.subtransactionstateid=data.data.subtransactionstateid;
            $scope.payment.subtransactionid=data.data.subtransactionid;
            $scope.payment.businesssubjectid=$scope.transaction.clientid;
            if(typeof($event) != 'undefined' ) $btn.button('reset');
            $scope.savePaymentSchedules($event);
        },function(reason) {
            if(typeof($event) != 'undefined' ) $btn.button('reset');
            alertify.error('Surgio un problema en sistema por favor contacte con soporte...');
        });
    }

    $scope.setPagina=function(object,filters,page){
        $scope[object][filters].pagina=page;
    }
    $scope.listSales=function($event){
        var $btn;
		 if(typeof($event) != 'undefined' ) $btn = $($event.currentTarget).button('loading');
        $scope.transactions.filters.comment='viewCredit';
        transactionService.listSale($scope.transactions.filters).then(function(data){
            if(typeof($event) != 'undefined' ) $btn.button('reset');  
            $scope.transactions.list=data.data.list;
            if(data.data.total > 0){
				var params={dom:"#paginador_Table", onclickaccion:'list' ,pagina:data.data.pagina,total:data.data.total, xpagina:data.data.xpagina, accion:null};
				configurar_paginador(params);
			}
        });        
    }
    $scope.exportAll = function(idtable,name,$event){
        var $btn;
        if(typeof($event) != 'undefined' ) $btn = $($event.currentTarget).button('loading');
        $scope.transactions.filters.all=1;
        delete $scope.transactions.filters.pagina;
        $scope.transactions.filters.comment='viewCredit';
        transactionService.listSale($scope.transactions.filters).then(function(data){
            if(typeof($event) != 'undefined' ) $btn.button('reset');
            delete $scope.transactions.filters.all;
            $scope.transactions.filters.pagina=1;
            $scope.transactions.listall=data.data;
            $timeout(function(){exportTableToExcel(idtable,name);},1000);

        });

    }

    $scope.listObjectSale=function(){
        objectService.listObjectSale({}).then(function(data){
            $scope.objects.list=data.data;
        });        
    }
    // $scope.listClient=function(){
    //     subjectService.listClient({all:1}).then(function(data){
    //         $scope.clients.list=data.data;
    //     });        
    // }
    $scope.prepareDelete=function(data){
        
        alertify.prompt('Confirme:', function (e, str) {
            if (e) {
                    transactionService.deletesale({subtransactionstateid:data.subtransactionstateid}).then(function(data){
                        alertify.success('Se elimino Correctamente');
                        $scope.listSales();
                    },function(reason) {
                        $scope.listSales();
                        alertify.error('Surgio un problema en sistema por favor contacte con soporte...');
                    });
            } 
        }, 'Desea Eliminar el Pago ... ??');
                
    }
    $scope.prepareChange=function(data){
        var datesplit=$scope.transactions.dateinit.split("/");
        alertify.prompt('Confirme:', function (e, str) {
            if (e) {
                    transactionService.changestatusdisbursement({datedelivered:(datesplit[2]+'-'+datesplit[1]+'-'+datesplit[0]),subtransactionid:data.subtransactionid,subtransactionstateid:data.subtransactionstateid}).then(function(data){
                        alertify.success('Se elimino Correctamente');
                        $scope.listSales();
                    },function(reason) {
                        $scope.listSales();
                        alertify.error('Surgio un problema en sistema por favor contacte con soporte...');
                    });
            } 
        }, 'Desea Cambiar a Desembolso... ??');
                
    }
    $scope.onProductClick=function(item){             		
		$scope.addProductToDetails(item);
    }
    $scope.calculateTotal=function(){
		
		$scope.transaction.neto=0; $scope.transaction.total=0;
		var netotemp=0;
		var details = $scope.transaction.details;
        var totalreal = 0;
        for(i=0;i<details.length;i++){		
                details[i].totalmoney= parseFloat( details[i].amount * details[i].unitpricesale );
                totalreal += details[i].amount * details[i].unitpricesale;
                netotemp +=details[i].totalmoney; 					
            }

        $scope.transaction.neto = Math.round(parseFloat(netotemp / (1) )*1000)/1000;
//				sea aumento por caso del redondeo de valores 
        $scope.transaction.netotemp=Math.round(parseFloat(netotemp / (1 ) )*100)/100
//				finaliza				
        $scope.transaction.igv=$scope.transaction.netotemp*0.0;
        
        $scope.transaction.total=Math.round(($scope.transaction.neto+$scope.transaction.igv)*100)/100;
				
	}
    $scope.removeDetail=function(index){
		var detail=$scope.transaction.details[index];
		if(detail>0){
			TransactionService.removeDetail({subtransactiondetailid:detail.id});
		}
		$scope.transaction.details.splice(index,1);	
		$scope.calculateTotal();
    }
    $scope.addProductToDetails= function(item){
		var encontrado = false;
		var product={};
		product=angular.copy(item);	
        product.unitpricereal = product.unitpricesale;
        $scope.transaction.details.push({amount:1,unitmeasure:product.unitmeasure,product:product.nameconcat,stock:product.stock,objectid:product.objectid,valuedobjectid:product.valuedobjectid,unitpricesale:product.unitpricesale,unitpricereal:product.unitpricereal,totalmoney:product.unitpricesale,subtransactionid:0,id:0});
                
		$scope.calculateTotal();
    }

    $scope.loadPayment=function(data){
        $scope.payment={list:[]};
        $scope.payment.amount=data.total;
        $scope.payment.subtransactionid=data.subtransactionid;
        $scope.payment.businesssubjectid=data.clientid;
        $scope.payment.subtransactionstateid=data.subtransactionstateid;
        $scope.payment.clientname=data.client;
        $scope.payment.address=data.address;
        $scope.payment.secundaryemployed=data.secundaryemployed;
        $scope.payment.identitynumber=data.identitynumber;
        $scope.payment.code=data.code;
        $scope.payment.phone=data.phone;
        $scope.payment.fees=data.paymentfees;
        $scope.payment.tasa=data.interestrate;
        $scope.payment.frecuencyday=data.frecuencyday;
        $scope.payment.season=data.season;
        $scope.payment.date=$filter('date')(data.date,'dd/MM/yyyy');
        feesService.listpaymentschedule({subtransactionid:data.subtransactionid}).then(function(data){
            $scope.payment.list=data.data.list;
        });
    }

	$scope.generatePayments=function(){
        // var date=new Date();
        var datesplit=$scope.payment.date.split("/");
        var date=new Date(datesplit[2]+'-'+datesplit[1]+'-'+datesplit[0]);
        $scope.payment.list=[];

		var total=$scope.payment.amount*($scope.payment.tasa/100)+$scope.payment.amount*1;
		var capital=$scope.payment.amount/$scope.payment.fees;
		var interes=($scope.payment.amount*($scope.payment.tasa/100))/$scope.payment.fees;
		var capitalExcedente =total-$filter('number')(capital+interes,1)*$scope.payment.fees;
		
		for(var i=0;i<$scope.payment.fees*1;i++){
			date=$scope.addDate(date,$scope.payment.frecuencyday*1);
            var data={ time:$filter('date')(date, "yyyy-MM-dd"),capital:$filter('number')(capital,1),
                        interest:$filter('number')(interes,1),gastos:0,
                        feeamount:$filter('number')(capital+interes+capitalExcedente,1),day:1};
                data.capital=data.capital.replace(/,/g, "");
                data.interest=data.interest.replace(/,/g, "");
                data.feeamount=data.feeamount.replace(/,/g, "");
			$scope.payment.list.push(data);
			if(i==0){
				capitalExcedente=0;
			}
		}
	}
    $scope.addDate=function(date,days){
        date.setDate(date.getDate() + days);
        if(date.getDay()==0){
            date.setDate(date.getDate() + 1);
        }
        return date;
    }
    $scope.preparePrint=function(){
        $("#modal-payment").modal("hide");
        $("#modal-sale-print").modal("show");

    }
    $scope.savePaymentSchedules=function($event){
        var $btn;
        if(typeof($event) != 'undefined' ) $btn = $($event.currentTarget).button('loading');
        var datesplit=$scope.payment.date.split("/");
        $scope.payment.date=datesplit[2]+'-'+datesplit[1]+'-'+datesplit[0];
        for (let index = 0; index < $scope.payment.list.length; index++) {
            $scope.payment.list[index].capital=$scope.payment.list[index].capital.replace(/,/g, "");
            $scope.payment.list[index].feeamount=0;
        }
        transactionService.savePaymentSchedule($scope.payment).then(function(data){
            if(typeof($event) != 'undefined' ) $btn.button('reset');
            $scope.listSales();
            alertify.success("Se Gernero El cronograma Correctamente");
            $("#modal-sale-print").modal("show");
        },function(reason) {
            if(typeof($event) != 'undefined' ) $btn.button('reset');
            alertify.error('Surgio un problema en sistema por favor contacte con soporte...');
        });      
    }

    $scope.getclientid=function(value)
    {
        $scope.transaction.clientid=parseInt(value);
    }

    $scope.listEmployeed=function($event){
        subjectService.listEmployeed({all:1}).then(function(data){
            $scope.employeeds.list=data.data;
        });        
    }
    $scope.listEmployeed();
   
    $scope.listObjectSale();
    // $scope.listClient();
    $scope.listSales();
});
