appangular.controller('objectModelsController', function($scope, objectService) {
    
    $scope.objectModel = {};
    $scope.objectModels = {list: []};
    $scope.brandobjects={list: []};
    $scope.properties = {list: []};

    $scope.newObjectModel = function (){
        $scope.objectModel = {};
    }
    $scope.loadObjectModel = function (item){
        $scope.objectModel = item;
    }
    $scope.listObjectModel = function (){
        objectService.listObjectModel({}).then(function(data){
            $scope.objectModels.list=data.data;
        }); 
    }
    $scope.saveObjectModel=function(){
        objectService.saveObjectModel($scope.objectModel).then(function(data){
            $scope.listObjectModel();
        });         
    }
    $scope.allBrandObject = function() {   
        objectService.allBrandObject({}).then(function(data){            
            $scope.brandobjects.list = data.data;
            
        });
    } 
    $scope.listproperty = function (){
        objectService.listproperty({}).then(function(data){
            $scope.properties.list=data.data;
        }); 
    }
    $scope.listObjectModel();
    $scope.allBrandObject();
    $scope.listproperty();
    
});
