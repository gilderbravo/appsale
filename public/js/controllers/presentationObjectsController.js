appangular.controller('presentationObjectsController', function($scope, objectService) {
    
    $scope.presentation = {};
    $scope.presentations = {list: []};

    $scope.newPresentationObjects = function (){
        $scope.presentation = {};
    }
    $scope.loadPresentationObjects = function (item){
        $scope.presentation = item;
    }
    $scope.listpresentation = function (){
        objectService.listpresentation({}).then(function(data){
            $scope.presentations.list=data.data;
        }); 
    }
    $scope.savePresentationObjects = function (){
        objectService.savePresentationObjects($scope.presentation).then(function(data){
            $scope.listpresentation();
        });     
    }
    

    $scope.listpresentation();
    
    
});
