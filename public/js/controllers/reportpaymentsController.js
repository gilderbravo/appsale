appangular.controller('reportpaymentsController', function($scope,$filter, $timeout,reportService) {
    
    $scope.transactions = {list:[],filters:{},listall:[]};
    $scope.transaction={details:[]};
    $scope.payment={list:[]};
    $scope.totalfeeamount=0;
    var dateActualy = new Date();
    var fechaActual = new Date(dateActualy.getFullYear(), dateActualy.getMonth(), dateActualy.getDate());
    	


    $scope.transactions.filters.dateinit = $filter('date')(fechaActual,'dd/MM/yyyy');
    $scope.transactions.filters.dateend = $filter('date')(new Date(fechaActual.getFullYear(), fechaActual.getMonth()+1, 0), 'dd/MM/yyyy');


    $scope.loadSale=function(data){
        $scope.transaction={details:[]};
        $scope.transaction=data;
        transactionService.listSubtransactiondetails($scope.transaction).then(function(data){
            $scope.transaction.details=data.data;
        });
        $timeout(function(){$("#cmbCliente").val($scope.transaction.clientid).change();},0);

    }

    $scope.setPagina=function(object,filters,page){
        $scope[object][filters].pagina=page;
    }
    $scope.listReportPayments=function($event){
        var $btn;
        if(typeof($event) != 'undefined' ) $btn = $($event.currentTarget).button('loading');

        reportService.listReportpayments($scope.transactions.filters).then(function(data){
            if(typeof($event) != 'undefined' ) $btn.button('reset');  
            $scope.transactions.list=data.data.list;
            $scope.totalfeeamount=data.data.totalfeeamount;
            if(data.data.total > 0){
				var params={dom:"#paginador_Table", onclickaccion:'list' ,pagina:data.data.pagina,total:data.data.total, xpagina:data.data.xpagina, accion:null};
				configurar_paginador(params);
			}
        });        
    }


    $scope.exportAll = function(idtable,name,$event){
        var $btn;
        if(typeof($event) != 'undefined' ) $btn = $($event.currentTarget).button('loading');
        $scope.transactions.filters.all=1;
        delete $scope.transactions.filters.pagina;

        reportService.listReportpayments($scope.transactions.filters).then(function(data){
            if(typeof($event) != 'undefined' ) $btn.button('reset');
            delete $scope.transactions.filters.all;
            $scope.transactions.filters.pagina=1;
            $scope.transactions.listall=data.data;
            $scope.totalfeeamount=data.totalfeeamount;
            $timeout(function(){exportTableToExcel(idtable,name);},1000);

        });

    }
    $scope.preparePrint=function(){

        $("#modal-sale-print").modal("show");

    }
    $scope.prepareDelete=function(data){
        
        alertify.prompt('Confirme:', function (e, str) {
            if (e) {
                    reportService.deletefee({feeid:data.feeid,referencespayment:data.referencespayment}).then(function(data){
                        alertify.success('Se elimino Correctamente');
                        $scope.listReportPayments();
                    },function(reason) {
                        $scope.listReportPayments();
                        alertify.error('Surgio un problema en sistema por favor contacte con soporte...');
                    });
            } 
        }, 'Desea Eliminar el Pago ... ??');
                
    }
    $scope.listReportPayments();
});
