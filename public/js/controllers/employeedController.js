appangular.controller('employeedController', function($scope,$timeout, subjectService,$filter) {
    
    $scope.transactions = {list:[],filters:{}};
    $scope.transaction = {};
    $scope.identityDocuments ={list:[]};
    $scope.position ={list:[]};
    $scope.user ={};

    var dateActualy = new Date();
    var fechaActual = new Date(dateActualy.getFullYear(), dateActualy.getMonth(), dateActualy.getDate());

    $scope.transactions.filters.dateinit = $filter('date')(fechaActual,'dd/MM/yyyy');
    $scope.transactions.filters.dateend = $filter('date')(fechaActual, 'dd/MM/yyyy');

    $scope.newEmployeed = function (){
        $scope.transaction = {};
        $timeout(function(){$("#cmbIdentityDocument").val(1).change()},20);
        $timeout(function(){$("#cmbRol").val(1).change()},20);

    }
    $scope.validateTypeIdentity=function(item){
        if(item==1){
            $("#lblName").text('Nombre'+":");
			$("#lblfirstname").text('Apellido'+":");
            document.getElementById("txtIdentityNumber").maxLength = "8";
        }else{
            $("#lblName").text('Nombre Comercial'+":");
            $("#lblfirstname").text('Razon Social'+":");            
            document.getElementById("txtIdentityNumber").maxLength = "11";
        }
    }

    $scope.loadEmployeed = function (item){
        $scope.transaction = item;
        $timeout(function(){$("#cmbIdentityDocument").val($scope.transaction.identitydocumentid).change()},20);
        $timeout(function(){$("#cmbRol").val($scope.transaction.positionid).change()},20);
    }
    $scope.loadEmployeedDelete = function (item){
        
        alertify.prompt('Confirme:', function (e, str) {
            if (e) {
                subjectService.updateUser({employedid:item.employedid}).then(function(data){
                        alertify.success('Se elimino Correctamente');
                        $scope.list();
                    },function(reason) {
                        alertify.error('Surgio un problema en sistema por favor contacte con soporte...');
                        $scope.list();
                    });
            } 
        }, 'Desea Eliminar el Usuario del Sistema? '+ item.name + ' ' +item.firstname);
    }
    $scope.loadUser = function (item){
        $scope.user.name = item.name + " "+item.firstname+ " "+ item.secondname;
        $scope.user.email=item.email;
        $scope.user.employedid=item.employedid;
        $scope.user.positionid=item.positionid;
        if(item.userid>0){
            $scope.user.userid=item.userid;
        }
        
    }
    $scope.searchDNIRUC= function(number,type){
        subjectService.searchdniruc({number:number,type:type}).then(function(data) {
            // if (data.data.length > 0){
                $scope.transaction.name = data.data.nombres;
                $scope.transaction.firstname = data.data.apellido_paterno + " " + data.data.apellido_materno;
            // }else{alertify.error("no se pudo consultar...");}
        });
    }
    $scope.saveEmployeed=function($event){
        var $btn;
        if(typeof($event) != 'undefined' ) $btn = $($event.currentTarget).button('loading');
        var validate=true;
        validate=validate_field(['#cmbIdentityDocument','#txtFirstname','#txtIdentityNumber','#txtname','#txtEmail']);
        if(validate){
            subjectService.saveEmployeed($scope.transaction).then(function(data){
                if(typeof($event) != 'undefined' ) $btn.button('reset');
                $('#modal-employeed').modal('hide');
                $scope.list();
                alertify.success("Transaccion Realizado Correctamente...");
            });
        }else{
            if(typeof($event) != 'undefined' ) $btn.button('reset');
            alertify.error("Campos Obligatorios");
        }
        
    }
    $scope.saveuser=function($event){
        var $btn;
        if(typeof($event) != 'undefined' ) $btn = $($event.currentTarget).button('loading');
        var validate=true;
        validate=validate_field(['#txtPassword','#txtPasswordR']);
        if(validate){
            if ($scope.user.password==$scope.user.passwordr) {
                subjectService.saveUser($scope.user).then(function(data){
                    if(typeof($event) != 'undefined' ) $btn.button('reset');
                    $('#modal-user').modal('hide');
                    $scope.list();
                    alertify.success("Transaccion Realizado Correctamente...");
                });
            }else{
                if(typeof($event) != 'undefined' ) $btn.button('reset');
                alertify.error("Las contraseñas no son iguales ...");
            }


        }else{
            if(typeof($event) != 'undefined' ) $btn.button('reset');
            alertify.error("Campos Obligatorios");
        }

    }
    $scope.setPagina=function(object,filters,page){
        $scope[object][filters].pagina=page;
    }
    $scope.list=function($event){
        var $btn;
        if(typeof($event) != 'undefined' ) $btn = $($event.currentTarget).button('loading');
        subjectService.listEmployeed($scope.transactions.filters).then(function(data){
            if(typeof($event) != 'undefined' ) $btn.button('reset');
            $scope.transactions.list=data.data.list;
            if(data.data.total > 0){
                var params={dom:"#paginador_Table", onclickaccion:'list' ,pagina:data.data.pagina,total:data.data.total, xpagina:data.data.xpagina, accion:null};
                configurar_paginador(params);
            }
        });        
    }
    $scope.listIdentityDocument=function(){
        subjectService.listIdentityDocument({}).then(function(data){
            $scope.identityDocuments.list=data.data;
        });
    }
    $scope.listPosition=function(){
        subjectService.listPositions({}).then(function(data){
            $scope.position.list=data.data;
        });
    }
    $scope.list();
    $scope.listPosition();
    $scope.listIdentityDocument();
});
