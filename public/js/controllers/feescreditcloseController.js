appangular.controller('feescreditcloseController', function($scope,$filter, $timeout, feesService,transactionService,subjectService) {
    
    $scope.fees = {list:[],filters:{}};
    $scope.fee = { details:[] };
    $scope.pendingPayment={list:[]};
    $scope.feetransaction={details:[]};
    $scope.payments={list:[]};
    var dateActualy = new Date();
	var fechaActual = new Date(dateActualy.getFullYear(), dateActualy.getMonth(), dateActualy.getDate());
    var lastDay = new Date(dateActualy.getFullYear(), dateActualy.getMonth() + 1, 0);
    var firstDay = new Date(dateActualy.getFullYear(), dateActualy.getMonth(), 1);

	$scope.fees.dateinit = $filter('date')(fechaActual,'dd/MM/yyyy');
    $scope.fees.dateend = $filter('date')(fechaActual, 'dd/MM/yyyy');
    
    $scope.fees.filters.dateinit = $filter('date')(new Date(dateActualy.getFullYear(), dateActualy.getMonth()),'dd/MM/yyyy');
    $scope.fees.filters.dateend = $filter('date')(new Date(dateActualy.getFullYear(), dateActualy.getMonth()+1, 0), 'dd/MM/yyyy');
    
    $scope.loadFees=function(item){
        $scope.fee=item;
        $scope.fee.amount = item.totalpayment;
        $scope.fee.capital = item.totalpayment;
        $scope.fee.interest=0;
        $scope.fee.date = $filter('date')(fechaActual, 'dd/MM/yyyy');     
    }

    $scope.saveFees=function(){
        var datesplit=$scope.fee.date.split("/");
        $scope.fee.date=datesplit[2]+'-'+datesplit[1]+'-'+datesplit[0]
        $scope.fee.transactiontypeid=7;
        feesService.saveCreditClose($scope.fee).then(function(data){
            $scope.listFees();
            alertify.success("Se Guardo Correctamente");
            $("#modal-fee-print").modal("show");
        },function(reason) {
            if(typeof($event) != 'undefined' ) $btn.button('reset');
            alertify.error('Surgio un problema en sistema por favor contacte con soporte...');
        });
    }

    $scope.setPagina=function(object,filters,page){
        $scope[object][filters].pagina=page;
    }
    $scope.listFees=function($event){
        var $btn;
         if(typeof($event) != 'undefined' ) $btn = $($event.currentTarget).button('loading');
         $scope.fees.filters.transactiontypeid=1;
        feesService.listFeesClient($scope.fees.filters).then(function(data){
            if(typeof($event) != 'undefined' ) $btn.button('reset');  
            $scope.fees.list=data.data.list;
            if(data.data.total > 0){
				var params={dom:"#paginador_Table", onclickaccion:'list' ,pagina:data.data.pagina,total:data.data.total, xpagina:data.data.xpagina, accion:null};
				configurar_paginador(params);
			}
        });        
    }
 
    $scope.listFees();
});
