appangular.controller('expenseController', function($scope,$filter, $timeout, feesService,transactionService,objectService,subjectService) {
    
    $scope.transactions = {list:[],filters:{}};
    $scope.objects = {list:[],filters:{}};
    $scope.clients = {list:[]};
    $scope.transaction={details:[]};
    $scope.payment={list:[]};
    
    var dateActualy = new Date();
	var fechaActual = new Date(dateActualy.getFullYear(), dateActualy.getMonth(), dateActualy.getDate());	
	
	$scope.transactions.dateinit = $filter('date')(fechaActual,'dd/MM/yyyy');
    $scope.transactions.dateend = $filter('date')(fechaActual, 'dd/MM/yyyy');

    $scope.transactions.filters.dateinit = $filter('date')(fechaActual,'dd/MM/yyyy');
    $scope.transactions.filters.dateend = $filter('date')(fechaActual, 'dd/MM/yyyy');
    
    $scope.newExpense = function (){
        $scope.transaction={details:[]};
        $scope.transaction.date=$filter('date')(fechaActual,'dd/MM/yyyy');
    }
   
    $scope.loadExpense=function(data){
        $scope.transaction={details:[]};
        $scope.transaction=data;
    }

    $scope.saveExpense=function($event){
        var $btn;
        if(typeof($event) != 'undefined' ) $btn = $($event.currentTarget).button('loading');
        var datesplit=$scope.transaction.date.split("/");
        $scope.transaction.date=datesplit[2]+'-'+datesplit[1]+'-'+datesplit[0];
        transactionService.saveExpenseAndPayment($scope.transaction).then(function(data){
            if(typeof($event) != 'undefined' ) $btn.button('reset');
           $scope.listExpenses();
        },function(reason) {
            if(typeof($event) != 'undefined' ) $btn.button('reset');
            alertify.error('Surgio un problema en sistema por favor contacte con soporte...');
        });
    }

    $scope.setPagina=function(object,filters,page){
        $scope[object][filters].pagina=page;
    }
    $scope.listExpenses=function($event){
        var $btn;
		 if(typeof($event) != 'undefined' ) $btn = $($event.currentTarget).button('loading');
        $scope.transactions.filters.comment='viewCredit';
        transactionService.listExpenseAndPayment($scope.transactions.filters).then(function(data){
            if(typeof($event) != 'undefined' ) $btn.button('reset');  
            $scope.transactions.list=data.data.list;
            if(data.data.total > 0){
				var params={dom:"#paginador_Table", onclickaccion:'list' ,pagina:data.data.pagina,total:data.data.total, xpagina:data.data.xpagina, accion:null};
				configurar_paginador(params);
			}
        },function(reason) {
            if(typeof($event) != 'undefined' ) $btn.button('reset');
            alertify.error('Surgio un problema en sistema por favor contacte con soporte...');
        });      
    }
    $scope.prepareDelete=function(data){
        
        alertify.prompt('Confirme:', function (e, str) {
            if (e) {
                transactionService.deletesale({subtransactionstateid:data.subtransactionstateid}).then(function(data){
                        alertify.success('Se elimino Correctamente');
                        $scope.listExpenses();
                    },function(reason) {
                        $scope.listExpenses();
                        alertify.error('Surgio un problema en sistema por favor contacte con soporte...');
                    });
            } 
        }, 'Desea Eliminar ... ??');
                
    }
    $scope.listExpenses();
});
