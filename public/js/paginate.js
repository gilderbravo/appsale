var SUPER_PAGINADOR_ACTUAL=1;
var SUPER_PAGINADOR_MAX_PAGINAS=10;
var PAGINADOR_PAGINA_ACTUAL=1;
var PAGINADOR_TOTAL_PAGINAS=1;
var PAGINADOR_MAX_PAGINAS=10;
	
function configurar_paginador(params){
	var element=params.dom;
	var onclickaccion=params.onclickaccion;
	var pagina_actual=params.pagina;
	var pagina_fila=1;
	var total=params.total;
	var xpagina=params.xpagina;
	var accion=params.accion;
	var nro_paginas=Math.ceil(total/xpagina);
	
	if( ! $(element).find(".wrap_paginator").length){
		crearPaginador(element,onclickaccion,pagina_actual,total,xpagina);
		pagina_fila=pagina_actual;
	}else{
		pagina_fila=(pagina_actual*xpagina-10)+1;
	}
	var $su_info=$(element).find(".info_paginador");
	var $su_paginador=$(element).find(".wrap_paginator");
	var p=cargarObjetoPaginador($su_paginador);
	
	if(accion=='reset') p.SUPER_PAGINADOR_ACTUAL=1;
	p.PAGINADOR_PAGINA_ACTUAL=pagina_actual;
	p.PAGINADOR_TOTAL_PAGINAS=nro_paginas;
	
	guardarObjetoPaginador(p,$su_paginador);
	//$su_paginador.parent().find('table tfoot').html("<tr><td colspan="+$su_paginador.parent().find("table thead th").length+"><strong>Se encontraron "+ total  +" registro(s) </strong></td></tr>");
	$su_info.html(""+pagina_fila+" - "+(pagina_actual*xpagina)+" (total:"+total+")");
	$su_paginador.find(".super_paginator_left, .super_paginator_right").removeAttr('disabled').attr('title','Total registros:'+nro_paginas);
	
	if(nro_paginas==0){ $su_paginador.find(".super_paginator_left, .super_paginator_right").hide();
	}else if(nro_paginas==1){
		$su_paginador.find(".super_paginator_left, .super_paginator_right").hide();
		render_paginador(onclickaccion, $su_paginador.find("button:nth-child(1)") );
	}else if(nro_paginas>p.SUPER_PAGINADOR_MAX_PAGINAS){
		$su_paginador.find(".super_paginator_left, .super_paginator_right").show();
		render_paginador( onclickaccion, $su_paginador.find("button:nth-child(1)") );
	}else{
		render_paginador(onclickaccion, $su_paginador.find("button:nth-child(1)"));
	}
}


function render_paginador(onclickaccion, ele, mode){
	var $su_paginador=$(ele).parent();
	var p=cargarObjetoPaginador($su_paginador);
	if(mode=='left'){ p.SUPER_PAGINADOR_ACTUAL--; guardarObjetoPaginador(p,$su_paginador);
	}else if(mode=='right'){ p.SUPER_PAGINADOR_ACTUAL++; guardarObjetoPaginador(p,$su_paginador);}
	
	if(p.SUPER_PAGINADOR_ACTUAL>1) { $su_paginador.find(".super_paginator_left").show();
	}else $su_paginador.find(".super_paginator_left").hide();
	
	var su_paginas= Math.ceil(p.PAGINADOR_TOTAL_PAGINAS/p.SUPER_PAGINADOR_MAX_PAGINAS); //Nro Paginadores del paginador
	//var r2= Math.ceil(su_paginas/p.PAGINADOR_MAX_PAGINAS);
	var residuo_ultim_su_pagina=p.PAGINADOR_TOTAL_PAGINAS % p.PAGINADOR_MAX_PAGINAS;
	//console.debug(p.PAGINADOR_TOTAL_PAGINAS);
	//console.debug(p.SUPER_PAGINADOR_MAX_PAGINAS);
	//console.debug(su_paginas);
	//console.debug(p.SUPER_PAGINADOR_ACTUAL);
	//console.debug(residuo_ultim_su_pagina);
	
	if(p.SUPER_PAGINADOR_ACTUAL<su_paginas){
		$su_paginador.find(".super_paginator_right").show();
	}else{
		$su_paginador.find(".super_paginator_right").hide();
	}
	
	var $paginador=$su_paginador.find(".full_paginator ul");
	$paginador.html("").fadeOut("fast");
	
	var max=p.SUPER_PAGINADOR_ACTUAL*p.SUPER_PAGINADOR_MAX_PAGINAS;
	if(p.SUPER_PAGINADOR_ACTUAL>= su_paginas &&  p.SUPER_PAGINADOR_MAX_PAGINAS>residuo_ultim_su_pagina) max=p.PAGINADOR_TOTAL_PAGINAS; //no poner a 1
	
	for(i=((p.SUPER_PAGINADOR_ACTUAL-1)*p.SUPER_PAGINADOR_MAX_PAGINAS);i<max; i++){
		$paginador.append('<li class="'+(p.PAGINADOR_PAGINA_ACTUAL==(i+1)?"active":"")+'"><a onclick="'+onclickaccion+'(this)" href="javascript:void(0)" data-id="'+(i+1)+'">'+(i+1)+'</a></li>');
    }
	
	$paginador.fadeIn("fast");
	
}

function cargarObjetoPaginador($tag){
	var cadena=$tag.attr('data-url');
	cadena=cadena.replace("{","").replace('}',"");
	var arr = cadena.split(",");
	var object={};
	for(var i=0; i<arr.length;i++){
		var t=arr[i].split(":");
		object[t[0]]=t[1];
	}
	return object;
}

function nuevoObjetoPaginador(){
	return {
		SUPER_PAGINADOR_ACTUAL:SUPER_PAGINADOR_ACTUAL,
		SUPER_PAGINADOR_MAX_PAGINAS:SUPER_PAGINADOR_MAX_PAGINAS,
		PAGINADOR_PAGINA_ACTUAL:PAGINADOR_PAGINA_ACTUAL,
		PAGINADOR_TOTAL_PAGINAS:PAGINADOR_TOTAL_PAGINAS,
		PAGINADOR_MAX_PAGINAS:PAGINADOR_MAX_PAGINAS,
	};
}

function guardarObjetoPaginador(objeto, $tag){
	$tag.attr("data-url",cadenaObjectPaginador(objeto));
}

function cadenaObjectPaginador(objeto){
	var cadena="{SUPER_PAGINADOR_ACTUAL:"+objeto.SUPER_PAGINADOR_ACTUAL
	+",SUPER_PAGINADOR_MAX_PAGINAS:"+objeto.SUPER_PAGINADOR_MAX_PAGINAS
	+",PAGINADOR_PAGINA_ACTUAL:"+objeto.PAGINADOR_PAGINA_ACTUAL
	+",PAGINADOR_TOTAL_PAGINAS:"+objeto.PAGINADOR_TOTAL_PAGINAS
	+",PAGINADOR_MAX_PAGINAS:"+objeto.PAGINADOR_MAX_PAGINAS
	+"}"
	return cadena;
}



function crearPaginador(element, onclickaccion,pagina_actual,total,xpagina){
	var objeto=nuevoObjetoPaginador();
	var data_url=cadenaObjectPaginador(objeto);
	
	var html= "<div class='col-sm-6'><span class='info_paginador'></span></div>" +
			"<div class='col-sm-6'><div data-url='"+data_url+"' class='wrap_paginator'>"+
				"<button class='super_paginator_left btn btn-success btn-xs' onclick=\"render_paginador('"+onclickaccion+"',this,'left')\">"+
					"<span aria-hidden='true'>&laquo;</span>"+
				"</button>"+
				"<div class='full_paginator'>"+
					"<ul class='pagination'></ul>"+
				"</div>"+
				"<button class='super_paginator_right btn btn-success btn-xs' onclick=\"render_paginador('"+onclickaccion+"',this,'right')\">"+
					"<span aria-hidden='true'>&raquo;</span>"+
				"</button>"+
			"</div></div>";
	
	$(element).append(html);
}

function validar_campo(lista){
    var valido=true;
    for(var i=0;i<lista.length;i++){
        $elemento= $(lista[i]);
        $parent=$elemento.parent();
        if( $elemento.val()=='' || $elemento.val()==' ' ||$elemento.val()==null || $elemento.val()==0){
            $parent.addClass("has-error");
            $parent.find(".errores").fadeIn();
            $elemento.focus();
            valido=false;
        }else{
            $parent.removeClass("has-error");
            $parent.find(".errores").hide();
        }
    }
    return valido;
}

function limpiarInput(lista){
    for(var i=0;i<lista.length;i++){
        $elemento=$(lista[i]);
        $elemento.val('');
    }
}

function validarFormulario(tagform){
    var valido=true;
    $.each( $(tagform+" .required"), function(i,val){
        $elemento=$(val);
        $parent=$elemento.parent();

        if($parent.find(".errores").length ){$parent.find(".errores").remove(); }

        if( $elemento.val()=='' || $elemento.val()==' ' ||$elemento.val()==null || $elemento.val()==0){
            $parent.addClass("has-error");
            $parent.append('<div class="errores"><i class="fa fa-info-circle"></i> Completa este Campo</div>').find('.errores').fadeIn();
            $elemento.focus();
            valido=false;
        }else{
            $parent.removeClass("has-error");
            $parent.find(".errores").fadeOut();
        }
    });
    return valido;
}

function empty(ele){
	
}