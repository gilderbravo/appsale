appangular.factory("objectService", function($http) {
    return {
        allBrandObject: function(params) {
            return $http.post("../object/allBrandObject", params);
        },       
        saveBrandObject: function(params) {
            return $http.post("../object/saveBrandObject", params);
        },
        deleteBrandObject: function(params) {
            return $http.post("../object/deleteBrandObject", params);
        },   
        listproperty: function(params) {
            return $http.post("../object/listproperty", params);
        },  
        savePropertyObjects: function(params) {
            return $http.post("../object/savePropertyObjects", params);
        },   
        listcategory: function(params) {
            return $http.post("../object/listcategory", params);
        }, 
        saveCategoryObjects: function(params) {
            return $http.post("../object/saveCategoryObjects", params);
        },   
        listpresentation: function(params) {
            return $http.post("../object/listpresentation", params);
        },     
        savePresentationObjects: function(params) {
            return $http.post("../object/savePresentationObjects", params);
        },     
        listObjectModel: function(params) {
            return $http.post("../object/listObjectModel", params);
        },
        saveObjectModel: function(params) {
            return $http.post("../object/saveObjectModel", params);
        },
        listObject: function(params) {
            return $http.post("../object/listObject", params);
        },
        saveObject: function(params) {
            return $http.post("../object/saveObject", params);
        },
        listObjectSale: function(params) {
            return $http.post("../object/listObjectSale", params);
        },

    }
});
appangular.factory("transactionService", function($http) {
    return {
        listPurchase: function(params) {
            return $http.post("../transaction/listPurchase", params);
        },
        listSale: function(params) {
            return $http.post("../transaction/listSale", params);
        },
        listSaleTicket: function(params) {
            return $http.post("../transaction/listSaleTicket", params);
        },
        saveSale: function(params) {
            return $http.post("../transaction/saveSale", params);
        },
        saveSaleTicket: function(params) {
            return $http.post("../transaction/saveSaleTicket", params);
        },
        changestatusdisbursement: function(params) {
            return $http.post("../transaction/changestatusdisbursement", params);
        },
        deletesale: function(params) {
            return $http.post("../transaction/deletesale", params);
        },
        
        savePurchase: function(params) {
            return $http.post("../transaction/savePurchase", params);
        },
        listSubtransactiondetails: function(params) {
            return $http.post("../transaction/listSubtransactiondetails", params);
        },
        savePaymentSchedule: function(params) {
            return $http.post("../transaction/savePaymentSchedule", params);
        },
        listPaymentSchedule: function(params) {
            return $http.post("../transaction/listPaymentSchedule", params);
        },
        listExpenseAndPayment: function(params) {
            return $http.post("../transaction/listExpenseAndPayment", params);
        },
        saveExpenseAndPayment: function(params) {
            return $http.post("../transaction/saveExpenseAndPayment", params);
        },
        saveDepositAndQualification: function(params) {
            return $http.post("../transaction/saveDepositAndQualification", params);
        },
        listCashbox: function(params) {
            return $http.post("../transaction/listCashbox", params);
        },
        saveCashbox: function(params) {
            return $http.post("../transaction/saveCashbox", params);
        },
    }
});
appangular.factory("subjectService", function($http) {
    return {
        listClient: function(params) {
            return $http.post("../subject/listClient", params);
        },
        listProvider: function(params) {
            return $http.post("../subject/listProvider", params);
        },
        updateUser:function(params) {
            return $http.post("../subject/updateUser", params);
        },
        saveClient: function(params) {
            return $http.post("../subject/saveClient", params);
        },
        listIdentityDocument: function(params) {
            return $http.post("../subject/listIdentityDocument", params);
        },
        searchdniruc: function(params) {
            if (params.type==1){
                return $http.post('https://www.facturador.nextfuture.com.pe/public/api/persona/dni/'+params.number,{});
              }else {
                return $http.post('https://www.facturador.nextfuture.com.pe/public/api/persona/ruc/'+params.number,{});
              }
            // return $http.post("../subject/searchdniruc", params);
        },
        listEmployeed: function(params) {
            return $http.post("../subject/listEmployeed", params);
        },
        saveEmployeed: function(params) {
            return $http.post("../subject/saveEmployeed", params);
        },
        saveUser: function(params) {
            return $http.post("../subject/saveUser", params);
        },
        listPositions: function(params) {
            return $http.post("../subject/listPositions", params);
        },
    }
});
appangular.factory("feesService", function($http) {
    return {
        listpaymentschedule: function(params) {
            return $http.post("../fees/listpaymentschedule", params);
        },
        listFeesClient: function(params) {
            return $http.post("../fees/listFeesClient", params);
        },
        saveFee: function(params) {
            return $http.post("../fees/saveFee", params);
        },
        saveFeeClose: function(params) {
            return $http.post("../fees/saveFeeClose", params);
        },
        saveCreditClose: function(params) {
            return $http.post("../fees/saveCreditClose", params);
        },
        removepayment: function(params) {
            return $http.post("../fees/removepayment", params);
        },
    }
});
appangular.factory("reportService", function($http) {
    return {
        listReportpayments: function(params) {
            return $http.post("../reports/listReportpayments", params);
        },
        listReportTransactionCloses: function(params) {
            return $http.post("../reports/listReportTransactionCloses", params);
        },
        listCashboxSummary: function(params) {
            return $http.post("../reports/listCashboxSummary", params);
        },
        deletefee: function(params) {
            return $http.post("../reports/deletefee", params);
        },
        updateCreditReversal: function(params) {
            return $http.post("../reports/updateCreditReversal", params);
        },


    }
});