var appangular = angular.module('appangular', ['Alertify','rt.select2']).run(function(select2Config) {
    select2Config.minimumResultsForSearch = 1;
    select2Config.dropdownAutoWidth = true;
    select2Config.minimumInputLength=2;
 });
 appangular.directive('ngEnter', function () {
    return function (scope, element, attrs) {
        element.bind("keydown keypress", function (event) {
            if (event.which === 13) {
                scope.$apply(function () {
                    scope.$eval(attrs.ngEnter);
                });
                event.preventDefault();
            }
        });
    };
});
appangular.run(function($rootScope,$filter, $timeout) {
    $rootScope.clientid=0;
    
});

