$(document).ready(function() {

    $('.date').datepicker({
        format: 'dd/mm/yyyy',
        language: "es",
        autoclose: true
    });
});
$(document).ready(function() {
    // $('.select2').select2();
    $('select2').select2({
        placeholder: 'This is my placeholder',
        allowClear: true
      });
});
function validate_field(list){
    var valid=true;
    for(var i=0;i<list.length;i++){
        $element= $(list[i]);
        $parent=$element.parent();
        if( $element.val()=='' || $element.val()==' ' || $element.val()==null || $element.val()=='? object:null ?' || $element.val()=='? number:0 ?' || $element.val()=='? undefined:undefined ?'){
            $parent.addClass("has-error");
            $element.focus();
            valid=false;
        }else{
            $parent.removeClass("has-error");
        }
    }
    return valid;
}
function validateInputNumber(e){
    key = (document.all) ? e.keyCode : e.which;
    //Tecla de retroceso para borrar, siempre la permite
    if (key==8){
        return true;
    }	       
    // Patron de entrada, en este caso solo acepta numeros
    pattern =/[0-9]/;
    final_key = String.fromCharCode(key);
    return pattern.test(final_key);
}

function exportTableToExcel(tableID, filename = ''){
    var downloadLink;
    var dataType = 'application/vnd.ms-excel';
    var tableSelect = document.getElementById(tableID);
    var tableHTML = tableSelect.outerHTML.replace(/ /g, '%20');

    // Specify file name
    filename = filename?filename+'.xls':'excel_data.xls';

    // Create download link element
    downloadLink = document.createElement("a");

    document.body.appendChild(downloadLink);

    if(navigator.msSaveOrOpenBlob){
        var blob = new Blob(['ufeff', tableHTML], {
            type: dataType
        });
        navigator.msSaveOrOpenBlob( blob, filename);
    }else{
        // Create a link to the file
        downloadLink.href = 'data:' + dataType + ', ' + tableHTML;

        // Setting the file name
        downloadLink.download = filename;

        //triggering the function
        downloadLink.click();
    }
}
$(function () {
    function formatRepo (repo) {
		return repo.text;
	}

	function formatRepoSelection (repo) {
		return repo.text;
	}

	$('.chosen-select').select2({
		ajax: {
            headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
            type:"POST",
            url: "../subject/listClient",
            dataType: 'json',
            delay: 500,
			data: function (params) {
				// var tax_name = $("[name=main_cat_choice]").val();
				return  ('{ "filtro": "'+params.term+'" }');
			},
			processResults: function( data ) {
				var options = [];
				if ( data.list ) {
					$.each( data.list, function( index, text ) {
						options.push( { id: (text.id + '|'+text.name+ " " +text.firstname+'|'+text.identitynumber), text: text.name+" "+text.firstname  } );
					});
11
				}
				return {
					results: options
				};
			},
		},
		templateResult: formatRepo,
		templateSelection: formatRepoSelection,
		theme: "classic"
    });

    });

    // function getval(sel)
    // {
    //     alert(sel.value);
    // }
