@extends('adminlte::layouts.app')
@section('htmlheader_title')
	| Depositos Diarios
@endsection
@section('contentheader_title')
	Nuevo  <a href="#" ng-click="newPayment();" data-target="#modal-payment" data-toggle="modal" class="btn btn-success btn-xs">Nuevo</a>
@endsection
@section('angularcontroller')
ng-controller="depositController"
@endsection

@section('main-content')
	<div class="container-fluid spark-screen" >
	<script>
	function list(element){
		var nueva_pagina;
		if(element!=undefined) {
			nueva_pagina=$(element).attr('data-id');
			angular.element("#depositController").scope().setPagina('transactions','filters',nueva_pagina);
			angular.element('#depositController').scope().listPayments();
		}
	}
	function getvalclient(sel)
    {
		angular.element('#depositController').scope().getclientid(sel.value);
    }
	</script>
		<div class="row">
			<div class="col-md-12">
				<div class="panel panel-default">
					<div class="panel-heading">LISTADO DE DEPOSITOS DIARIOS</div>
					<div class="row table-filters ">
							<div class="col-md-4">
								<input type="text" ng-model="transactions.filters.filtro" placeholder="Buscar" class="form-control" ng-enter="transactions.filters.pagina=1;listPayments($event)"/>
							</div>				
							<div class="col-md-2">									
								<div class="form-inline">
									<div class="input-group date">
										<span class="input-group-addon"><i class="fa fa-calendar"></i></span> <input type="text"  value="transactions.filters.dateinit"  ng-model="transactions.filters.dateinit" placeholder="#{msg['Fecha_Inicio']}" class="form-control dateinput input-append date" />
									</div>
								</div>
							</div>
							<div class="col-md-2">								
								<div class="form-inline">
									<div class="input-group date">
										<span class="input-group-addon"><i class="fa fa-calendar"></i></span> <input type="text" value="transactions.filters.dateend" ng-model="transactions.filters.dateend" placeholder="#{msg['Fecha_Final']}" class="form-control dateinput" />
									</div>
								</div>
							</div>													
							<div class="col-md-3 ">										
								<button class="btn btn-info btn-xs" data-loading-text="Please wait..." ng-click="transactions.filters.pagina=1;listPayments($event)"><i class="fa fa-search"></i> Buscar</button>
								<button class="btn btn-success btn-xs"  data-loading-text="Please wait..."  ng-click="exportAll(event);"><i class="fa fa-file-excel-o"></i> Exportar</button>

							</div>
							
					</div>
					<div class="panel-body" id="depositController" >
						<table class="table table-striped table-bordered table-condensed table-hover ">
							<thead>
								<th>Fecha</th>
								<th>Detalle</th>	
								<th>Numero</th>
								<th>Monto</th>
                                <th>Estado</th>
                                <th>Acciones</th>
							</thead>               
							<tr ng-repeat="item in transactions.list">									
								<td>@{{item.date}}</td>
								<td>@{{item.comment}}</td>
								<td>@{{item.code}}</td>		
								<td>@{{item.total}}</td>		
                                <td>@{{item.status}}</td>		                                
								<td>
								@if ( Auth::user()->positionid==1)
									<a href="#" ng-click="prepareDelete(item);" class="btn btn-danger btn-xs"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
									@endif
								</td>
							</tr>									
						</table>
						<div id="paginador_Table"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
	@include ('payment.modal-create')
@endsection
@section ('javascript')
<script src="{{asset('js/controllers/depositController.js')}}"></script>
@endsection