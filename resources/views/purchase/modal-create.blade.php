<div class="modal fade modal-slide-in-right" aria-hidden="true" role="dialog" tabindex="-1" id="modal-purchase">
	<div class="modal-dialog modal-size">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" 
				aria-label="Close">
                     <span aria-hidden="true">×</span>
                </button>				
                <h4 class="modal-title"> Registro Compra</h4>
			</div>
			<div class="modal-body">
				<div class="col-md-12">
					<div class="col-md-7">
						<div class="col-md-6">								
							<div class="form-group">
								<label class=	"col-md-4 control-label">Proveedor</label>
								<div class="col-md-8">
									<select id="cmbCliente" class="form-control select2" ng-model="transaction.clientid">
										<option ng-repeat="item in providers.list" value="@{{item.id}}">@{{item.identitydocumentid ==2?item.firstname:(item.name + ' '+item.firstname)}}</option>
									</select>													
								</div>
							</div>														
						</div>
						<div class="col-md-6">								
							<div class="form-group col-md-12">
								<label class="col-md-4 ">Fecha</label>
								<div class="input-group date " >
										<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
										<input type="text"  ng-model="transaction.date" class="form-control dateinput input-append date" />
								</div>
							</div>
							<div class="form-group col-md-12">
								<div class="input-group col-md-12" style="margin: 5px;">
									<span class="input-group-addon" id="basic-addon1">N° Comp.</span>
									<div class="col-md-10 ">								
										<input type="text"  ng-model="transaction.code" class="form-control" />										
									</div>
								</div>
								
							</div>		
						</div>
						<div class="wrapper-table-detail">
							<table class='table table-bordered table-stripped table-hover' >
								<thead><tr><th>Cantidad</th><th>DESC</th><th class="col-price">PREC</th><th>IMP</th><th></th></tr></thead>
								<tbody class="datails-tbody">
										<tr ng-repeat="item in transaction.details"  >
										<td class="col-amount"  > <span class="class-amount numberCantPrint" style="display: none;">@{{item.amount}} </span> <input class="form-control amount-center amountInput" ng-change="calculateTotal()"   focus-on-me="$last"  ng-model="item.amount" name="amount" /></td>													          										
										<td class="desc-product"> <input class="form-control"  ng-model="item.product" name="product" /></td>
										<td > <span class="class-amount numberUniPrint" style="display: none;">@{{item.unitpricesale|number:2}} </span> <input class="form-control amount-right amountInput"  ng-change="calculateTotal()" ng-model="item.unitpricesale"  /></td>
										<td class="money">@{{item.totalmoney|number:2}}</td>
										<td class='centered' ><a ng-click="removeDetail($index)" class='btn btn-danger btn-xs'><i class="fa fa-remove"></i></a></td>
										</tr>
								</tbody>
								<tfoot class="amount-right">
										<tr><td></td><td></td><td></td><td class="texto">Neto:</td><td class="numberPrint">@{{transaction.neto|number:2 }}</td><td></td></tr>
										<tr><td></td><td></td><td></td><td class="texto">IGV<span id="valueIgv">(18%)</span>:</td><td class="numberPrint">@{{transaction.igv|number:2 }}</td><td></td></tr>
										<tr><td></td><td></td><td></td><td class="texto" >Total:</td><td class="numberPrint">@{{transaction.total|number:2}}</td><td></td></tr>
								</tfoot>
								</table>
						</div>
					</div>
					<div class="col-md-5">
						<div id="list" title="1">
							<div class="list-product">
								<div class="product-title"> 
									<div class="col-md-8">
										<span>Nombre</span>
									</div>
									<div class="col-md-2 centered" >
										<span >Stock</span>
									</div>
									<div class="col-md-2">
										<span>Precio</span>
									</div>
								</div>
								<div class="product-details" ng-repeat="item in objects.list" >
										<div class="col-md-8 name" ng-click="onProductClick(item)">@{{item.nameconcat}} </div>
										<div class="col-md-2 name centered" ng-click="onProductClick(item)">
											<span>@{{item.stock}}</span>
										</div>
										<div class="col-md-2 amount-right name" ng-click="onProductClick(item)">@{{(item.unitpricesale | number:2)}}</div>
									  
								</div>
							</div>
						</div>
					</div>					 					
				</div>
			</div>
			<div class="modal-foot ">
				<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
				<button type="submit" class="btn btn-primary" ng-click="savePurchase();" data-dismiss="modal" ng-disabled="BrandForm.$invalid" id="submit-all">Confirmar</button>
			</div>
		</div>
	</div>
	
</div>