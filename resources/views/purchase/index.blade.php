@extends('adminlte::layouts.app')
@section('htmlheader_title')
	| Compras
@endsection
@section('contentheader_title')
	COMPRAS <a href="#" ng-click="newPurchase();" data-target="#modal-purchase" data-toggle="modal" class="btn btn-success btn-xs">Nuevo</a>
@endsection
@section('angularcontroller')
ng-controller="purchaseController" 
@endsection


@section('main-content')
	<div class="container-fluid spark-screen" ng-cloak>
		<script>
			function list(element){
				var nueva_pagina;
				if(element!=undefined) {
					nueva_pagina=$(element).attr('data-id');
					angular.element("#purchaseController").scope().setPagina('transactions','filters',nueva_pagina);
					angular.element('#purchaseController').scope().listSales();
				}
			}
			function getvalclient(sel)
			{
				angular.element('#purchaseController').scope().getclientid(sel.value);
			}
		</script>
		<div class="row">
			<div class="col-md-12">
				<div class="panel panel-default">
					<div class="panel-heading">LISTADO DE COMPRAS</div>
					<div class="row table-filters ">
							<div class="col-md-4">
								<input type="text" ng-model="transactions.filters.filtro" placeholder="Buscar" class="form-control" ng-enter="transactions.filters.pagina=1;listSales($event)"/>
							</div>				
							<div class="col-md-2">									
								<div class="form-inline">
									<div class="input-group date">
										<span class="input-group-addon"><i class="fa fa-calendar"></i></span> <input type="text"  value="transactions.filters.dateinit"  ng-model="transactions.filters.dateinit" placeholder="#{msg['Fecha_Inicio']}" class="form-control dateinput input-append date" />
									</div>
								</div>
							</div>
							<div class="col-md-2">								
								<div class="form-inline">
									<div class="input-group date">
										<span class="input-group-addon"><i class="fa fa-calendar"></i></span> <input type="text" value="transactions.filters.dateend" ng-model="transactions.filters.dateend" placeholder="#{msg['Fecha_Final']}" class="form-control dateinput" />
									</div>
								</div>
							</div>													
							<div class="col-md-3 ">										
								<button class="btn btn-info btn-xs" data-loading-text="Please wait..." ng-click="transactions.filters.pagina=1;listSales($event)"><i class="fa fa-search"></i> Buscar</button>
								<button class="btn btn-success btn-xs"  data-loading-text="Please wait..."  ng-click="exportAll(event);"><i class="fa fa-file-excel-o"></i> Exportar</button>

							</div>
							
					</div>
					<div class="panel-body" id="purchaseController" >
						<table class="table table-striped table-bordered table-condensed table-hover ">
							<thead>
								<th>Fecha</th>
								<th>DNI/RUC</th>	
								<th>Proveedor</th>
								<th>Comprobante</th>
								<th>Monto</th>
                                <th>Estado</th>
                                <th>Acciones</th>
							</thead>               
							<tr ng-repeat="item in transactions.list">									
								<td>@{{item.date}}</td>
								<td>@{{item.identitynumber}}</td>
                                <td>@{{item.client}}</td>		
								<td>@{{item.code}}</td>		
								<td>@{{item.total}}</td>		
                                <td>@{{item.status}}</td>		                                
								<td>
									<a href="#" ng-click="loadPurchase(item);" data-target="#modal-purchase" data-toggle="modal" class="btn btn-warning btn-xs"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
									<a href="#" ng-click="loadPurchase(item);" data-toggle="modal"  data-target="#modal-purchase-delete" class="btn btn-danger btn-xs"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
								</td>
							</tr>									
						</table>
						<div id="paginador_Table"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
	@include ('purchase.modal-create')
	@include ('purchase.modal-delete')
@endsection
@section ('javascript')
<script src="{{asset('js/controllers/purchaseController.js')}}"></script>
@endsection