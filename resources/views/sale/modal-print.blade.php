<div class="modal fade modal-slide-in-right" aria-hidden="true" data-keyboard="false" role="dialog" tabindex="-1" id="modal-sale-print">
	<div class="modal-dialog modal-md">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" 
				aria-label="Close">
                     <span aria-hidden="true">×</span>
                </button>				
                <h4 class="modal-title"> Imprimir Pago	</h4>
			</div>
			<div class="modal-body">
				<div id="print_sales" class="ticket-print" style="-webkit-transform:scale(0.85);">
					<div class="row" style="margin-left: -10px;">
						<div class="col-lg-12" style="padding: 0">
								<p style="margin:0;font-size:100%; text-align: center;font-family:Verdana, Geneva, sans-serif;font-weight: normal;padding-bottom: 5px;">INVERSIONES DEYCAR</p>
								<p style="margin:0;font-size:80%; text-align: center;font-family:Verdana, Geneva, sans-serif;">RUC: 20600370155</p>
								<p style="margin:0;font-size:80%; text-align: center;font-family:Verdana, Geneva, sans-serif;">Dirección: Jr. Pucallpa N° 339</p>					                    			
											
								<p style="margin:0;font-size:80%; text-align: center;font-family:Verdana, Geneva, sans-serif; font-weight: normal;" >Comprobante de Venta</p>
								<p style="margin:0;font-size:80%; text-align: center;font-family:Verdana, Geneva, sans-serif;">@{{transaction.codevoucher }}</p>
								<p style="margin:0;font-size:80%;font-family:Verdana, Geneva, sans-serif;">Atendido por: {{ Auth::user()->name }}</p> 	    												    												    											
								<p style="margin:0;font-size:80%;font-family:Verdana, Geneva, sans-serif;">Fecha: @{{ transaction.date |date:'dd/MM/yyyy' }}</p>	    												  	    											  									
								<p style="margin:0;font-size:80%;font-family:Verdana, Geneva, sans-serif;">Señor: @{{ transaction.client }}</p>
								<p style="margin:0;font-size:80%;font-family:Verdana, Geneva, sans-serif;">Dni/Ruc: @{{ transaction.identitynumber }}</p>
								<p style="margin:0;font-size:80%;font-family:Verdana, Geneva, sans-serif;border-top: 1.5px dashed #000; margin-top: 5px;margin-bottom: 5px;"></p>
						</div>
						<div class="col-lg-12" style="padding: 0">
							<div style="display: flex;padding:0; " >
								<div style="width:25%; display: inline-block;"><p style="margin:0;padding:0;  font-size:80%;font-family:Verdana, Geneva, sans-serif;">N°</p></div>
								<div style="width:25%; display: inline-block;"><p style="margin:0;padding:0; font-size:80%;font-family:Verdana, Geneva, sans-serif;">Cant.</p></div>
								<div style="width:25%; display: inline-block;"><p style="margin:0;padding:0; font-size:80%;font-family:Verdana, Geneva, sans-serif;">Descripcion</p></div>
								<div style="width:20%; display: inline-block;"><p style="margin:0;padding:0; font-size:80%;font-family:Verdana, Geneva, sans-serif;">Precio</p></div>
								<div style="width:20%; display: inline-block;"><p style="margin:0;padding:0; font-size:80%;font-family:Verdana, Geneva, sans-serif;">Total</p></div>
							</div> 
							<p style="margin:0;font-size:80%;font-family:Verdana, Geneva, sans-serif;border-top: 1.5px dashed #000; margin-top: 5px;margin-bottom: 5px;"></p>
							<div ng-repeat="item in transaction.details" style="display: flex;padding:0;">
								<div style="width:25%; display: inline-block;"><p style="margin:0;padding:0;font-size:80%;font-family:Verdana, Geneva, sans-serif;">@{{$index + 1 }}</p></div>
								<div style="width:25%; display: inline-block;"><p style="margin:0;padding:0;font-size:80%;font-family:Verdana, Geneva, sans-serif;">@{{item.amount }}</p></div>
								<div style="width:25%; display: inline-block;"><p style="margin:0;padding:0;font-size:80%;font-family:Verdana, Geneva, sans-serif;">@{{item.product }}</p></div>
								<div style="width:25%; display: inline-block;"><p style="margin:0;padding:0;font-size:80%;font-family:Verdana, Geneva, sans-serif;">@{{item.unitpricesale}}</p></div>
								<div style="width:20%; display: inline-block;"><p style="margin:0;padding:0;font-size:80%;font-family:Verdana, Geneva, sans-serif;">@{{item.totalmoney}}</p></div>
							</div>		    								
							<p style="margin:0;font-size:80%;font-family:Verdana, Geneva, sans-serif;border-top: 1.5px dashed #000; margin-top: 5px;margin-bottom: 5px;"></p>
							<div style="display: flex;"> <div style="width:60%; margin:0;font-size:80%; display:inline-block;"><p style="margin:0;font-size:80%;font-family:Verdana, Geneva, sans-serif;">TOTAL:</p> </div> <div style="width:40%;display:inline-block;vertical-align:top; text-align:right;"><p style="margin:0; font-size:80%;font-family:Verdana, Geneva, sans-serif; ">S/ @{{transaction.total|number:2}}</p></div> </div>
							
							<p style="margin:0;font-size:80%;font-family:Verdana, Geneva, sans-serif;border-top: 1.5px dashed #000; margin-top: 5px;margin-bottom: 5px;"></p>
						</div>
					</div>
				</div>
			</div>
			<div class="modal-foot ">
				<button type="button"  class="btn btn-default hidden-print" data-dismiss="modal">Cerrar</button>
				<button class="btn btn-success hidden-print" data-dismiss="modal" id="fee_print" onclick="printingVoucher('#print_sales','sale_prints')"><i class="fa fa-print"></i> <h:outputText value="#{msg['Imprimir']}" /></button>

			</div>
		</div>
	</div>
	
</div>