<!-- <div class="modal fade modal-slide-in-right" aria-hidden="true" role="dialog" tabindex="-1" id="modal-sale"> -->
<div class="modal fade modal-slide-in-right" aria-hidden="true" data-backdrop="static" data-keyboard="false" role="dialog" id="modal-sale">
	<div class="modal-dialog modal-size">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" 
				aria-label="Close">
                     <span aria-hidden="true">×</span>
                </button>				
                <h4 class="modal-title"> Venta</h4>
			</div>
			<div class="modal-body">
				<div class="col-md-12">
					<div class="col-md-7">
						<div class="col-md-7">								
							<div class="form-group">							
								<div class="input-group col-md-12" style="margin: 5px;">
									<span class="input-group-addon" id="basic-addon1">Cliente</span>
									<div class="col-md-12 " style="width: 300px;">								
										<select class="chosen-select" id="sel_client" onChange="getvalclient(this)" style="width: 100%;">
										</select>
									</div>
								</div>
								<div class="input-group col-md-12" style="margin: 5px;" ng-show="transaction.subtransactionid>0">
									<span class="input-group-addon" id="basic-addon1">
									@{{transaction.client}}
									</span>									
								</div>
							</div>														
						</div>
						<div class="col-md-5">								
							<div class="form-group">						
								
								<div class="input-group col-md-12" style="margin: 5px;">
									<span class="input-group-addon" id="basic-addon1">Fecha</span>
									<div class="col-md-12 " >								
									<div class="input-group date">
											<span class="input-group-addon"><i class="fa fa-calendar"></i></span> 
											<input type="text"  value="transaction.date"  ng-model="transaction.date" class="form-control dateinput input-append date" />
										</div>
									</div>
								</div>
								<div class="form-check form-check-inline col-md-12">
										<label class="">Ventas con Salida Stock? </label>
										<label class="form-check-label" >Si</label>
										<input class="form-check-input" ng-model="transaction.isSalesStock" type="checkbox" id="inlineCheckbox1" value="1">  									
								</div>	
							</div>		
						</div>
						<div class="wrapper-table-detail ">
							<table class='table table-striped table-bordered table-condensed table-hover' >
								<thead><tr><th>Cantidad</th><th>DESC</th><th class="col-price">PREC</th><th>IMP</th><th></th></tr></thead>
								<tbody class="datails-tbody">
										<tr ng-repeat="item in transaction.details"  >
										<td class="col-amount"  > <span class="class-amount numberCantPrint" style="display: none;">@{{item.amount}} </span> <input class="form-control amount-center amountInput" ng-change="calculateTotal()"   focus-on-me="$last"  ng-model="item.amount" name="amount" /></td>
										<td class="desc-product"> <input class="form-control"  ng-model="item.product" name="product" /></td>
										<td > <span class="class-amount numberUniPrint" style="display: none;">@{{item.unitpricesale|number:2}} </span> <input class="form-control amount-right amountInput"  ng-change="calculateTotal()" ng-model="item.unitpricesale"  /></td>
										<td class="money">@{{item.totalmoney |number:2}}</td>
										<td class='centered' ><a ng-click="removeDetail($index)" class='btn btn-danger btn-xs'><i class="fa fa-remove"></i></a></td>
										</tr>
								</tbody>
								<tfoot class="amount-right">
										<tr><td></td><td></td><td></td><td class="texto">Neto:</td><td class="numberPrint">@{{transaction.neto|number:2 }}</td><td></td></tr>
										<tr><td></td><td></td><td></td><td class="texto">IGV<span id="valueIgv">18%</span>:</td><td class="numberPrint">@{{transaction.igv|number:2 }}</td><td></td></tr>
										<tr><td></td><td></td><td></td><td class="texto" >Total:</td><td class="numberPrint">@{{transaction.total|number:2}}</td><td></td></tr>
								</tfoot>
								</table>
						</div>

					</div>
					<div class="col-md-5">
						<div id="list" title="1">
							<div class="list-product list-product-content">
								<div class="row title product-title"> 
									<div class="col-md-8"><span><Strong>Nombre</Strong></span></div>
									<div class="col-md-2 centered" ><span ><Strong>Stock<Strong></span></div>
									<div class="col-md-2"><span><Strong>Precio<Strong></span></div>
								</div>
								<div class="list-product row" ng-repeat="item in objects.list" >
									   <a  ng-click="onProductClick(item)"   href="javascript:void(0)" >
										<div class="col-md-8 name centered">@{{item.nameconcat}} </div>
										<div class="col-md-2 name centered" ><span>@{{item.stock}}</span></div>
										<div class="col-md-2 amount-right name centered">@{{(item.unitpricesale | number:2)}}</div>
									   </a>
								</div>
							</div>
						</div>
					</div>					 					
				</div>
			</div>
			<div class="modal-foot ">
				<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
				<button type="submit" class="btn btn-primary"  data-loading-text="Espere Por Favor..."  ng-click="saveSale($event);" data-dismiss="modal" ng-disabled="BrandForm.$invalid" id="submit-all">Confirmar</button>
			</div>
		</div>
	</div>
	
</div>