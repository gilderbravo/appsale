@extends('adminlte::layouts.app')
@section('htmlheader_title')
	| Categorias del Objeto
@endsection
@section('contentheader_title')
	Categorias <a href="#" ng-click="newCategoryObjects();" data-target="#modal-categoryObjects" data-toggle="modal" class="btn btn-success btn-xs">Nuevo</a>
@endsection
@section('angularcontroller')
ng-controller="categoryObjectsController" 
@endsection
@section('main-content')
	<div class="container-fluid spark-screen" ng-cloak>
		<div class="row">
			<div class="col-md-12">
				<div class="panel panel-default">
					<div class="panel-heading">Listado de Categorias
						
					</div>
					
					<div class="panel-body">
						<table class="table table-striped table-bordered table-condensed table-hover">
							<thead>
								<th>N°</th>
								<th>Nombre</th>	
								<th>Descripción</th>
								{{-- <th>Parent Id</th>
								<th>Level number</th>				 --}}
								<th>Opción</th>									
							</thead>               
							<tr ng-repeat="item in categories.list">									
								<td>@{{item.id}}</td>
								<td>@{{item.name}}</td>
								<td>@{{item.description}}</td>
								{{-- <td>@{{item.parentid}}</td>
								<td>@{{item.levelnumber}}</td> --}}
								{{-- <td>@{{item.status}}</td>					 --}}
								<td>
									<a href="#" ng-click="loadCategoryObjects(item);" data-target="#modal-categoryObjects" data-toggle="modal" class="btn btn-warning btn-xs"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
									<a href="#" ng-click="loadCategoryObjects(item);" data-toggle="modal"  data-target="#modal-categoryObjects-delete" class="btn btn-danger btn-xs"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
								</td>
							</tr>									
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
	@include ('categoryObjects.modal-create')
	@include ('categoryObjects.modal-delete')
@endsection
@section ('javascript')
<script src="{{asset('js/controllers/categoryObjectsController.js')}}"></script>
@endsection