<div class="modal fade modal-slide-in-right" aria-hidden="true" role="dialog" tabindex="-1" id="modal-objectModel">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" 
				aria-label="Close">
                     <span aria-hidden="true">×</span>
                </button>				
                <h4 class="modal-title"> Modelos</h4>
			</div>
			<div class="modal-body">
				<div class=" col-md-12">
						<div class="col-md-6">								
							<div class="form-group">
								<label class="col-md-4 control-label">Modelo</label>
								<div class="col-md-8">
									<input class="form-control" type="text" ng-model="objectModel.name">												
								</div>
							</div>	
							<br /><br />
							<div class="form-group">
								<label class="col-md-4 control-label">Marca</label>
								<div class="col-md-8">
									<select id="cmbBrand" class="form-control" ng-model="objectModel.brandobjectid" >
											<option ng-repeat="item in brandobjects.list" value="@{{item.id}}">@{{item.name}}</option>
									</select>	
								</div>
							</div>													
							<br /><br />
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label class="col-md-4 control-label">Propiedad</label>
								<div class="col-md-8">
									<select id="cmbProperty" class="form-control" ng-model="objectModel.propertyobjectid" >
										<option ng-repeat="item in properties.list" value="@{{item.id}}">@{{item.name}}</option>
									</select>																
								</div>
							</div>
							<br /><br />
							<div class="form-group">
								<label class="col-md-4 control-label">Descripcion</label>
								<div class="col-md-8">
									<textarea class="form-control"  rows="2" ng-model="objectModel.description"></textarea>
								</div>
							</div>
							<br /><br />
						</div>	
																
				</div>
				
			</div>
			<div class="modal-footer">
					<br /><br /><br /><br />
				<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
				<button type="submit" class="btn btn-primary" ng-click="saveObjectModel();" data-dismiss="modal" ng-disabled="ObjectForm.$invalid" id="submit-all">Confirmar</button>
			</div>
		</div>
	</div>
	
</div>