@extends('adminlte::layouts.app')
@section('htmlheader_title')
	| Productos
@endsection
@section('contentheader_title')
	PRODUCTOS <a href="#" ng-click="newObjectModel();" data-target="#modal-objectModel" data-toggle="modal" class="btn btn-success btn-xs">Nuevo</a>
@endsection
@section('angularcontroller')
ng-controller="objectModelsController" 
@endsection


@section('main-content')
	<div class="container-fluid spark-screen" ng-cloak>
		<div class="row">
			<div class="col-md-12">
				<div class="panel panel-default">
					<div class="panel-heading">Listado de Modelos
						
					</div>
					
					<div class="panel-body">
						<table class="table table-striped table-bordered table-condensed table-hover">
							<thead>
								<th>N°</th>								
								<th>Nombre</th>
								<th>Descripcion</th>	
								<th>Marca</th>	
								<th>Propiedad</th>												
								<th>Opción</th>									
							</thead>               
							<tr ng-repeat="item in objectModels.list">									
								<td>@{{$index+1}}</td>								
								<td>@{{item.name}}</td>
								<td>@{{item.description}}</td>
								<td>@{{item.brand}}</td>
								<td>@{{item.property}}</td>
								<td>
									<a href="#" ng-click="loadObjectModel(item);" data-target="#modal-objectModel" data-toggle="modal" class="btn btn-warning btn-xs"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
									<a href="#" ng-click="loadObjectModel(item);" data-toggle="modal"  data-target="#modal-objectModel-delete" class="btn btn-danger btn-xs"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
								</td>
							</tr>									
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
	@include ('objectModels.modal-create')
	@include ('objectModels.modal-delete')
@endsection
@section ('javascript')
<script src="{{asset('js/controllers/objectModelsController.js')}}"></script>
@endsection