@extends('adminlte::layouts.app')
@section('htmlheader_title')
	| Proveedor
@endsection
@section('contentheader_title')
	Proveedores <a href="#" ng-click="newClient();" data-target="#modal-client" data-toggle="modal" class="btn btn-success btn-xs">Nuevo</a>
@endsection
@section('angularcontroller')
ng-controller="providerController" 
@endsection


@section('main-content')
	<div class="container-fluid spark-screen" ng-cloak>
		<script>
			function list(element){
				var nueva_pagina;
				if(element!=undefined) {
					nueva_pagina=$(element).attr('data-id');
					angular.element("#providerController").scope().setPagina('clients','filters',nueva_pagina);
					angular.element('#providerController').scope().listClient();
				}
			}
		</script>
		<div class="row">
			<div class="col-md-12">
				<div class="panel panel-default">
					<div class="panel-heading">Listado de Proveedores</div>
					<div class="row table-filters ">
						<div class="col-md-4">
							<input type="text" ng-model="clients.filters.filtro" placeholder="Buscar" class="form-control" ng-enter="clients.filters.pagina=1;listClient($event)"/>
						</div>
						<div class="col-md-2">
							<div class="form-inline">
								<div class="input-group date">
									<span class="input-group-addon"><i class="fa fa-calendar"></i></span> <input type="text"  value="clients.filters.dateinit"  ng-model="clients.filters.dateinit" placeholder="#{msg['Fecha_Inicio']}" class="form-control dateinput input-append date" />
								</div>
							</div>
						</div>
						<div class="col-md-2">
							<div class="form-inline">
								<div class="input-group date">
									<span class="input-group-addon"><i class="fa fa-calendar"></i></span> <input type="text" value="clients.filters.dateend" ng-model="clients.filters.dateend" placeholder="#{msg['Fecha_Final']}" class="form-control dateinput" />
								</div>
							</div>
						</div>
						<div class="col-md-3 ">
							<button class="btn btn-info btn-xs" data-loading-text="Please wait..." ng-click="clients.filters.pagina=1;listClient($event)"><i class="fa fa-search"></i> Buscar</button>
							<button class="btn btn-success btn-xs"  data-loading-text="Please wait..."  ng-click="exportAll(event);"><i class="fa fa-file-excel-o"></i> Exportar</button>

						</div>

					</div>
					<div class="panel-body" id="providerController">
						<table class="table table-striped table-bordered table-condensed table-hover">
							<thead>
								<th>N°</th>
								<th>Nombre</th>
								<th>Apellido</th>
								<th>Documento</th>	
								<th>DNI/RUC</th>	
								<th>Direccion</th>				
								<th>Telefono</th>								
								<th>Accion</th>									
							</thead>               
							<tr ng-repeat="item in clients.list">									
								<td>@{{$index+1}}</td>
								<td>@{{item.name}}</td>
								<td>@{{item.firstname}}</td>
								<td>@{{item.document}}</td>
								<td>@{{item.identitynumber}}</td>
								<td>@{{item.address}}</td>
								<td>@{{item.phone}}</td>		
								<td>
									<a href="#" ng-click="loadClient(item);" data-target="#modal-client" data-toggle="modal" class="btn btn-warning btn-xs"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
									<a href="#" ng-click="loadClient(item);" data-toggle="modal"  data-target="#modal-client-delete" class="btn btn-danger btn-xs"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
								</td>
							</tr>									
						</table>
						<div id="paginador_Table"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
	@include ('provider.modal-create')
	@include ('provider.modal-delete')
@endsection
@section ('javascript')
<script src="{{asset('js/controllers/providerController.js')}}"></script>
@endsection