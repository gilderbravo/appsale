<div class="modal fade modal-slide-in-right" aria-hidden="true" role="dialog" tabindex="-1" id="modal-client">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" 
				aria-label="Close">
                     <span aria-hidden="true">×</span>
                </button>				
                <h4 class="modal-title"> Proveedor</h4>
			</div>
			<div class="modal-body">
				<div class="col-md-12">
						<div class="col-md-6">	
							<div class="form-group">
								<label class="col-md-4 control-label">Tipo Doc.</label>
								<div class="col-md-8">
									<select id="cmbIdentityDocument" class="form-control " ng-model="client.identitydocumentid" ng-change="validateTypeIdentity(client.identitydocumentid);" >										
										<option ng-repeat="item in identityDocuments.list" value="@{{item.id}}" >@{{item.name}}</option>
									</select>													
								</div>
							</div>
							<br /><br />
							<div class="form-group">
								<label class="col-md-4 control-label">DNI/RUC</label>
								<div class="col-md-8">
									<input type="text" id="txtIdentityNumber" class="form-control" ng-model="client.identitynumber" onkeypress="return validateInputNumber(event)" ng-enter="searchDNIRUC(client.identitynumber,client.identitydocumentid);" >
								</div>
							</div>
							<br /><br />
							<div class="form-group">
								<label class="col-md-4 control-label" id="lblName">Nombre</label>
								<div class="col-md-8">
									<input class="form-control" id="txtname" type="text" ng-model="client.name">
								</div>
							</div>

							<br /><br />
							<div class="form-group">
									<label class="col-md-4 control-label" id="lblfirstname">Apellidos</label>
									<div class="col-md-8">
										<input class="form-control" id="txtFirstname" type="text" ng-model="client.firstname">												
									</div>
							</div>
							<br /><br />
							<div class="form-group">
								<label class="col-md-4 control-label">Direccion</label>
								<div class="col-md-8">
									<input class="form-control" type="text" ng-model="client.address">												
								</div>
							</div>

						</div>

						<div class="col-md-6">
							<div class="form-group">
								<label class="col-md-4 control-label">Telefono</label>
								<div class="col-md-8">
									<input type="text" class="form-control" ng-model="client.phone" onkeypress="return validateInputNumber(event)" maxlength="9">
								</div>
							</div>
							<br /><br />
							<div class="form-group">
								<label class="col-md-4 control-label">Fecha Nacimiento</label>
								<div class="col-md-8">
										<input type="text" class="form-control date" ng-model="client.birthday">
								</div>
							</div>	
							<br /><br />
							<div class="form-group">
								<label class="col-md-4 control-label">Ciudad</label>
								<div class="col-md-8">
									<input type="text" class="form-control" ng-model="client.cyty">												
								</div>
							</div>		

						</div>						
				</div>
				
			</div>
			
			<div class="modal-foot">					
				<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
				<button type="submit" class="btn btn-primary" data-loading-text="Espere Por Favor..." ng-click="saveClient($event);"  ng-disabled="ClientForm.$invalid" id="submit-all">Confirmar</button>
			</div>
		</div>
	</div>
	
</div>