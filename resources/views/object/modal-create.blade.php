<div class="modal fade modal-slide-in-right" aria-hidden="true" role="dialog" tabindex="-1" id="modal-object">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" 
				aria-label="Close">
                     <span aria-hidden="true">×</span>
                </button>				
                <h4 class="modal-title"> Productos</h4>
			</div>
			<div class="modal-body">
				<div class="col-md-12">
						<div class="col-md-6">								
							<div class="form-group">
								<label class="col-md-4 control-label">Codigo</label>
								<div class="col-md-8">
									<input class="form-control" type="text" ng-model="object.code">												
								</div>
							</div>
							<br /><br />
							<div class="form-group">
									<label class="col-md-4 control-label">Producto</label>
									<div class="col-md-8">
										<input class="form-control" type="text" ng-model="object.name">												
									</div>
							</div>
							<br /><br />
							<div class="form-group">
								<label class="col-md-4 control-label">Categoria</label>
								<div class="col-md-8">
									<select id="cmbCategory" class="form-control" ng-model="object.objectcategoryid" >
										<option ng-repeat="item in categories.list" value="@{{item.id}}">@{{item.name}}</option>
									</select>													
								</div>
							</div>
							<br /><br />
							<div class="form-group">
								<label class="col-md-4 control-label">Precio Venta</label>
								<div class="col-md-8">
									<input type="text" class="form-control" ng-model="object.pricesale">												
								</div>
							</div>
							<br /><br />
							<div class="form-group">
								<label class="col-md-4 control-label">Descripcion</label>
								<div class="col-md-8">
									<textarea class="form-control"  rows="2" ng-model="object.description"></textarea>
								</div>
							</div>							
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label class="col-md-4 control-label">Codigo Barra</label>
								<div class="col-md-8">
									<input type="text" class="form-control" ng-model="object.barcode">												
								</div>
							</div>
							<br /><br />
							<div class="form-group">
								<label class="col-md-4 control-label">Presentacion</label>
								<div class="col-md-8">
									<select id="cmbPresentation" class="form-control " ng-model="object.presentationobjectid" >
										<option ng-repeat="item in presentations.list" value="@{{item.id}}">@{{item.name}}</option>
									</select>													
								</div>
							</div>	
							<br /><br />							
							<div class="form-group">
								<label class="col-md-4 control-label">Modelo</label>
								<div class="col-md-8">
									<select id="cmbModel" class="form-control" ng-model="object.objectmodelid" >
										<option ng-repeat="item in objectModels.list" value="@{{item.id}}">@{{item.name}}</option>
									</select>													
								</div>
							</div>	
							<br /><br />
							<div class="form-group">
								<label class="col-md-4 control-label">Precio Compra</label>
								<div class="col-md-8">
									<input type="text" class="form-control" ng-model="object.pricepurchase">												
								</div>
							</div>		

						</div>						
				</div>
				
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
				<button type="submit" class="btn btn-primary" ng-click="saveObject();" data-dismiss="modal" ng-disabled="ObjectForm.$invalid" id="submit-all">Confirmar</button>
			</div>
		</div>
	</div>
	
</div>