@extends('adminlte::layouts.app')
@section('htmlheader_title')
	| Productos
@endsection
@section('contentheader_title')
	PRODUCTOS <a href="#" ng-click="newObject();" data-target="#modal-object" data-toggle="modal" class="btn btn-success btn-xs">Nuevo</a>
@endsection
@section('angularcontroller')
ng-controller="objectController" 
@endsection


@section('main-content')
	<div class="container-fluid spark-screen" ng-cloak>
		<div class="row">
			<div class="col-md-12">
				<div class="panel panel-default">
					<div class="panel-heading">Listado de productos
						
					</div>
					
					<div class="panel-body">
						<table class="table table-striped table-bordered table-condensed table-hover">
							<thead>
								<th>N°</th>
								<th>Codigo</th>
								<th>Nombre</th>
								<th>Descripcion</th>	
								<th>Presentacion</th>	
								<th>Categoria</th>				
								<th>Modelo</th>								
								<th>Accion</th>									
							</thead>               
							<tr ng-repeat="item in objects.list">									
								<td>@{{$index+1}}</td>
								<td>@{{item.code}}</td>
								<td>@{{item.name}}</td>
								<td>@{{item.description}}</td>
								<td>@{{item.presentation}}</td>
								<td>@{{item.category}}</td>
								<td>@{{item.model}}</td>		
								<td>
									<a href="#" ng-click="loadObject(item);" data-target="#modal-object" data-toggle="modal" class="btn btn-warning btn-xs"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
									<a href="#" ng-click="loadObject(item);" data-toggle="modal"  data-target="#modal-object-delete" class="btn btn-danger btn-xs"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
								</td>
							</tr>									
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
	@include ('object.modal-create')
	@include ('object.modal-delete')
@endsection
@section ('javascript')
<script src="{{asset('js/controllers/objectController.js')}}"></script>
@endsection