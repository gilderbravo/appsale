<div class="modal fade modal-slide-in-right" aria-hidden="true" role="dialog" tabindex="-1" id="modal-fee">
	<div class="modal-dialog modal-size">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" 
				aria-label="Close">
                     <span aria-hidden="true">×</span>
                </button>				
                <h4 class="modal-title"> Cierre de Ahorros</h4>
			</div>
			<div class="modal-body">
				<div class="col-md-12">
					<div class="col-md-6">								
						<label class="col-sm-4 ">Cliente</label>
						<label class="col-sm-4 ">@{{fee.clientname}}</label>
					</div>
					<div class="col-md-6">								
						<label class="col-sm-3 ">Cantidad Ahorrado</label>
						<label class="col-sm-4 ">@{{fee.totalpayment}}</label>
					</div>
					<div class="col-md-6">								
						<label class="col-sm-3 ">Interes</label>
						<div class="col-sm-4">
							<input type="text"  value="fee.interest"  ng-model="fee.interest" class="form-control " ng-blur="addDuestoFee(fee.interest);" />
						</div>																
					</div>
													 					
					<div class="col-md-6">								
						<label class="col-sm-4 ">Total</label>
						<label class="col-sm-4 ">@{{fee.amount }}</label>														
					</div>								 					
				</div>
		</div>
			<div class="modal-foot ">
				<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
				<button type="submit" class="btn btn-primary" ng-click="saveFees();" data-dismiss="modal" ng-disabled="BrandForm.$invalid" id="submit-all">Confirmar</button>
			</div>
		</div>
	</div>
	
</div>