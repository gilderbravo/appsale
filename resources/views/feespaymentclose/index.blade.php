@extends('adminlte::layouts.app')
@section('htmlheader_title')
	| Cierre de Ahorros
@endsection
@section('contentheader_title')
	Cierre de Ahorros
@endsection
@section('angularcontroller')
ng-controller="feespaymentcloseController" 
@endsection


@section('main-content')
	<div class="container-fluid spark-screen" >
	<script>
	function list(element){
		var nueva_pagina;
		if(element!=undefined) {
			nueva_pagina=$(element).attr('data-id');
			angular.element("#feespaymentcloseController").scope().setPagina('fees','filters',nueva_pagina);
			angular.element('#feespaymentcloseController').scope().listFees();
		}
	}
	</script>
		<div class="row">
			<div class="col-md-12">
				<div class="panel panel-default">
					<div class="panel-heading">LISTADO AHORRO </div>
					<div class="row table-filters ">
							<div class="col-md-4">
								<input type="text" ng-model="fees.filters.filtro" placeholder="Buscar" class="form-control" ng-enter="fees.filters.pagina=1;listFees($event)"/>
							</div>				
							<div class="col-md-2">									
								<div class="form-inline">
									<div class="input-group date">
										<span class="input-group-addon"><i class="fa fa-calendar"></i></span> <input type="text"  value="fees.filters.dateinit"  ng-model="fees.filters.dateinit" placeholder="#{msg['Fecha_Inicio']}" class="form-control dateinput input-append date" />
									</div>
								</div>
							</div>
							<div class="col-md-2">								
								<div class="form-inline">
									<div class="input-group date">
										<span class="input-group-addon"><i class="fa fa-calendar"></i></span> <input type="text" value="fees.filters.dateend" ng-model="fees.filters.dateend" placeholder="#{msg['Fecha_Final']}" class="form-control dateinput" />
									</div>
								</div>
							</div>													
							<div class="col-md-3 ">										
								<button class="btn btn-info btn-xs" data-loading-text="Please wait..." ng-click="fees.filters.pagina=1;listFees($event)"><i class="fa fa-search"></i> Buscar</button>
								<button class="btn btn-success btn-xs"  data-loading-text="Please wait..."  ng-click="exportAll(event);"><i class="fa fa-file-excel-o"></i> Exportar</button>

							</div>
							
					</div>
					
					<div class="panel-body" id="feespaymentcloseController">
					<div class="wrapper-mobile">
						<div class="table-mobile">
							<div class="row-mobile header-mobile">
							<div class="cell-mobile">Accion</div>
							<div class="cell-mobile">Tipo Operacion</div>
							<div class="cell-mobile">Fecha</div>
							<div class="cell-mobile">N° Doc</div>
							<div class="cell-mobile">Cliente</div>
							<div class="cell-mobile">Monto</div>
							<div class="cell-mobile">Cantidad Pagada</div>
							<div class="cell-mobile">Estado</div>
							</div>
							<div class="row-mobile" ng-repeat="item in fees.list">
								<div class="cell-mobile" data-title="Accion" style="width: 180px;">
										<a href="#"  ng-click="loadFees(item);" data-target="#modal-fee" data-toggle="modal" class="btn btn-success btn-xs">
										<i class="fa fa-list" aria-hidden="true"></i>
										</a>
								</div>
								<div class="cell-mobile" data-title="tipo">@{{item.transactiontypeid==1?'Creditos':'JuntaMas'}}</div>
								<div class="cell-mobile" data-title="Fecha">@{{item.date}}</div>
								<div class="cell-mobile" data-title="N° Doc">@{{item.code}}</div>
								<div class="cell-mobile" data-title="Cliente">@{{item.clientname}}</div>
								<div class="cell-mobile" data-title="Monto">@{{item.total | number:2}}</div>
								<div class="cell-mobile" data-title="Cantidad Pagada">@{{item.totalpayment  | number: 2}}</div>
								<div class="cell-mobile" data-title="Estado">@{{item.state}}</div>
							</div>
						</div>
						<div id="paginador_Table"></div>
						</div>				
		
					</div>
				</div>
			</div>
		</div>
	</div>
	@include ('feespaymentclose.modal-create')
	@include ('feespaymentclose.modal-print')
@endsection
@section ('javascript')
<script src="{{asset('js/controllers/feespaymentcloseController.js')}}"></script>
@endsection