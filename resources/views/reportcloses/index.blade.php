@extends('adminlte::layouts.app')
@section('htmlheader_title')
	| Cierre de Transacciones
@endsection
@section('contentheader_title')
	Cierre de Transacciones
@endsection
@section('angularcontroller')
ng-controller="reportclosesController"
@endsection

@section('main-content')
	<div class="container-fluid spark-screen" >
	<script>
	function list(element){
		var nueva_pagina;
		if(element!=undefined) {
			nueva_pagina=$(element).attr('data-id');
			angular.element("#reportclosesController").scope().setPagina('transactions','filters',nueva_pagina);
			angular.element('#reportclosesController').scope().listReportPayments();
		}
	}
	</script>
		<div class="row">
			<div class="col-md-12">
				<div class="panel panel-default">
					<div class="panel-heading">REPORTE DE CIERRE  POR FECHAS</div>
					<div class="row table-filters ">
							<div class="col-md-4">
								<input type="text" ng-model="transactions.filters.filtro" placeholder="Buscar" class="form-control" ng-enter="transactions.filters.pagina=1;listReportPayments($event)"/>
							</div>				
							<div class="col-md-2">									
								<div class="form-inline">
									<div class="input-group date">
										<span class="input-group-addon"><i class="fa fa-calendar"></i></span> <input type="text"  value="transactions.filters.dateinit"  ng-model="transactions.filters.dateinit" placeholder="#{msg['Fecha_Inicio']}" class="form-control dateinput input-append date" />
									</div>
								</div>
							</div>
							<div class="col-md-2">								
								<div class="form-inline">
									<div class="input-group date">
										<span class="input-group-addon"><i class="fa fa-calendar"></i></span> <input type="text" value="transactions.filters.dateend" ng-model="transactions.filters.dateend" placeholder="#{msg['Fecha_Final']}" class="form-control dateinput" />
									</div>
								</div>
							</div>													
							<div class="col-md-3 ">										
								<button class="btn btn-info btn-xs" data-loading-text="Please wait..." ng-click="transactions.filters.pagina=1;listReportPayments($event)"><i class="fa fa-search"></i> Buscar</button>
								<button class="btn btn-success btn-xs"  data-loading-text="Please wait..."  ng-click="exportAll('exportReportPayments','reportepagos',$event);"><i class="fa fa-file-excel-o"></i> Exportar</button>

							</div>
							
					</div>
					<div class="panel-body" id="reportclosesController" >
						<table class="table table-striped table-bordered table-condensed table-hover " id="export">
							<thead>
								<th>Fecha</th>
								<th>N° Cierre</th>								
								<th>DNI</th>
								<th>Cliente</th>
								<th>Empleado</th>
								<th>N° Comprobante</th>
								<th>Capital</th>
								<th>Cantidad</th>
								<th>Interes</th>
								<th>Total</th>
								<th></th>
							</thead>
							<tbody>
								<tr ng-repeat="item in transactions.list">									
									<td>@{{item.issedtime}}</td>
									<td>@{{item.code}}</td>
									<td>@{{item.identitynumber}}</td>
									<td>@{{item.client}}</td>
									<td>@{{item.name}}</td>
									<td>@{{item.codeorigin}}</td>
									<td>@{{item.capital}}</td>
									<td>@{{item.amount}}</td>
									<td>@{{item.interest}}</td>
									<td>@{{item.total}}</td>
									@if ( Auth::user()->positionid==1)
									<td><a href="#" ng-click="prepareDelete(item);" class="btn btn-danger btn-xs"><i class="fa fa-trash-o" aria-hidden="true"></i></a></td>
									@endif
								</tr>	
							</tbody>	
							<tfoot>
								<tr>									
									<td></td><td></td><td></td><td></td><td></td><td></td><td>Total</td><td>@{{totalamount | number:2}}</td>
								</tr>
							</tfoot>							
						</table>
						<div id="paginador_Table"></div>
					</div>
					<div ng-show="export">
						<table class="table table-striped table-bordered table-condensed table-hover " id="exportReportPayments">
							<thead>
							<th>Fecha</th>
								<th>N° Cierre</th>								
								<th>DNI</th>
								<th>Cliente</th>
								<th>Empleado</th>
								<th>N° Comprobante</th>
								<th>Capital</th>
								<th>Cantidad</th>
								<th>Interes</th>
								<th>Total</th>
								
							</thead>
							<tbody>
								<tr ng-repeat="item in transactions.listall">
								<td>@{{item.issedtime}}</td>
									<td>@{{item.code}}</td>
									<td>@{{item.identitynumber}}</td>
									<td>@{{item.client}}</td>
									<td>@{{item.name}}</td>
									<td>@{{item.codeorigin}}</td>
									<td>@{{item.capital}}</td>
									<td>@{{item.amount}}</td>
									<td>@{{item.interest}}</td>
									<td>@{{item.total}}</td>
								</tr>
							</tbody>
							<tfoot>
								<tr>									
									<td></td><td></td><td></td><td></td><td></td><td></td><td>Total</td><td>@{{totalamount | number:2}}</td>
								</tr>
							</tfoot>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection
@section ('javascript')
<script src="{{asset('js/controllers/reportclosesController.js')}}"></script>
@endsection