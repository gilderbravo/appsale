<div class="modal fade modal-slide-in-right" aria-hidden="true" role="dialog" tabindex="-1" id="modal-propertyObjects">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" 
				aria-label="Close">
                     <span aria-hidden="true">×</span>
                </button>				
                <h4 class="modal-title"> Propiedades </h4>
			</div>
			<div class="modal-body">
			 
					<div class="form-group">
						<label>Nombre</label>
						<input class="form-control" placeholder="Nombre..." ng-model="property.name" required>
					</div>      
					<div class="form-group">
						<label>Descripción</label>
						<textarea class="form-control" rows="2" ng-model="property.description"></textarea>				
					</div>  					
					
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
				<button type="submit" class="btn btn-primary" ng-click="savePropertyObjects();" data-dismiss="modal" ng-disabled="PropertyForm.$invalid" id="submit-all">Confirmar</button>
			</div>
		</div>
	</div>
	
</div>