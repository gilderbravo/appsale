@extends('adminlte::layouts.app')
@section('htmlheader_title')
	| Propiedades del Objeto
@endsection
@section('contentheader_title')
	Propiedades <a href="#" ng-click="newPropertyObjects();" data-target="#modal-propertyObjects" data-toggle="modal" class="btn btn-success btn-xs">Nuevo</a>
@endsection
@section('angularcontroller')
ng-controller="propertyObjectsController" 
@endsection
@section('main-content')
	<div class="container-fluid spark-screen" ng-cloak>
		<div class="row">
			<div class="col-md-12">
				<div class="panel panel-default">
					<div class="panel-heading">Listado de Propiedades
						
					</div>
					
					<div class="panel-body">
						<table class="table table-striped table-bordered table-condensed table-hover">
							<thead>
								<th>Id</th>
								<th>Nombre</th>	
								<th>Descripción</th>				
								<th>Opcion</th>									
							</thead>               
							<tr ng-repeat="item in properties.list">									
								<td>@{{item.id}}</td>
								<td>@{{item.name}}</td>
								<td>@{{item.description}}</td>		
								<td>
									<a href="#" ng-click="loadPropertyObjects(item);" data-target="#modal-propertyObjects" data-toggle="modal" class="btn btn-warning btn-xs"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
									<a href="#" ng-click="loadPropertyObjects(item);" data-toggle="modal"  data-target="#modal-propertyObjects-delete" class="btn btn-danger btn-xs"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
								</td>
							</tr>									
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
	@include ('propertyObjects.modal-create')
	@include ('propertyObjects.modal-delete')
@endsection
@section ('javascript')
<script src="{{asset('js/controllers/propertyObjectsController.js')}}"></script>
@endsection