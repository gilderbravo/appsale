<div class="modal fade modal-slide-in-right" aria-hidden="true" role="dialog" tabindex="-1" id="modal-payments">
	<div class="modal-dialog modal-md">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"
						aria-label="Close">
					<span aria-hidden="true">×</span>
				</button>
				<h4 class="modal-title"> Pagos Realizados</h4>
			</div>
			<div class="modal-body">
				<div class="col-md-12">
					<div class="wrapper-table-detail " >
						<table class='table table-striped table-bordered table-condensed table-hover' >
							<thead><tr><th>Cuota N°</th><th>Fecha Pago</th><th>Fecha de Cronograma</th><th class="col-price">Monto </th><th>Accion</th></tr></thead>
							<tbody class="datails-tbody">
							<tr ng-repeat="item in payments.list"  >
								<td class="money">@{{item.numberdues }}</td>
								<td class="money">@{{item.issedtime }}</td>
								<td class="money">@{{item.time }}</td>
								<td class="money">@{{(item.advancement>0?item.advancement:item.feeamount) |number:2}}</td>
								<td>
									<a href="#" ng-click="prepareDelete(item);" class="btn btn-danger btn-xs"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
								</td>
							</tr>
							</tbody>
							<tfoot class="amount-right">
							<tr><td></td><td></td><td class="texto" >Total:</td><td class="numberPrint">@{{payments.feeamount|number:2}}</td></tr>
							</tfoot>
						</table>
					</div>
				</div>
			</div>
			<div class="modal-foot ">
				<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
			</div>
		</div>
	</div>

</div>