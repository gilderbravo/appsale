@extends('adminlte::layouts.app')
@section('htmlheader_title')
	| Balance de Pagos Del Día
@endsection
@section('contentheader_title')
	Balance de Pagos Del Día
@endsection
@section('angularcontroller')
ng-controller="balancesheetController"
@endsection

@section('main-content')
	<div class="container-fluid spark-screen" >
	<script>
	function list(element){
		var nueva_pagina;
		if(element!=undefined) {
			nueva_pagina=$(element).attr('data-id');
			angular.element("#balancesheetController").scope().setPagina('transactions','filters',nueva_pagina);
			angular.element('#balancesheetController').scope().listReportPayments();
		}
	}
	</script>
		<div class="row">
			<div class="col-md-12">
				<div class="panel panel-default">
					<div class="panel-heading">Balance Del Día</div>
					<div class="row table-filters ">
							<div class="col-md-4">
								<input type="text" ng-model="transactions.filters.filtro" placeholder="Buscar" class="form-control" ng-enter="transactions.filters.pagina=1;listReportPayments($event)"/>
							</div>				
							<div class="col-md-2">									
								<div class="form-inline">
									<div class="input-group date">
										<span class="input-group-addon"><i class="fa fa-calendar"></i></span> <input type="text"  value="transactions.filters.dateinit"  ng-model="transactions.filters.dateinit" placeholder="#{msg['Fecha_Inicio']}" class="form-control dateinput input-append date" />
									</div>
								</div>
							</div>
							<div class="col-md-2">								
								<div class="form-inline">
									<div class="input-group date">
										<span class="input-group-addon"><i class="fa fa-calendar"></i></span> <input type="text" value="transactions.filters.dateend" ng-model="transactions.filters.dateend" placeholder="#{msg['Fecha_Final']}" class="form-control dateinput" />
									</div>
								</div>
							</div>													
							<div class="col-md-3 ">										
								<button class="btn btn-info btn-xs" data-loading-text="Please wait..." ng-click="transactions.filters.pagina=1;listReportPayments($event)"><i class="fa fa-search"></i> Buscar</button>
								<button class="btn btn-success btn-xs"  data-loading-text="Please wait..."  ng-click="exportAll('exportReportPayments','reportepagos',$event);"><i class="fa fa-file-excel-o"></i> Exportar</button>

							</div>
							
					</div>
					<div class="panel-body" id="reportpaymentsController" >
						<table class="table table-striped table-bordered table-condensed table-hover " id="export">
							<thead>
								<th>Fecha</th>
								<th>Tipo Operacion</th>
								<th>Pago</th>
								<th>Cobrador</th>
								<th>N Pago</th>
								<th>Cliente</th>
								<th>N Comprobante</th>
								<th>Ingreso</th>
								
							</thead>
							<tbody>
								<tr ng-repeat="item in transactions.list">									
									<td>@{{item.date}}</td>
									<td>Pago</td>
									<td>@{{item.accountant != 0?'Contable':'Regulado' }}</td>
									<td>@{{item.systemuser}}</td>
									<td>@{{item.codefee}}</td>
									<td>@{{item.client}}</td>
									<td>@{{item.code}}</td>
									<td>@{{item.cash}}</td>
								
								</tr>	
							</tbody>	
							<tfoot>
								<tr>									
									<td></td><td></td><td></td><td></td><td></td><td></td><td>Total</td><td>@{{totalfeeamount | number:2}}</td>
								</tr>
							</tfoot>							
						</table>
						<div id="paginador_Table"></div>
					</div>
					<div ng-show="export">
						
						<table class="table table-striped table-bordered table-condensed table-hover " id="exportReportPayments">
							
							<thead >									
								<td style="text-align: center"><h1>INGRESOS</h1></td>
							</thead>
							<thead >									
								<td style="text-align: center"><h6>JUNTA MAS DIARIOS</h6></td>
							</thead>
							<thead >
								<th>Fecha</th><th>Tipo Operacion</th><th>Pago</th><th>Cobrador</th><th>N Pago</th><th>Cliente</th><th>N Comprobante</th><th>Ingreso</th><th> Generado </th>								
							</thead>
							<tbody>
								<tr ng-repeat="item in transactions.juntamas">
									<td style="text-align: center">@{{item.date}}</td>
									<td style="text-align: center">Pago</td>
									<td>@{{item.accountant != 0?'Contable':'Regulado' }}</td>
									<td>@{{item.systemuser}}</td>
									<td style="text-align: center">@{{item.codefee}}</td>
									<td>@{{item.client}}</td>
									<td style="text-align: center">@{{item.code}}</td>
									<td style="text-align: center">@{{item.cash}}</td>
									<td style="text-align: center">@{{item.type}}</td>
								</tr>	
								<tr>									
									<td></td><td></td><td></td><td></td><td></td><td></td><td>Sub Total</td><td>@{{totaljuntamasfeeamount | number:2}}</td>
								</tr>
								<tr >									
									<td style="text-align: center"><h6>CANASTA DIARIOS</h6></td>
								</tr>							
								<tr>
									<th>Fecha</th><th>Tipo Operacion</th><th>Pago</th><th>Cobrador</th><th>N Pago</th><th>Cliente</th><th>N Comprobante</th><th> Ingreso </th><th>Generado</th>								
								</tr>
								<tr ng-repeat="item in transactions.canasta">
									<td style="text-align: center">@{{item.date}}</td>
									<td style="text-align: center">Pago</td>
									<td>@{{item.accountant != 0?'Contable':'Regulado' }}</td>
									<td>@{{item.systemuser}}</td>
									<td style="text-align: center">@{{item.codefee}}</td>
									<td>@{{item.client}}</td>
									<td style="text-align: center">@{{item.code}}</td>
									<td style="text-align: center">@{{item.cash}}</td>
									<td style="text-align: center">@{{item.type}}</td>
								</tr>
								<tr>									
									<td></td><td></td><td></td><td></td><td></td><td></td><td>Sub Total</td><td>@{{totalcanastafeeamount | number:2}}</td>
								</tr>	
								<tr >									
									<td style="text-align: center"><h6>COBRANZA DE CREDITOS</h6></td>
								</tr>							
								<tr>
									<th>Fecha</th><th>Tipo Operacion</th><th>Pago</th><th>Cobrador</th><th>N Pago</th><th>Cliente</th><th>N Comprobante</th><th>Ingreso </th><th>Generado</th>								
								</tr>
								<tr ng-repeat="item in transactions.listall">
									<td style="text-align: center">@{{item.date}}</td>
									<td style="text-align: center">Pago</td>
									<td>@{{item.accountant != 0?'Contable':'Regulado' }}</td>
									<td>@{{item.systemuser}}</td>
									<td style="text-align: center">@{{item.codefee}}</td>
									<td>@{{item.client}}</td>
									<td style="text-align: center">@{{item.code}}</td>
									<td style="text-align: center">@{{item.cash}}</td>
									<td style="text-align: center">@{{item.type}}</td>
								</tr>
								<tr>									
									<td></td><td></td><td></td><td></td><td></td><td></td><td>Sub Total</td><td>@{{totalcreditfeeamount | number:2}}</td>
								</tr>
								<tr >									
									<td style="text-align: center"><h6>OTROS INGRESOS</h6></td>
								</tr>							
								<tr>
									<th>Fecha</th><th>Tipo Operacion</th><th>Pago</th><th>Cobrador</th><th>N Pago</th><th>Descripcion</th><th>N Comprobante</th><th>Ingreso</th><th></th>								
								</tr>
								<tr ng-repeat="item in transactions.listOtros">
									<td style="text-align: center">@{{item.date}}</td>
									<td style="text-align: center">Abono</td>
									<td>@{{item.accountant != 0?'Contable':'Contable' }}</td>
									<td>@{{item.systemuser}}</td>
									<td style="text-align: center">@{{item.code}}</td>
									<td>@{{item.comment}}</td>
									<td style="text-align: center"></td>
									<td style="text-align: center">@{{item.total}}</td>
									<td style="text-align: center"></td>
								</tr>
								<tr>									
									<td></td><td></td><td></td><td></td><td></td><td></td><td>Sub Total</td><td>@{{totalotrosamount | number:2}}</td>
								</tr>
								<tr>									
									<td></td><td></td><td></td><td></td><td></td><td></td><td>Total Ingresos</td><td>@{{totalotrosamount+totalcanastafeeamount + totaljuntamasfeeamount + totalcreditfeeamount | number:2}}</td>
								</tr>
								<tr >									
									<td style="text-align: center"><h1>EGRESOS</h1></td>
								</tr>
								<tr >									
									<td style="text-align: center"><h6>DEVOLUCION DE JUNTA MAS Y CANASTA</h6></td>
								</tr>							
								<tr>
									<th>Fecha</th><th>Tipo Operacion</th><th>Pago</th><th>Cobrador</th><th>N Pago</th><th>Descripcion</th><th>N Comprobante</th><th>Ingreso</th><th></th>								
								</tr>
								<tr ng-repeat="item in transactions.listdevolucion">
									<td style="text-align: center">@{{item.date}}</td>
									<td style="text-align: center">Egreso</td>
									<td>@{{item.accountant != 0?'Contable':'Contable' }}</td>
									<td>@{{item.systemuser}}</td>
									<td style="text-align: center">@{{item.code}}</td>
									<td>@{{item.comment}}</td>
									<td style="text-align: center"></td>
									<td style="text-align: center">@{{item.total}}</td>
									<td style="text-align: center">@{{item.commenttransaction}}</td>
								</tr>
								<tr>									
									<td></td><td></td><td></td><td></td><td></td><td></td><td>Sub Total</td><td>@{{totaldevamount | number:2}}</td>
								</tr>
								<tr >									
									<td style="text-align: center"><h6>DESEMBOLSO DE CREDITOS</h6></td>
								</tr>							
								<tr>
									<th>Fecha</th><th>Tipo Operacion</th><th>Pago</th><th>Cobrador</th><th>N Pago</th><th>Descripcion</th><th>N Comprobante</th><th>Ingreso</th><th></th>								
								</tr>
								<tr ng-repeat="item in transactions.listdesembolso">
									<td style="text-align: center">@{{item.date}}</td>
									<td style="text-align: center">Egreso</td>
									<td>@{{item.accountant != 0?'Contable':'Contable' }}</td>
									<td>@{{item.systemuser}}</td>
									<td style="text-align: center">@{{item.code}}</td>
									<td>@{{item.comment}}</td>
									<td style="text-align: center"></td>
									<td style="text-align: center">@{{item.total}}</td>
									<td style="text-align: center">@{{item.commenttransaction}}</td>
								</tr>
								<tr>									
									<td></td><td></td><td></td><td></td><td></td><td></td><td>Sub Total</td><td>@{{totaldesamount | number:2}}</td>
								</tr>
								<tr >									
									<td style="text-align: center"><h6>CIERRE DE AHORROS</h6></td>
								</tr>							
								<tr>
									<th>CODIGO</th><th>FECHA</th><th>CLIENTE</th><th>N Comprobante</th><th>EMPLEADO</th><th>TOTAL AHORRO</th><th>INTERES GENERADO</th><th>TOTAL</th>								
								</tr>
								<tr ng-repeat="item in transactions.transactioncloses">
									<td style="text-align: center">@{{item.code}}</td>
									<td style="text-align: center">@{{item.issedtime}}</td>
									<td>@{{item.client}}</td>
									<td>@{{item.codeorigin}}</td>
									<td style="text-align: center">@{{item.name}}</td>
									<td style="text-align: center">@{{item.amount}}</td>
									<td style="text-align: center">@{{item.interest}}</td>
									<td style="text-align: center">@{{item.total}}</td>
								</tr>
								<tr>									
									<td></td><td></td><td></td><td></td><td></td><td></td><td>Total</td><td>@{{transactioncloseamount | number:2}}</td>
								</tr>

								<tr >									
									<td style="text-align: center"><h6>OTROS</h6></td>
								</tr>							
								<tr>
									<th>Fecha</th><th>Tipo Operacion</th><th>Pago</th><th>Cobrador</th><th>N Pago</th><th>Descripcion</th><th>N Comprobante</th><th>Ingreso</th><th></th>								
								</tr>
								<tr ng-repeat="item in transactions.listegresosotros">
									<td style="text-align: center">@{{item.date}}</td>
									<td style="text-align: center">Egreso</td>
									<td>@{{item.accountant != 0?'Contable':'Contable' }}</td>
									<td>@{{item.systemuser}}</td>
									<td style="text-align: center">@{{item.code}}</td>
									<td>@{{item.comment}}</td>
									<td style="text-align: center"></td>
									<td style="text-align: center">@{{item.total}}</td>
									<td style="text-align: center">@{{item.commenttransaction}}</td>
								</tr>
								<tr>									
									<td></td><td></td><td></td><td></td><td></td><td></td><td>Sub Total</td><td>@{{totalotrosegresosnamount | number:2}}</td>
								</tr>
								<tr>									
									<td></td><td></td><td></td><td></td><td></td><td></td><td>Total Egresos</td><td>@{{totaldevamount+totaldesamount+totalotrosegresosnamount | number:2}}</td>
								</tr>

							</tbody>
							<tfoot >
								<tr>									
									<td></td><td></td><td></td><td></td><td></td><td></td><td>Total</td><td>@{{(totalotrosamount+totalcanastafeeamount + totaljuntamasfeeamount + totalcreditfeeamount) - (totaldevamount+totaldesamount+totalotrosegresosnamount) | number:2}}</td>
								</tr>
								<tr style="text-align: center;border:2px;" >									
									<td style="text-align: center"><h1>MOVIMIENTOS</h1></td>
								</tr>							
								
								<tr >
									<td style="text-align: center">SALDO INICIAL</td>
									<td style="text-align: center">@{{(totalotrosamount+totalcanastafeeamount + totaljuntamasfeeamount + totalcreditfeeamount) - (totaldevamount+totaldesamount+totalotrosegresosnamount) | number:2}}</td>									
								</tr>
								<tr >
									<td style="text-align: center">SALDO ANTERIOR</td>
									<td style="text-align: center">@{{transactions.initamount | number:2}}</td>									
								</tr>
								<tr >
									<td style="text-align: center">HABILITACION GERENCIA</td>
									<td style="text-align: center">@{{transactions.totalqualificationamount | number:2}}</td>		
								</tr>
								<tr >
									<td style="text-align: center">DEPOSITO DIARIO</td>
									<td style="text-align: center">@{{transactions.totaldebitamount | number:2}}</td>		
								</tr>
								<tr >
									<td style="text-align: center">TOTAL SALDO DEL DIA</td>
									<td style="text-align: center">@{{finalamount | number:2}}</td>		
								</tr>
								<tr >									
									<td style="text-align: center"><h1>BOVEDA</h1></td>
								</tr>							
								<tr>
									<th>MONEDA U BILLETE</th><th>CANTIDAD</th><th>TOTAL</th>							
								</tr>
								<tr ng-repeat="item in transactions.money">
									<td style="text-align: center">@{{item.money}}</td>
									<td style="text-align: center">@{{item.amount}}</td>
									<td style="text-align: center">@{{item.total }}</td>
								</tr>
								<tr >
									<td style="text-align: center">TOTAL BOVEDA </td>
									<td style="text-align: center">@{{transactions.summoney | number:2}}</td>		
								</tr>
							</tfoot>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection
@section ('javascript')
<script src="{{asset('js/controllers/balancesheetController.js')}}"></script>
@endsection