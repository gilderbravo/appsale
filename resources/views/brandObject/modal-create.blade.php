<div class="modal fade modal-slide-in-right" aria-hidden="true" role="dialog" tabindex="-1" id="modal-brandObject">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" 
				aria-label="Close">
                     <span aria-hidden="true">×</span>
                </button>				
                <h4 class="modal-title"> Marca</h4>
			</div>
			<div class="modal-body">
			 
					<div class="form-group">
						<label>Nombre</label>
						<input class="form-control" placeholder="Nombre..." ng-model="brand.name" required>
					</div>      
					<div class="form-group">
						<label>Estado</label>
						<select class="form-control" ng-model="brand.status" placeholder="Seleccione">							
							<option  value="1" selected >Activo</option>
							<option value="0">Inactivo</option>
						</select>
						{{-- <input class="form-control" rows="2" ng-model="brand.status">				 --}}
					</div>  					
					
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
				<button type="submit" class="btn btn-primary" ng-click="saveBrandObject();"  ng-disabled="BrandForm.$invalid" id="submit-all">Confirmar</button>
			</div>
		</div>
	</div>
	
</div>