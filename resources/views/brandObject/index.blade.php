@extends('adminlte::layouts.app')
@section('htmlheader_title')
	| Productos
@endsection
@section('contentheader_title')
	MARCA DE PRODUCTOS <a href="#" ng-click="newBrandObject();" data-target="#modal-brandObject" data-toggle="modal" class="btn btn-success btn-xs">Nuevo</a>
@endsection
@section('angularcontroller')
ng-controller="objectController" 
@endsection


@section('main-content')
	<div class="container-fluid spark-screen" ng-cloak>
		<div class="row">
			<div class="col-md-12">
				<div class="panel panel-default">
					<div class="panel-heading">Listado de Marcas
						
					</div>
					
					<div class="panel-body">
						<table class="table table-striped table-bordered table-condensed table-hover " >
							<thead>
								<th style="width: 7%;text-align: center">N°</th>
								<th >Nombre</th>	
								{{-- <th>Estado</th>				 --}}
								<th style="width: 10%">Opción</th>									
							</thead>               
							<tr ng-repeat="item in listbrandobject.list">									
								<td style="text-align: center">@{{$index+1}}</td>
								<td>@{{item.name | uppercase}}</td>
								{{-- <td>@{{item.status}}</td>		 --}}
								<td>
									<a href="#" ng-click="loadBrandObject(item);" data-target="#modal-brandObject" data-toggle="modal" class="btn btn-warning btn-xs" title="Editar"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
									<a href="#" ng-click="loadBrandObject(item);" data-toggle="modal"  data-target="#modal-brandObject-delete" class="btn btn-danger btn-xs" title="Eliminar"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
								</td>
							</tr>									
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
	@include ('brandObject.modal-create')
	@include ('brandObject.modal-delete')
@endsection
@section ('javascript')
<script src="{{asset('js/controllers/objectController.js')}}"></script>
@endsection