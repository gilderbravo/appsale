<div class="modal fade modal-slide-in-right" aria-hidden="true" data-backdrop="static" data-keyboard="false" role="dialog" id="modal-sale">

	<div class="modal-dialog modal-size">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"
						aria-label="Close">
					<span aria-hidden="true">×</span>
				</button>
				<h4 class="modal-title"> Generar Plazo Fijo</h4>
				
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-md-12 ">
						<div class="input-group col-md-12" style="margin: 5px;">
							<span class="input-group-addon" id="basic-addon1">Cliente</span>
							<div class="col-md-12 " style="width: 300px;">								
								<select class="chosen-select form-select" id="sel_client" onChange="getvalclient(this)" style="width: 100%;">
								</select>
							</div>
							<span class="input-group-addon" id="basic-addon1">Monto</span>
							<input class="form-control" ng-model="payment.amount" ng-blur="changeAmount()">
							
							<span class="input-group-addon" id="basic-addon1">Cuotas</span>
							<input class="form-control" ng-model="payment.fees">

							<span class="input-group-addon" id="basic-addon1">Frecuencia en Dias:</span>
							<input class="form-control" ng-model="payment.frecuencyday">
							<span class="input-group-addon" id="basic-addon1">Tasa Efectiva:</span>
							<input class="form-control" ng-model="payment.tasa">
						</div>
					</div>
						<br>
					<div class="col-md-12 ">
							<div class="input-group col-md-12" style="margin: 5px;">
								<span class="input-group-addon" id="basic-addon1">F. Ahorro</span>
								<div class="input-group date" style=" width: 150px;">
									<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
									<input type="text"  ng-model="transaction.date" class="form-control dateinput input-append date" />
								</div>									
								<span class="input-group-addon" id="basic-addon1">F. Retiro</span>
								<div class="input-group date" style=" width: 150px;">
									<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
									<input type="text"  ng-model="payment.date" class="form-control dateinput input-append date" />
								</div>
								<span class="input-group-addon" id="basic-addon1">Colaborador</span>
								<select id="cmbemployed" style="width:120px;" class="form-control select2 " ng-model="transaction.secundaryemployeedid">
									<option ng-repeat="item in employeeds.list" value="@{{item.id}}">@{{item.name}}</option>
								</select>
								<span class="input-group-addon" id="basic-addon1">Tipo Ahorro</span>
								<select id="cmbSeason" class="form-control select2" ng-model="transaction.season">
									<option value="Plazo Fijo" selected="true">Plazo Fijo</option>
								</select>
								
							</div>
							<div>
								<a class="btn btn-primary btn-xs" ng-click="generatePayments()">Generar Pagos</a>
								<a class="btn btn-success btn-xs" ng-click="preparePrint()" >Imprimir</a>
							</div>
						
					</div>
					
					</div>				
				<div class="col-md-12" style="top: 10px;  background-color: lightblue; width: 100%;  height: 380px;  overflow: scroll;">
					<div class="wrapper-table-detail ">
						<table class='table table-striped table-bordered table-condensed table-hover' >
							<thead><tr><th>Cuota</th><th>Vencimiento</th><th>Capital</th><th>Interes</th><th>Monto Cuota</th><th>Dia</th></tr></thead>
							<tbody class="datails-tbody">
							<tr ng-repeat="item in payment.list"  >
								<td style="width: 20px">@{{$index+1}}</td>
								<td style="width: 20px">@{{ item.time }}</td>
								<td>@{{item.capital|number: 2}}</td>
								<td class='centered'>@{{item.interest|number: 2}}</td>
								<td>@{{item.feeamount|number: 2}}</td>
								<td class="amount-right">@{{item.day}}</td>
							</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>
			<div class="modal-footer ">
				<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
				<button type="submit" class="btn btn-primary"  data-loading-text="Espere Por Favor..."  ng-click="saveSale($event);" data-dismiss="modal" ng-disabled="BrandForm.$invalid" id="submit-all">Confirmar</button>
			</div>
		</div>
	</div>

</div>