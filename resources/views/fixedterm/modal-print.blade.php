<div class="modal fade modal-slide-in-right" aria-hidden="true" data-keyboard="false" role="dialog" tabindex="-1" id="modal-sale-print">
	<div class="modal-dialog modal-md">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" 
				aria-label="Close">
                     <span aria-hidden="true">×</span>
                </button>				
                <h4 class="modal-title"> Imprimir Pago	</h4>
			</div>
			<div class="modal-body">
				<div id="printfees" class="ticket-print" style="-webkit-transform:scale(0.85);">
					<div class="row" style="margin-left: -10px;">
						<div class="col-lg-12" style="padding: 0">
								<p style="margin:0;font-size:100%; text-align: center;font-family:Verdana, Geneva, sans-serif;font-weight: normal;padding-bottom: 5px;">DEYCAR</p>
								<p style="margin:0;font-size:80%; text-align: center;font-family:Verdana, Geneva, sans-serif;">RUC: 20600370155</p>
								<!-- <p style="margin:0;font-size:80%; text-align: center;font-family:Verdana, Geneva, sans-serif;">Dirección: Jr. Pucallpa N° 339</p>					                    			 -->
											
								<p style="margin:0;font-size:80%; text-align: center;font-family:Verdana, Geneva, sans-serif; font-weight: normal;" >Plazo Fijo</p>
								<p style="margin:0;font-size:80%; text-align: center;font-family:Verdana, Geneva, sans-serif;">@{{payment.code }}</p>
								<p style="margin:0;font-size:80%;font-family:Verdana, Geneva, sans-serif;">Atendido por: {{ Auth::user()->name }}</p> 	    												    												    											
								<p style="margin:0;font-size:80%;font-family:Verdana, Geneva, sans-serif;">Fecha: @{{ payment.date |date:'dd/MM/yyyy HH:mm:ss' }}</p>	    												  	    											  									
								<p style="margin:0;font-size:80%;font-family:Verdana, Geneva, sans-serif;">Señor: @{{ payment.clientname  }}</p>
								<p style="margin:0;font-size:80%;font-family:Verdana, Geneva, sans-serif;">Dni/Ruc: @{{ payment.identitynumber }}</p>
								<p style="margin:0;font-size:80%;font-family:Verdana, Geneva, sans-serif;">Tasa Efectiva: @{{ payment.tasa }}</p>
								<p style="margin:0;font-size:80%;font-family:Verdana, Geneva, sans-serif;border-top: 1.5px dashed #000; margin-top: 5px;margin-bottom: 5px;"></p>
						</div>
						<div class="col-lg-12" style="padding: 0">
							<div style="display: flex;padding:0; " >
								<div style="width:10%; display: inline-block;"><p style="margin:0;padding:0;  font-size:80%;font-family:Verdana, Geneva, sans-serif;">N°</p></div>
								<div style="width:25%; display: inline-block;"><p style="margin:0;padding:0; font-size:80%;font-family:Verdana, Geneva, sans-serif;">Fecha</p></div>
								<div style="width:20%; display: inline-block;"><p style="margin:0;padding:0; font-size:80%;font-family:Verdana, Geneva, sans-serif;">Monto</p></div>
								<div style="width:15%; display: inline-block;"><p style="margin:0;padding:0; font-size:80%;font-family:Verdana, Geneva, sans-serif;">Interes</p></div>
								<div style="width:20%; display: inline-block;"><p style="margin:0;padding:0; font-size:80%;font-family:Verdana, Geneva, sans-serif;">Sub Total</p></div>
							</div> 
							<p style="margin:0;font-size:80%;font-family:Verdana, Geneva, sans-serif;border-top: 1.5px dashed #000; margin-top: 5px;margin-bottom: 5px;"></p>
							<div ng-repeat="item in payment.list" style="display: flex;padding:0;">
								<div style="width:10%; display: inline-block;"><p style="margin:0;padding:0;font-size:80%;font-family:Verdana, Geneva, sans-serif;">@{{$index+1  }}</p></div>
								<div style="width:25%; display: inline-block;"><p style="margin:0;padding:0;font-size:80%;font-family:Verdana, Geneva, sans-serif;">@{{item.time |date:'dd/MM/yy' }}</p></div>
								<div style="width:20%; display: inline-block;"><p style="margin:0;padding:0;font-size:80%;font-family:Verdana, Geneva, sans-serif;">@{{item.capital | number: 2}}</p></div>
								<div style="width:15%; display: inline-block;"><p style="margin:0;padding:0;font-size:80%;font-family:Verdana, Geneva, sans-serif;">@{{item.interest | number: 2}}</p></div>
								<div style="width:20%; display: inline-block;"><p style="margin:0;padding:0;font-size:80%;font-family:Verdana, Geneva, sans-serif;">@{{(item.interest + item.capital) | number: 2}}</p></div>
							</div>		    								
							<p style="margin:0;font-size:80%;font-family:Verdana, Geneva, sans-serif;border-top: 1.5px dashed #000; margin-top: 5px;margin-bottom: 5px;"></p>
							<div style="display: flex;"> <div style="width:60%; margin:0;font-size:80%; display:inline-block;"><p style="margin:0;font-size:80%;font-family:Verdana, Geneva, sans-serif;">TOTAL:</p> </div> <div style="width:40%;display:inline-block;vertical-align:top; text-align:right;"><p style="margin:0; font-size:80%;font-family:Verdana, Geneva, sans-serif; ">S/ @{{payment.total_generate|number:2}}</p></div> </div>
							
							<p style="margin:0;font-size:80%;font-family:Verdana, Geneva, sans-serif;border-top: 1.5px dashed #000; margin-top: 5px;margin-bottom: 5px;"></p>
						</div>
					</div>
				</div>
			</div>
			<div class="modal-foot ">
				<button type="button"  class="btn btn-default hidden-print" data-dismiss="modal">Cerrar</button>
				<button class="btn btn-success" data-dismiss="modal" id="sale_prints" 
					onclick="printingVoucher('#printfees','sale_prints')"><i class="fa fa-print"></i> <h:outputText value="#{msg['Imprimir']}" /></button>
			</div>
		</div>
	</div>
	
</div>