@extends('adminlte::layouts.app')
@section('htmlheader_title')
	| Plazo Fijo
@endsection
@section('contentheader_title')
	Nuevo Plazo Fijo <a href="#" ng-click="newSale();" data-target="#modal-sale" data-toggle="modal" class="btn btn-success btn-xs">Nuevo</a>
@endsection
@section('angularcontroller')
ng-controller="fixedtermController"
@endsection

@section('main-content')
	<div class="container-fluid spark-screen" >
	<script>
	function list(element){
		var nueva_pagina;
		if(element!=undefined) {
			nueva_pagina=$(element).attr('data-id');
			angular.element("#fixedtermController").scope().setPagina('transactions','filters',nueva_pagina);
			angular.element('#fixedtermController').scope().listSales();
		}
	}
	function getvalclient(sel)
    {
		angular.element('#fixedtermController').scope().getclientid(sel.value);
    }
	</script>
		<div class="row">
			<div class="col-md-12">
				<div class="panel panel-default">
					<div class="panel-heading">LISTADO DE PLAZO FIJO</div>
					<div class="row table-filters ">
							<div class="col-md-4">
								<input type="text" ng-model="transactions.filters.filtro" placeholder="Buscar" class="form-control" ng-enter="transactions.filters.pagina=1;listSales($event)"/>
							</div>				
							<div class="col-md-2">									
								<div class="form-inline">
									<div class="input-group date">
										<span class="input-group-addon"><i class="fa fa-calendar"></i></span> <input type="text"  value="transactions.filters.dateinit"  ng-model="transactions.filters.dateinit" placeholder="#{msg['Fecha_Inicio']}" class="form-control dateinput input-append date" />
									</div>
								</div>
							</div>
							<div class="col-md-2">								
								<div class="form-inline">
									<div class="input-group date">
										<span class="input-group-addon"><i class="fa fa-calendar"></i></span> <input type="text" value="transactions.filters.dateend" ng-model="transactions.filters.dateend" placeholder="#{msg['Fecha_Final']}" class="form-control dateinput" />
									</div>
								</div>
							</div>													
							<div class="col-md-3 ">										
								<button class="btn btn-info btn-xs" data-loading-text="Please wait..." ng-click="transactions.filters.pagina=1;listSales($event)"><i class="fa fa-search"></i> Buscar</button>
								<button class="btn btn-success btn-xs"  data-loading-text="Please wait..."  ng-click="exportAll(event);"><i class="fa fa-file-excel-o"></i> Exportar</button>

							</div>
							
					</div>
					<div class="panel-body" id="fixedtermController" >
						<table class="table table-striped table-bordered table-condensed table-hover ">
							<thead>
								<th>Fecha</th>
								<th>Tipo Ahorro</th>
								<th>DNI/RUC</th>	
								<th>Cliente</th>
								<th>Comprobante</th>
								<th>Monto</th>
								<th>Fecha Desembolso</th>
                                <th>Estado</th>
                                <th>Acciones</th>
							</thead>               
							<tr ng-repeat="item in transactions.list">									
								<td>@{{item.date}}</td>
								<td>@{{item.season==null?'Plazo Fijo':item.season}}</td>
								<td>@{{item.identitynumber}}</td>
                                <td>@{{item.client}}</td>		
								<td>@{{item.code}}</td>		
								<td>@{{item.total}}</td>
								<td>@{{item.datedelivered}}</td>		
                                <td>@{{item.status}}</td>		                                
								<td>
									<a href="#" ng-click="loadPayment(item);" data-target="#modal-payment" data-toggle="modal" class="btn btn-success btn-xs"><i class="fa fa-cc-visa" aria-hidden="true"></i></a>
									<a href="#" ng-click="prepareDelete(item);" class="btn btn-danger btn-xs"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
									<a href="#" ng-click="prepareChange(item);" ng-show="item.stateid!=9" class="btn btn-primary btn-xs"><i class="fa fa-check-circle-o" aria-hidden="true"></i></a>

								</td>
							</tr>									
						</table>
						<div id="paginador_Table"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
	@include ('fixedterm.modal-create')
	@include ('fixedterm.modal-payment')
	@include ('fixedterm.modal-delete')
	@include ('fixedterm.modal-print')
@endsection
@section ('javascript')
<script src="{{asset('js/controllers/fixedtermController.js')}}"></script>
@endsection