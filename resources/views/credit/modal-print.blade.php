<div class="modal fade modal-slide-in-right" aria-hidden="true" data-keyboard="true" role="dialog" tabindex="-1" id="modal-sale-print">
	<div class="modal-dialog modal-lg">
		<div class="modal-content"> 
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" 
				aria-label="Close">
                     <span aria-hidden="true">×</span>
                </button>				
                <div>
					<button type="button"  class="btn btn-default hidden-print" data-dismiss="modal">Cerrar</button>
					<button class="btn btn-success" data-dismiss="modal" id="sale_prints" 
					onclick="printingVoucher('#print_sales','sale_prints')"><i class="fa fa-print"></i> <h:outputText value="#{msg['Imprimir']}" /></button>

				</div>
			</div>
			<div class="modal-body" style=" width: 100%;  height: 500px;  overflow: scroll;">
				<div id="print_sales" >
					<div class="grid">
						<div>
							<div id="parent">
								<div id="value">
									<div style="width:95%; display: inline-block;text-align:center"><p style="margin:0;padding:0;  font-size:100%;font-family:Verdana, Geneva, sans-serif;"><STRONG>TARJETA DE CREDITO | DEYCAR </STRONG> </p></div>
								</div>
							</div>
							<div id="parent">
								<div id="value">
									<div style="width:95%; display: inline-block;"><p style="margin:0;padding:0;  font-size:90%;font-family:Verdana, Geneva, sans-serif;"><STRONG>SOCIO: </STRONG> @{{ payment.clientname  }}</p></div>
									<div style="width:95%; display: inline-block;"><p style="margin:0;padding:0;  font-size:90%;font-family:Verdana, Geneva, sans-serif;"><STRONG>DNI: </STRONG> @{{ payment.identitynumber  }}</p></div>
									<div style="width:95%; display: inline-block;"><p style="margin:0;padding:0;  font-size:90%;font-family:Verdana, Geneva, sans-serif;"><STRONG>DIRECCION: </STRONG>@{{ payment.address  }}</p></div>
									<div style="width:95%; display: inline-block;"><p style="margin:0;padding:0;  font-size:90%;font-family:Verdana, Geneva, sans-serif;"><STRONG>ANALISTA: </STRONG>@{{ payment.secundaryemployed  }}</p></div>
									<div style="width:45%; display: inline-block;"><p style="margin:0;padding:0;  font-size:90%;font-family:Verdana, Geneva, sans-serif;"><STRONG>F. DESEMBOLSO: </STRONG>@{{ payment.date  |date:'dd/MM/yy'  }}</p></div>
									<div style="width:45%; display: inline-block;"><p style="margin:0;padding:0;  font-size:90%;font-family:Verdana, Geneva, sans-serif;"><STRONG>CELULAR: </STRONG>@{{ payment.phone  }}</p></div>
									<div style="width:45%; display: inline-block;"><p style="margin:0;padding:0;  font-size:90%;font-family:Verdana, Geneva, sans-serif;"><STRONG>CUOTAS: </STRONG>@{{ payment.fees  }}</p></div>
									<div style="width:45%; display: inline-block;"><p style="margin:0;padding:0;  font-size:90%;font-family:Verdana, Geneva, sans-serif;"><STRONG>MONTO: </STRONG>@{{ payment.amount  }}</p></div>
									<div style="width:45%; display: inline-block;"><p style="margin:0;padding:0;  font-size:90%;font-family:Verdana, Geneva, sans-serif;"><STRONG>N° DOC: </STRONG>@{{ payment.code  }}</p></div>
								</div>
							</div>	
							<div id="parent">
								<div id="value">
									<div class="divborder ">
										<div style="width:15%; display: inline-block;text-align: center;vertical-align: top;"><p style="margin:0;padding:0; font-size:80%;font-family:Verdana, Geneva, sans-serif;">N°</p></div>
										<div style="width:15%; display: inline-block;text-align: center;vertical-align: top;"><p style="margin:0;padding:0;font-size:80%;font-family:Verdana, Geneva, sans-serif;">FECHA</p></div>
										<div style="width:15%; display: inline-block;text-align: center;vertical-align: top;"><p style="margin:0;padding:0;font-size:80%;font-family:Verdana, Geneva, sans-serif;">CUOTA</p></div>
										<div style="width:25%; display: inline-block;text-align: center;vertical-align: top;"><p style="margin:0;padding:0;font-size:80%;font-family:Verdana, Geneva, sans-serif;">F. CANCELO</p></div>
										<div style="width:25%; display: inline-block;text-align: center;vertical-align: top;"><p style="margin:0;padding:0;font-size:80%;font-family:Verdana, Geneva, sans-serif;">FIRMA</p></div>
									</div> 	
									<div ng-repeat="item in payment.list" class="divborder">
										<div style="width:15%; display: inline-block;text-align: center;vertical-align: top;"><p style="margin:0;padding:0;font-size:70%;font-family:Verdana, Geneva, sans-serif;">@{{$index+1 }}</p></div>
										<div style="width:15%; display: inline-block;text-align: center;vertical-align: top;"><p style="margin:0;padding:0;font-size:70%;font-family:Verdana, Geneva, sans-serif;">@{{ item.time |date:'dd/MM/yy'}}</p></div>
										<div style="width:15%; display: inline-block;text-align: center;vertical-align: top;"><p style="margin:0;padding:0;font-size:70%;font-family:Verdana, Geneva, sans-serif;">@{{item.feeamount|number: 2}}</p></div>
										<div style="width:25%; display: inline-block;text-align: center;vertical-align: top;"><p style="margin:0;padding:0;font-size:70%;font-family:Verdana, Geneva, sans-serif;"></p></div>
										<div style="width:25%; display: inline-block;text-align: center;vertical-align: top;"><p style="margin:0;padding:0;font-size:70%;font-family:Verdana, Geneva, sans-serif;"></p></div>					    									
									</div>	
									
									
								</div>	
							</div>	
										
						</div>
						<div>
							<div id="parent">
								<div id="value">
									<div style="width:95%; display: inline-block;text-align:center"><p style="margin:0;padding:0;  font-size:100%;font-family:Verdana, Geneva, sans-serif;"><STRONG>TARJETA DE CREDITO | DEYCAR </STRONG> </p></div>
								</div>
							</div>
							<div id="parent">
								<div id="value">
									<div style="width:95%; display: inline-block;"><p style="margin:0;padding:0;  font-size:90%;font-family:Verdana, Geneva, sans-serif;"><STRONG>SOCIO: </STRONG> @{{ payment.clientname  }}</p></div>									
									<div style="width:95%; display: inline-block;"><p style="margin:0;padding:0;  font-size:90%;font-family:Verdana, Geneva, sans-serif;"><STRONG>DNI: </STRONG> @{{ payment.identitynumber  }}</p></div>
									<div style="width:95%; display: inline-block;"><p style="margin:0;padding:0;  font-size:90%;font-family:Verdana, Geneva, sans-serif;"><STRONG>DIRECCION: </STRONG>@{{ payment.address  }}</p></div>
									<div style="width:95%; display: inline-block;"><p style="margin:0;padding:0;  font-size:90%;font-family:Verdana, Geneva, sans-serif;"><STRONG>ANALISTA: </STRONG>@{{ payment.secundaryemployed  }}</p></div>
									<div style="width:45%; display: inline-block;"><p style="margin:0;padding:0;  font-size:90%;font-family:Verdana, Geneva, sans-serif;"><STRONG>F. DESEMBOLSO: </STRONG>@{{ payment.date  |date:'dd/MM/yy'  }}</p></div>
									<div style="width:45%; display: inline-block;"><p style="margin:0;padding:0;  font-size:90%;font-family:Verdana, Geneva, sans-serif;"><STRONG>CELULAR: </STRONG>@{{ payment.phone  }}</p></div>
									<div style="width:45%; display: inline-block;"><p style="margin:0;padding:0;  font-size:90%;font-family:Verdana, Geneva, sans-serif;"><STRONG>CUOTAS: </STRONG>@{{ payment.fees  }}</p></div>
									<div style="width:45%; display: inline-block;"><p style="margin:0;padding:0;  font-size:90%;font-family:Verdana, Geneva, sans-serif;"><STRONG>MONTO: </STRONG>@{{ payment.amount  }}</p></div>
									<div style="width:45%; display: inline-block;"><p style="margin:0;padding:0;  font-size:90%;font-family:Verdana, Geneva, sans-serif;"><STRONG>N° DOC: </STRONG>@{{ payment.code  }}</p></div>
								</div>
							</div>	
							<div id="parent">
								<div id="value">
									<div class="divborder ">
										<div style="width:15%; display: inline-block;text-align: center;vertical-align: top;"><p style="margin:0;padding:0; font-size:80%;font-family:Verdana, Geneva, sans-serif;">N°</p></div>
										<div style="width:15%; display: inline-block;text-align: center;vertical-align: top;"><p style="margin:0;padding:0;font-size:80%;font-family:Verdana, Geneva, sans-serif;">FECHA</p></div>
										<div style="width:15%; display: inline-block;text-align: center;vertical-align: top;"><p style="margin:0;padding:0;font-size:80%;font-family:Verdana, Geneva, sans-serif;">CUOTA</p></div>
										<div style="width:25%; display: inline-block;text-align: center;vertical-align: top;"><p style="margin:0;padding:0;font-size:80%;font-family:Verdana, Geneva, sans-serif;">F. CANCELO</p></div>
										<div style="width:25%; display: inline-block;text-align: center;vertical-align: top;"><p style="margin:0;padding:0;font-size:80%;font-family:Verdana, Geneva, sans-serif;">FIRMA</p></div>
									</div> 	
									<div ng-repeat="item in payment.list" class="divborder">
										<div style="width:15%; display: inline-block;text-align: center;vertical-align: top;"><p style="margin:0;padding:0;font-size:70%;font-family:Verdana, Geneva, sans-serif;">@{{$index+1 }}</p></div>
										<div style="width:15%; display: inline-block;text-align: center;vertical-align: top;"><p style="margin:0;padding:0;font-size:70%;font-family:Verdana, Geneva, sans-serif;">@{{ item.time |date:'dd/MM/yy'}}</p></div>
										<div style="width:15%; display: inline-block;text-align: center;vertical-align: top;"><p style="margin:0;padding:0;font-size:70%;font-family:Verdana, Geneva, sans-serif;">@{{item.feeamount|number: 2}}</p></div>
										<div style="width:25%; display: inline-block;text-align: center;vertical-align: top;"><p style="margin:0;padding:0;font-size:70%;font-family:Verdana, Geneva, sans-serif;"></p></div>
										<div style="width:25%; display: inline-block;text-align: center;vertical-align: top;"><p style="margin:0;padding:0;font-size:0%;font-family:Verdana, Geneva, sans-serif;"></p></div>					    									
									</div>	
									
									
								</div>	
							</div>	
						</div>
					<div>
				</div>
				</div>
			</div>
			<div class="modal-foot ">
				
			</div>
		</div>
	</div>
	
</div>