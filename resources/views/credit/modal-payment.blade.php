<div class="modal fade modal-slide-in-right" aria-hidden="true" role="dialog"  data-backdrop="static" data-keyboard="false" tabindex="-1" id="modal-payment">
	<div class="modal-dialog modal-size">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" 
				aria-label="Close">
                     <span aria-hidden="true">×</span>
                </button>				
                <h4 class="modal-title"> Cronograma de Pagos</h4>
			</div>
			<div class="modal-body">
				<div class="col-md-12">
					<div class="col-md-12">
						<div class="col-md-6">								
							<div class="form-group">
								<label class="col-md-2 ">Monto: </label>
								<div class="col-md-2">
									<input class="form-control" ng-model="payment.amount">													
								</div>
								<label class="col-md-2 ">Cuotas: </label>
								<div class="col-md-2">
									<input class="form-control" ng-model="payment.fees">													
								</div>
								<label class="col-md-2 ">Frecuencia en Dias: </label>
								<div class="col-md-2">									
									<input class="form-control" ng-model="payment.frecuencyday">													
								</div>

							</div>														
						</div>
						<div class="col-md-6">								
							<div class="form-group">
								<label class="col-md-2 ">Tasa Efectiva: </label>
								<div class="col-md-4">
									<input class="form-control" ng-model="payment.tasa">													
								</div>
								<label class="col-md-2 ">F. Desembolso</label>
								<div class="col-md-4">
									<div class="form-inline">
										<div class="input-group date">
											<span class="input-group-addon"><i class="fa fa-calendar"></i></span> 
											<input type="text"  ng-model="payment.date" class="form-control dateinput input-append date" />
										</div>
									</div>
								</div>							
								
							</div>		
						</div>
						<div class="col-md-12">								
							<div class="form-group">					
								<div class="col-md-4">
									<a class="btn btn-primary btn-xs" ng-click="generatePayments()">Generar Pagos</a>  
								</div>	
								<div class="col-md-4">
									<a class="btn btn-success btn-xs" ng-click="preparePrint()" >Imprimir</a>
								</div>	
							</div>		
						</div>
						
						<div class="col-md-12" style="top: 10px;  background-color: lightblue; width: 100%;  height: 380px;  overflow: scroll;">
							<div class="wrapper-table-detail ">
								<table class='table table-striped table-bordered table-condensed table-hover' >
									<thead><tr><th>Cuota</th><th>Vencimiento</th><th>Capital</th><th>Interes</th><th>Monto Cuota</th><th>Dia</th></tr></thead>
									<tbody class="datails-tbody">
										<tr ng-repeat="item in payment.list"  >
											<td style="width: 20px">@{{$index+1}}</td>
											<td style="width: 20px">@{{ item.time }}</td>
											<td>@{{item.capital|number: 2}}</td>
											<td class='centered'>@{{item.interest|number: 2}}</td>
											<td>@{{item.feeamount|number: 2}}</td>
											<td class="amount-right">@{{item.day}}</td>
										</tr>
									</tbody>								
								</table>
							</div>
						</div>
					</div>									 					
				</div>
			</div>
			<div class="modal-foot ">
				<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
				<button type="submit" class="btn btn-primary"  data-loading-text="Espere Por Favor..."  ng-click="savePaymentSchedules($event);" data-dismiss="modal" ng-disabled="BrandForm.$invalid" id="submit-all">Confirmar</button>
			</div>
		</div>
	</div>
	
</div>