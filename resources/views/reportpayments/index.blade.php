@extends('adminlte::layouts.app')
@section('htmlheader_title')
	| Reporte de Pagos
@endsection
@section('contentheader_title')
	Pagos Realizados del dia
@endsection
@section('angularcontroller')
ng-controller="reportpaymentsController"
@endsection

@section('main-content')
	<div class="container-fluid spark-screen" >
	<script>
	function list(element){
		var nueva_pagina;
		if(element!=undefined) {
			nueva_pagina=$(element).attr('data-id');
			angular.element("#reportpaymentsController").scope().setPagina('transactions','filters',nueva_pagina);
			angular.element('#reportpaymentsController').scope().listReportPayments();
		}
	}
	</script>
		<div class="row">
			<div class="col-md-12">
				<div class="panel panel-default">
					<div class="panel-heading">REPORTE DE PAGOS REALIZADOS POR FECHAS</div>
					<div class="row table-filters ">
							<div class="col-md-4">
								<input type="text" ng-model="transactions.filters.filtro" placeholder="Buscar" class="form-control" ng-enter="transactions.filters.pagina=1;listReportPayments($event)"/>
							</div>				
							<div class="col-md-2">									
								<div class="form-inline">
									<div class="input-group date">
										<span class="input-group-addon"><i class="fa fa-calendar"></i></span> <input type="text"  value="transactions.filters.dateinit"  ng-model="transactions.filters.dateinit" placeholder="#{msg['Fecha_Inicio']}" class="form-control dateinput input-append date" />
									</div>
								</div>
							</div>
							<div class="col-md-2">								
								<div class="form-inline">
									<div class="input-group date">
										<span class="input-group-addon"><i class="fa fa-calendar"></i></span> <input type="text" value="transactions.filters.dateend" ng-model="transactions.filters.dateend" placeholder="#{msg['Fecha_Final']}" class="form-control dateinput" />
									</div>
								</div>
							</div>													
							<div class="col-md-3 ">										
								<button class="btn btn-info btn-xs" data-loading-text="Please wait..." ng-click="transactions.filters.pagina=1;listReportPayments($event)"><i class="fa fa-search"></i> Buscar</button>
								<button class="btn btn-success btn-xs"  data-loading-text="Please wait..."  ng-click="exportAll('exportReportPayments','reportepagos',$event);"><i class="fa fa-file-excel-o"></i> Exportar</button>

							</div>
							
					</div>
					<div class="panel-body" id="reportpaymentsController" >
						<table class="table table-striped table-bordered table-condensed table-hover " id="export">
							<thead>
								<th>Fecha</th>
								<th>Tipo Operacion</th>
								<th>Pago</th>
								<th>Cobrador</th>
								<th>N° Pago</th>
								<th>Cliente</th>
								<th>N° Comprobante</th>
								<th>Tipo Credito</th>
								<th>Ingreso</th>
								<th></th>
							</thead>
							<tbody>
								<tr ng-repeat="item in transactions.list">									
									<td>@{{item.limittime}}</td>
									<td>Pago</td>
									<td>@{{item.accountant != 0?'Contable':'Regulado' }}</td>
									<td>@{{item.systemuser}}</td>
									<td>@{{item.codefee}}</td>
									<td>@{{item.client}}</td>
									<td>@{{item.code}}</td>
									<td>@{{item.season}}</td>
									<td>@{{item.cash}}</td>
									@if ( Auth::user()->positionid==1)
									<td><a href="#" ng-click="prepareDelete(item);" class="btn btn-danger btn-xs"><i class="fa fa-trash-o" aria-hidden="true"></i></a></td>
									@endif
								</tr>	
							</tbody>	
							<tfoot>
								<tr>									
									<td></td><td></td><td></td><td></td><td></td><td></td><td>Total</td><td>@{{totalfeeamount | number:2}}</td>
								</tr>
							</tfoot>							
						</table>
						<div id="paginador_Table"></div>
					</div>
					<div ng-show="export">
						<table class="table table-striped table-bordered table-condensed table-hover " id="exportReportPayments">
							<thead>
								<th>Fecha</th>
								<th>Tipo Operacion</th>
								<th>Pago</th>
								<th>Cobrador</th>
								<th>N° Pago</th>
								<th>Cliente</th>
								<th>N° Comprobante</th>
								<th>Tipo Credito</th>
								<th>Ingreso</th>								

							</thead>
							<tbody>
								<tr ng-repeat="item in transactions.listall">
									<td style="text-align: center">@{{item.limittime}}</td>
									<td style="text-align: center">Pago</td>
									<td>@{{item.accountant != 0?'Contable':'Regulado' }}</td>
									<td>@{{item.systemuser}}</td>
									<td style="text-align: center">@{{item.codefee}}</td>
									<td>@{{item.client}}</td>
									<td style="text-align: center">@{{item.code}}</td>
									<td style="text-align: center">@{{item.season}}</td>
									<td style="text-align: center">@{{item.cash}}</td>
								</tr>
							</tbody>
							<tfoot>
								<tr>									
									<td></td><td></td><td></td><td></td><td></td><td></td><td>Total</td><td>@{{totalfeeamount | number:2}}</td>
								</tr>
							</tfoot>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
	@include ('reportpayments.modal-print')
@endsection
@section ('javascript')
<script src="{{asset('js/controllers/reportpaymentsController.js')}}"></script>
@endsection