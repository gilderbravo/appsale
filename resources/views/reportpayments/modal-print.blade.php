<div class="modal fade modal-slide-in-right" aria-hidden="true" role="dialog" tabindex="-1" id="modal-sale-print">
	<div class="modal-dialog modal-md2">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" 
				aria-label="Close">
                     <span aria-hidden="true">×</span>
                </button>				
                <h4 class="modal-title"> Imprimir Cronograma de Pago	</h4>
			</div>
			<div class="modal-body" style="top: 10px; width: 100%;  height: 500px;  overflow: scroll;">
				<div id="printsale" class="ticket-print" style="-webkit-transform:scale(0.85); ">
					<div class="row" >
						<div class="col-lg-12" style="	width: 50%;border: 1px solid black !important;border-collapse: collapse;">
							<table style="width: 100%;text-align: center; font-size: 30px;border: 1px solid black !important;background-color: lightblue;">
								<thead>
									<tr >
										<td>JUNTA MAS</td>
									</tr>
								</thead>
								<tbody>
								<tr>
									<table style="border: 1px solid black !important;width: 100%;" >

										<tr>
											<td><strong>CLIENTE:</strong> @{{ payment.clientname  }}</td>
											<td><strong>Monto:</strong> @{{ payment.amount  }}</td>
										</tr>
										<tr>
											<td><strong>Cell:</strong> @{{ payment.phone  }}</td>
											<td><strong>T.I:</strong> @{{ payment.tasa  }}%</td>
										</tr>
										<tr>
											<td><strong>Direccion:</strong> @{{ payment.address  }}</td>
										</tr>

									</table>
								</tr>
								<tr>
									<td>
										<table class='table-print' style="border: 1px solid black !important;border-collapse: collapse;width: 100%;">
											<thead>
											<tr style="border: 1px solid black !important;border-collapse: collapse;">
												<th  style="width: 10%;text-align: center">N°</th>
												<th  style="width: 20%;text-align: center">Fecha</th>
												<th  style="width: 20%;text-align: center">Cuota</th>
												<th  style="width: 20%;text-align: center">f.Cancelo</th>
												<th  style="width: 30%;text-align: center">Firma</th>
											</tr>
											</thead>
											<tbody >
											<tr ng-repeat="item in payment.list" style="border: 1px solid black !important;border-collapse: collapse;"  >
												<td style="width: 10%;text-align: center;width: 15px;font-size: 12px">@{{$index+1}}</td>
												<td style="width: 20%;text-align: center;width: 15px;font-size: 12px" >@{{ item.time |date:'dd/MM/yy' }}</td>
												<td style="width: 20%;text-align: center;width: 15px;font-size: 12px">@{{item.feeamount|number: 2}}</td>
												<td></td>
												<td></td>
											</tr>
											</tbody>
										</table>
									</td>
								</tr>
								</tbody>
							</table>

						</div>
					</div>
				</div>
			</div>
			<div class="modal-foot ">
				<button type="button"  class="btn btn-default hidden-print" data-dismiss="modal">Cerrar</button>
				<button class="btn btn-success hidden-print" data-dismiss="modal" id="sale_print" ng-click="printme('printsale')"><i class="fa fa-print"></i> <h:outputText value="#{msg['Imprimir']}" /></button>
{{--				<button class="btn btn-success hidden-print" data-dismiss="modal" id="sale_print" onclick="printDiv('printsale','modal-sale-print')"><i class="fa fa-print"></i> <h:outputText value="#{msg['Imprimir']}" /></button>--}}

			</div>
		</div>
	</div>
	
</div>