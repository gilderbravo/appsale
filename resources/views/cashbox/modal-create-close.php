<div class="modal fade modal-slide-in-right" aria-hidden="true" data-backdrop="static" data-keyboard="false" role="dialog" id="modal-cashbox-close">

	<div class="modal-dialog| modal-size">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"
						aria-label="Close">
					<span aria-hidden="true">×</span>
				</button>
				<h4 class="modal-title"> Cierre de Caja</h4>
			</div>
			<div class="modal-body">
				<div class="col-md-7">
					<div class="col-md-12">
						<div class="form-group">
							<label class="col-md-3 "><strong>Fecha Cierre</strong></label>
							<div class="col-md-3">
								<div class="form-inline">
									<div class="input-group date">
										<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
										<input type="text"  ng-model="transaction.closingdate" class="form-control dateinput input-append date" />
									</div>
								</div>
							</div>
							<label class="col-md-3 "><strong>Monto Apertura Caja:</strong> </label>
							<div class="col-md-3">
								<input class="form-control" ng-model="transaction.initamount" >
							</div>
							
						</div>						
					</div>
					<br />
					<div class="col-md-12">
						<div class="form-group">
							<label class="col-md-3 "><strong> Cobros del Dia </strong></label>
							<div class="col-md-3">
							<label >{{transaction.totalfeeamount | number:2}}</label>
							</div>
							<label class="col-md-3 "><strong>Abonos</strong></label>
							<div class="col-md-3">
							<label >{{transaction.totalpaymentamount | number:2}}</label>
							</div>
							
						</div>						
					</div>
					<div class="col-md-12">
						<div class="form-group">
							<label class="col-md-3 "><strong> Habilitacion</strong></label>
							<div class="col-md-3">
							<label >{{transaction.totalqualificationamount | number:2}}</label>
							</div>
							<label class="col-md-3 "><strong>Depositos</strong></label>
							<div class="col-md-3">
							<label >{{transaction.totaldebitamount | number:2}}</label>
							</div>
							
						</div>						
					</div>
					<br />
					<div class="col-md-12">
						<div class="form-group">
							<label class="col-md-3 "><strong>Egresos </strong></label>
							<div class="col-md-3">
								<label >{{transaction.totalexpenseamount  | number:2}}</label>
							</div>
							<label class="col-md-3 "><strong>Total Cierre Caja</strong></label>
							<div class="col-md-3">
								<label >{{transaction.finalamount  | number:2}}</label>
							</div>
							
						</div>						
					</div>
					
				</div>
				<div class="col-md-5">
					<div class="col-md-12">
						<div class="form-group">
						<div class="wrapper-table-detail " >
							<table class='table table-striped table-bordered table-condensed table-hover' >
								<thead><tr><th>Moneda</th><th>Monto</th><th class="col-price">Total </th></tr></thead>
								<tbody class="datails-tbody">
									<tr ng-repeat="item in ticketing"  >
										<td class="money">S/.{{item.money }}</td>
										<td class="money"><input  type="text"  ng-model="item.amount" class="form-control "  ng-blur="calculateMoney()"/></td>
										<td class="money"><input  type="text"  ng-model="item.total" class="form-control "/></td>
									</tr>
								</tbody>
								<tfoot>
									<tr>
										<td></td><td></td><td>{{totalMoney}}</td>
									</tr>
								</tfoot>
								
							</table>
						</div>
								
						</div>						
					</div>
					<br />
				</div>
				<br>
			</div>
			<div class="modal-foot ">
				<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
				<button type="submit" class="btn btn-primary"  data-loading-text="Espere Por Favor..."  ng-click="saveCashbox($event);" data-dismiss="modal" ng-disabled="BrandForm.$invalid" id="submit-all">Cierre Caja</button>
			</div>
		</div>
	</div>

</div>