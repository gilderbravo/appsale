<div class="modal fade modal-slide-in-right" aria-hidden="true" data-backdrop="static" data-keyboard="false" role="dialog" id="modal-cashbox-open">

	<div class="modal-dialog| modal-size-md">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"
						aria-label="Close">
					<span aria-hidden="true">×</span>
				</button>
				<h4 class="modal-title"> Apertura de Caja</h4>
			</div>
			<div class="modal-body">
				<div class="col-md-12">
					<div class="col-md-12">
							<div class="form-group">
								<label class="col-md-3 ">Fecha Apertura</label>
								<div class="col-md-3">
									<div class="form-inline">
										<div class="input-group date">
											<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
											<input type="text"  ng-model="transaction.openingdate" class="form-control dateinput input-append date" />
										</div>
									</div>
								</div>
								<label class="col-md-3 ">Monto Apertura Caja: </label>
								<div class="col-md-2">
									<input class="form-control" ng-model="transaction.initamount" >
								</div>
								
							</div>
						
					</div>
				</div>
				<br>
			</div>
			<div class="modal-foot ">
				<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
				<button type="submit" class="btn btn-primary"  data-loading-text="Espere Por Favor..."  ng-click="saveCashbox($event);" data-dismiss="modal" ng-disabled="BrandForm.$invalid" id="submit-all">Apertura</button>
			</div>
		</div>
	</div>

</div>