@extends('adminlte::layouts.app')
@section('htmlheader_title')
	| Caja
@endsection
@section('contentheader_title')
	Apertura  <a href="#" ng-click="newCashbox();" data-target="#modal-cashbox-open" data-toggle="modal" class="btn btn-success btn-xs"><i class="fa fa-lock" aria-hidden="true"></i></a>

@endsection
@section('angularcontroller')
ng-controller="cashboxController"
@endsection

@section('main-content')
	<div class="container-fluid spark-screen" >
	<script>
	function list(element){
		var nueva_pagina;
		if(element!=undefined) {
			nueva_pagina=$(element).attr('data-id');
			angular.element("#cashboxController").scope().setPagina('transactions','filters',nueva_pagina);
			angular.element('#cashboxController').scope().listCashbox();
		}
	}
	
	</script>
		<div class="row">
			<div class="col-md-12">
				<div class="panel panel-default">
					<div class="panel-heading">LISTADO DE CAJAS</div>
					<div class="row table-filters ">
							<div class="col-md-4">
								<input type="text" ng-model="transactions.filters.filtro" placeholder="Buscar" class="form-control" ng-enter="transactions.filters.pagina=1;listCashboxs($event)"/>
							</div>				
							<div class="col-md-2">									
								<div class="form-inline">
									<div class="input-group date">
										<span class="input-group-addon"><i class="fa fa-calendar"></i></span> <input type="text"  value="transactions.filters.dateinit"  ng-model="transactions.filters.dateinit" placeholder="#{msg['Fecha_Inicio']}" class="form-control dateinput input-append date" />
									</div>
								</div>
							</div>
							<div class="col-md-2">								
								<div class="form-inline">
									<div class="input-group date">
										<span class="input-group-addon"><i class="fa fa-calendar"></i></span> <input type="text" value="transactions.filters.dateend" ng-model="transactions.filters.dateend" placeholder="#{msg['Fecha_Final']}" class="form-control dateinput" />
									</div>
								</div>
							</div>													
							<div class="col-md-3 ">										
								<button class="btn btn-info btn-xs" data-loading-text="Please wait..." ng-click="transactions.filters.pagina=1;listCashboxs($event)"><i class="fa fa-search"></i> Buscar</button>
								<button class="btn btn-success btn-xs"  data-loading-text="Please wait..."  ng-click="exportAll(event);"><i class="fa fa-file-excel-o"></i> Exportar</button>

							</div>
							
					</div>
					<div class="panel-body" id="cashboxController" >
						<table class="table table-striped table-bordered table-condensed table-hover ">
							<thead>
								<th>Fecha Apertura</th>
								<th>Monto Inicial</th>	
								<th>Monto Cobros</th>	
								<th>Monto Abonos</th>	
								<th>Monto Egresos</th>	
								<th>Fecha Cierre</th>
								<th>Monto Cierre</th>
                                <th>Estado</th>
                                <th>Acciones</th>
							</thead>               
							<tr ng-repeat="item in transactions.list">									
								<td>@{{item.openingdate}}</td>
								<td>@{{item.initamount | number:2}}</td>
								<td>@{{item.totalfeeamount  | number:2}}</td>
								<td>@{{item.totalpaymentamount  | number:2}}</td>
								<td>@{{item.totalexpenseamount  | number:2}}</td>
								<td>@{{item.closingdate}}</td>		
								<td>@{{item.finalamount  | number:2}}</td>		
                                <td>@{{item.statename}}</td>		                                
								<td>
									<a href="#" ng-show="item.stateid==7" ng-click="loadCashbox(item);" data-toggle="modal"  data-target="#modal-cashbox-close" class="btn btn-primary btn-xs"><i class="fa fa-lock" aria-hidden="true"></i></a>
								</td>
							</tr>									
						</table>
						<div id="paginador_Table"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
	@include ('cashbox.modal-create')
	@include ('cashbox.modal-create-close')
@endsection
@section ('javascript')
<script src="{{asset('js/controllers/cashboxController.js')}}"></script>
@endsection