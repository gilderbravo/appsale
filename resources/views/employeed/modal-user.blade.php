<div class="modal fade modal-slide-in-right" aria-hidden="true" role="dialog" tabindex="-1" id="modal-user">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" 
				aria-label="Close">
                     <span aria-hidden="true">×</span>
                </button>				
                <h4 class="modal-title"> Usuario</h4>
			</div>
			<div class="modal-body">
				<div class="col-md-12">
						<div class="col-md-6">
							<div class="form-group">
								<label class="col-md-4 control-label">E-mail</label>
								<div class="col-md-8">
									<input type="text" id="txtEmail" class="form-control" ng-model="user.email" >
								</div>
							</div>
							<br /><br />
							<div class="form-group">
								<label class="col-md-4 control-label" id="lblName">Password</label>
								<div class="col-md-8">
									<input class="form-control" id="txtPassword" type="password" ng-model="user.password">
								</div>
							</div>

							<br /><br />
							<div class="form-group">
									<label class="col-md-4 control-label" id="lblfirstname">Confirmar Password</label>
									<div class="col-md-8">
										<input class="form-control" id="txtPasswordR" type="password" ng-model="user.passwordr">
									</div>
							</div>

						</div>

				</div>
				
			</div>
			
			<div class="modal-foot">					
				<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
				<button type="submit" class="btn btn-primary" data-loading-text="Espere Por Favor..." ng-click="saveuser($event);"  ng-disabled="ClientForm.$invalid" id="submit-all">Confirmar</button>
			</div>
		</div>
	</div>
	
</div>