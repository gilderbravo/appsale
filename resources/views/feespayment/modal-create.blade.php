<div class="modal fade modal-slide-in-right" aria-hidden="true" role="dialog" tabindex="-1" id="modal-fee">
	<div class="modal-dialog modal-size">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" 
				aria-label="Close">
                     <span aria-hidden="true">×</span>
                </button>				
                <h4 class="modal-title"> Pagos de Ahorros</h4>
			</div>
			<div class="modal-body">
				<div class="col-md-12">
					<div class="col-md-7">
						<div class="col-md-6">								
							<label class="col-sm-4 ">Cantidad Cuota</label>
							<div class="col-sm-4">
								<input type="text"  value="fee.dues"  ng-model="fee.dues" class="form-control " ng-blur="addDuestoFee(fee.dues);" />
							</div>			
																	
						</div>
						
						<div class="col-md-6">								
							<div class="form-group">
								<label class="col-md-4 ">Fecha</label>
								<div class="col-md-8">
									<div class="form-inline">
										<div class="input-group date">
											<span class="input-group-addon"><i class="fa fa-calendar"></i></span> 
											<input type="text"  value="fee.date"  ng-model="fee.date" class="form-control dateinput input-append date" />
										</div>
									</div>
								</div>							
								
							</div>		
						</div>
						@if ( Auth::user()->positionid==1)
						<div class="form-check form-check-inline col-md-4">
							<label class="">Pago Contable? </label>
							<label class="form-check-label" >Si</label>
							<input class="form-check-input" ng-model="fee.accountant" type="checkbox"  ng-true-value="1" ng-false-value="0"  >  
						</div>
						@endif
						<div class="wrapper-table-detail " style="margin-top: 50px;">
							<table class='table table-striped table-bordered table-condensed table-hover' >
								<thead><tr><th>Cuota N°</th><th>Fecha Cronograma</th><th class="col-price">Monto </th><th></th></tr></thead>
								<tbody class="datails-tbody">
										<tr ng-repeat="item in fee.details"  >
										<td class="money">@{{item.numberdues }}</td>
										<td class="money">@{{item.time }}</td>
										<td class="money" ><input  type="text"  ng-model="item.feeamount" class="form-control " ng-blur="calculateTotalFees();"/></td>
										<td class='centered' ><a ng-click="removeDetail($index)" class='btn btn-danger btn-xs'><i class="fa fa-remove"></i></a></td>
										</tr>
								</tbody>
								<tfoot class="amount-right">
										<tr><td></td><td class="texto" >Total:</td><td class="numberPrint">@{{fee.total|number:2}}</td><td></td></tr>
								</tfoot>
								</table>
						</div>

					</div>
					<div class="col-md-5" >
							<div id="list" title="1" style="width: 100%;  height: 380px;  overflow: scroll;">
								<div class="list-product list-product-content">
									<div class="wrapper-mobile">
										<div class="table-mobile">
											<div class="row-mobile header-green">
											<div class="cell-mobile">N° Cuota</div>
											<div class="cell-mobile">Fecha Abono</div>
											<!-- <div class="cell-mobile">Monto</div> -->
											</div>
											<div class="row-mobile" ng-repeat="item in pendingPayment.list">
												
												<div class="cell-mobile" data-title="N° Cuota" ng-click="onPaymentClick(item)"   href="javascript:void(0)">@{{item.numberdues}}</div>
												<div class="cell-mobile" data-title="Fecha Pago" ng-click="onPaymentClick(item)"   href="javascript:void(0)">@{{item.time}}</div>
												<!-- <div class="cell-mobile" data-title="Monto" ng-click="onPaymentClick(item)"   href="javascript:void(0)">@{{(item.feeamount | number:2)}}</div> -->
												
											</div>
										</div>
									</div>				
						
								</div>
							</div>
						
						
					</div>					 					
				</div>
		</div>
			<div class="modal-foot ">
				<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
				<button type="submit" class="btn btn-primary" ng-click="saveFees();" data-dismiss="modal" ng-disabled="BrandForm.$invalid" id="submit-all">Confirmar</button>
			</div>
		</div>
	</div>
	
</div>