<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">

    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel (optional) -->
        @if (! Auth::guest())
            <div class="user-panel">
                <!-- <div class="pull-left image">
                    <img src="{{ Gravatar::get($user->email) }}" class="img-circle" alt="User Image" />
                </div> -->
                <div class="pull-left info">
                    <p>{{ Auth::user()->name }}</p>
                    <!-- Status -->
                    <a href="#"><i class="fa fa-circle text-success"></i> {{ trans('adminlte_lang::message.online') }}</a>
                </div>
            </div>
        @endif

        <!-- search form (Optional) -->
        <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="{{ trans('adminlte_lang::message.search') }}..."/>
              <span class="input-group-btn">
                <button type='submit' name='search' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i></button>
              </span>
            </div>
        </form>
        <!-- /.search form -->

        <!-- Sidebar Menu -->
        <ul class="sidebar-menu">
            <li class="header">MENUS</li>
            <!-- Optionally, you can add icons to the links -->
            <!-- <li class="active"><a href="{{ url('home') }}"><i class='fa fa-home'></i> <span>{{ trans('adminlte_lang::message.home') }}</span></a></li> -->
            @if ( Auth::user()->positionid==1)
                
                <li class="treeview">
                    <a><i class="fa fa-area-chart"></i><span>Registro de Cronogramas</span> <i class="fa fa-angle-left pull-right"></i></a>
                    <ul class="treeview-menu">
                        <li><a href="{{url('sale/index')}}"><i class="fa fa-cart-plus"></i><span>Venta</span></a></li>
                        <li><a href="{{url('credit/index')}}"><i class='fa fa-credit-card-alt'></i> <span>Creditos</span></a></li>
                        <li><a href="{{url('savingmoney/index')}}"><i class='fa fa-suitcase'></i> <span>JuntaMas</span></a></li>                                       
                        <li><a href="{{url('fixedterm/index')}}"><i class='fa fa-suitcase'></i> <span>Plazo Fijo</span></a></li> 
                        <li><a href="{{url('client/index')}}"><i class='fa fa-user'></i> <span>Cliente</span></a></li>

                    </ul>
                </li>

                <li class="treeview">
                    <a><i class="fa fa-area-chart"></i><span>Registro de Pagos</span> <i class="fa fa-angle-left pull-right"></i></a>
                    <ul class="treeview-menu">
                        <li><a href="{{url('fees/index')}}"><i class='fa fa-money'></i> <span>Deuda Clientes</span></a></li>
                        <li><a href="{{url('feespayment/index')}}"><i class='fa fa-money'></i> <span>Abono Clientes</span></a></li>
                    </ul>
                </li>

                <li class="treeview">
                    <a><i class="fa fa-area-chart"></i><span>Apertura, Cierre </span> <i class="fa fa-angle-left pull-right"></i></a>
                    <ul class="treeview-menu">
                        <li><a href="{{url('cashbox/index')}}"><i class='fa fa-user-plus'></i> <span>Apertura y Cierre Caja</span></a></li>
                        <li><a href="{{url('feespaymentclose/index')}}"><i class='fa fa-money'></i> <span>Cierre de Ahorro</span></a></li>
                        <li><a href="{{url('feescreditclose/index')}}"><i class='fa fa-money'></i> <span>Cierre de Creditos</span></a></li>
                    </ul>
                </li>
                <li class="treeview">
                    <a><i class="fa fa-area-chart"></i><span> Movimiento Dinero </span> <i class="fa fa-angle-left pull-right"></i></a>
                    <ul class="treeview-menu">
                        <li><a href="{{url('payment/index')}}"><i class='fa fa-user-plus'></i> <span>Abonos</span></a></li>
                        <li><a href="{{url('expense/index')}}"><i class='fa fa-user-plus'></i> <span>Egresos</span></a></li>
                        <li><a href="{{url('deposit/index')}}"><i class='fa fa-user-plus'></i> <span>Depositos Diarios</span></a></li>
                        <li><a href="{{url('qualification/index')}}"><i class='fa fa-user-plus'></i> <span>Habilitaciones Gerencia</span></a></li>
                    </ul>
                </li>
                <li class="treeview">
                    <a><i class="fa fa-area-chart"></i><span>Reportes</span> <i class="fa fa-angle-left pull-right"></i></a>
                    <ul class="treeview-menu">
                        <li><a href="{{url('reportpayments/index')}}"><i class='fa fa-link'></i> <span>Reporte Pagos </span></a></li>
                        <li><a href="{{url('reportcloses/index')}}"><i class='fa fa-link'></i> <span>Reporte Cierres  </span></a></li>
                        <li><a href="{{url('balancesheet/index')}}"><i class='fa fa-link'></i> <span>Reporte Consolidado </span></a></li>
                    </ul>
                </li>
                <li class="treeview">
                    <a><i class="fa fa-area-chart"></i><span>Almacen</span> <i class="fa fa-angle-left pull-right"></i></a>
                    <ul class="treeview-menu">
                        <li><a href="{{url('provider/index')}}"><i class='fa fa-link'></i> <span>Proveedor </span></a></li>
                        <li><a href="{{url('purchase/index')}}"><i class='fa fa-link'></i> <span>Compras</span></a></li>
                    </ul>
                </li>
                <li><a href="{{url('employeed/index')}}"><i class='fa fa-user-plus'></i> <span>Empleados</span></a></li>
                <li class="treeview">
                    <a href="#"><i class="fa fa-product-hunt"></i><span>Productos</span> <i class="fa fa-angle-left pull-right"></i></a>
                    <ul class="treeview-menu">
                        <li><a href="{{url('object/index')}}"><i class='fa fa-link'></i> <span>Producto</span></a></li>
                        <li><a href="{{url('brandObject/index')}}"><i class='fa fa-link'></i> <span>Marca</span></a></li>
                        <li><a href="{{url('categoryObjects/index')}}"><i class='fa fa-link'></i> <span>Categoria</span></a></li>
                        <li><a href="{{url('objectModels/index')}}"><i class='fa fa-link'></i> <span>Modelo</span></a></li>
                        <li><a href="{{url('presentationObjects/index')}}"><i class='fa fa-link'></i> <span>Presentacion</span></a></li>
                        <li><a href="{{url('propertyObjects/index')}}"><i class='fa fa-link'></i> <span>Propiedad</span></a></li>
                    </ul>
                </li>
            
            @endif
            @if ( Auth::user()->positionid==2)
                <li><a href="{{url('credit/index')}}"><i class='fa fa-credit-card-alt'></i> <span>Creditos</span></a></li>
                <li><a href="{{url('sale/index')}}"><i class="fa fa-cart-plus"></i><span>Venta</span></a></li>
                <li><a href="{{url('client/index')}}"><i class='fa fa-user'></i> <span>Cliente</span></a></li>
                <li><a href="{{url('fixedterm/index')}}"><i class='fa fa-suitcase'></i> <span>Plazo Fijo</span></a></li> 
                <li><a href="{{url('fees/index')}}"><i class='fa fa-money'></i> <span>Deuda Clientes</span></a></li>
                <li><a href="{{url('feespayment/index')}}"><i class='fa fa-money'></i> <span>Abono Clientes</span></a></li>
                <li><a href="{{url('savingmoney/index')}}"><i class='fa fa-suitcase'></i> <span>JuntaMas</span></a></li>
                <!-- <li><a href="{{url('feespaymentclose/index')}}"><i class='fa fa-money'></i> <span>Cierre de Ahorro</span></a></li> -->
                <!-- <li><a href="{{url('feescreditclose/index')}}"><i class='fa fa-money'></i> <span>Cierre de Creditos</span></a></li> -->
                <li><a href="{{url('cashbox/index')}}"><i class='fa fa-user-plus'></i> <span>Apertura y Cierre Caja</span></a></li>
                <li><a href="{{url('payment/index')}}"><i class='fa fa-user-plus'></i> <span>Abonos</span></a></li>
                <li><a href="{{url('expense/index')}}"><i class='fa fa-user-plus'></i> <span>Egresos</span></a></li>
                <li><a href="{{url('deposit/index')}}"><i class='fa fa-user-plus'></i> <span>Depositos Diarios</span></a></li>
                <li><a href="{{url('qualification/index')}}"><i class='fa fa-user-plus'></i> <span>Habilitaciones Gerencia</span></a></li>
                <li><a href="{{url('reportpayments/index')}}"><i class='fa fa-link'></i> <span>Reporte Pagos </span></a></li>
                <li><a href="{{url('balancesheet/index')}}"><i class='fa fa-link'></i> <span>Reporte Consolidado </span></a></li>
            @endif
            @if ( Auth::user()->positionid==3)
                <!-- <li><a href="{{url('credit/index')}}"><i class='fa fa-credit-card-alt'></i> <span>Creditos</span></a></li> -->
                <!-- <li><a href="{{url('sale/index')}}"><i class="fa fa-cart-plus"></i><span>Venta</span></a></li> -->
                <li><a href="{{url('client/index')}}"><i class='fa fa-user'></i> <span>Cliente</span></a></li>
                <li><a href="{{url('fees/index')}}"><i class='fa fa-money'></i> <span>Deuda Clientes</span></a></li>
                <li><a href="{{url('feespayment/index')}}"><i class='fa fa-money'></i> <span>Abono Clientes</span></a></li>

                <!-- <li><a href="{{url('savingmoney/index')}}"><i class='fa fa-suitcase'></i> <span>JuntaMas</span></a></li> -->
            @endif
        </ul><!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->
</aside>
