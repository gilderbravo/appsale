<!-- REQUIRED JS SCRIPTS -->

<!-- JQuery and bootstrap are required by Laravel 5.3 in resources/assets/js/bootstrap.js-->
<!-- Laravel App -->
<script src="{{asset('js/jquery-3.3.1.min.js')}}"></script>
<!-- Bootstrap 3.3.5 -->
{{-- <script src="{{asset('js/bootstrap.min.js')}}"></script>--}}
<!-- AdminLTE App -->
<script src="{{asset('js/app.js')}}"></script>
<!-- Angular Js -->
<script src="{{asset('js/angular.js')}}"></script>
<script src="{{asset('js/app_angular.js')}}"></script>
<script src="{{asset('js/ng-alertify.js')}}"></script>
<script src="{{asset('js/services/service.js')}}"></script>
<script src="{{asset('js/paginate.js')}}"></script>
<script src="{{asset('js/bootstrap-datepicker.js')}}"></script>
<script src="{{asset('js/jquery-migrate-1.1.0.js')}}"></script>
<script src="{{asset('js/jquery.jqprint-0.3.js')}}"></script>
<script src="{{asset('js/script.js')}}"></script>
<script src="{{asset('select2/dist/js/select2.min.js')}}"></script>
<script src="{{asset('js/FileSaver.js')}}"></script>
<script src="{{asset('js/print.js')}}"></script>
<script src="{{asset('js/angular-select2.min.js')}}"></script>

@yield('javascript')
<!-- Optionally, you can add Slimscroll and FastClick plugins.
      Both of these plugins are recommended to enhance the
      user experience. Slimscroll is required when using the
      fixed layout. -->
<script>
    window.Laravel = {!! json_encode([
        'csrfToken' => csrf_token(),
    ]) !!};
</script>
