@extends('adminlte::layouts.app')
@section('htmlheader_title')
	| Presentacion del Objeto
@endsection
@section('contentheader_title')
	Presentacion <a href="#" ng-click="newPresentationObjects();" data-target="#modal-presentationObjects" data-toggle="modal" class="btn btn-success btn-xs">Nuevo</a>
@endsection
@section('angularcontroller')
ng-controller="presentationObjectsController" 
@endsection
@section('main-content')
	<div class="container-fluid spark-screen" ng-cloak>
		<div class="row">
			<div class="col-md-12">
				<div class="panel panel-default">
					<div class="panel-heading">Listado de Presentacion
						
					</div>
					
					<div class="panel-body">
						<table class="table table-striped table-bordered table-condensed table-hover">
							<thead>
								<th>Id</th>
								<th>Nombre</th>	
								<th>Descripción</th>				
								<th>Accion</th>									
							</thead>               
							<tr ng-repeat="item in presentations.list">									
								<td>@{{item.id}}</td>
								<td>@{{item.name}}</td>
								<td>@{{item.description}}</td>		
								<td>
									<a href="#" ng-click="loadPresentationObjects(item);" data-target="#modal-presentationObjects" data-toggle="modal" class="btn btn-warning btn-xs"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
									<a href="#" ng-click="loadPresentationObjects(item);" data-toggle="modal"  data-target="#modal-presentationObjects-delete" class="btn btn-danger btn-xs"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
								</td>
							</tr>									
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
	@include ('presentationObjects.modal-create')
	@include ('presentationObjects.modal-delete')
@endsection
@section ('javascript')
<script src="{{asset('js/controllers/presentationObjectsController.js')}}"></script>
@endsection