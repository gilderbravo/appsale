<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGeneralTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('correlatives', function (Blueprint $table) {
            $table->increments('id');
            $table->string('seriebill');
            $table->string('numberbill');
            $table->string('numberticket'); 
            $table->string('serieticket'); 
            $table->string('numbernote'); 
            $table->string('serienote'); 
            $table->string('seriefee'); 
            $table->string('numberfee');
        });

        Schema::create('brandobjects', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->integer('status');            
        });
        Schema::create('propertyobjects', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->text('description')->nullable();            
        });
        Schema::create('objectmodels', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('brandobjectid')->unsigned();
            $table->integer('propertyobjectid')->unsigned();
            $table->string('name');
            $table->integer('status'); 
            $table->text('description')->nullable(); 
            
            $table->foreign('brandobjectid')->references('id')->on('brandobjects');
            $table->foreign('propertyobjectid')->references('id')->on('propertyobjects');
        });
        Schema::create('objectcategories', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->text('description')->nullable();
            $table->integer('parendid')->nullable();
            $table->integer('levelnumber')->nullable();
            $table->integer('status')->nullable();            
        });
        Schema::create('presentationobjects', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->text('description')->nullable();
            $table->integer('status')->nullable();            
        });

        Schema::create('objects', function (Blueprint $table) {
            $table->increments('id'); 
            $table->string('code')->nullable();
            $table->string('barcode')->nullable();
            $table->string('name');
            $table->text('description')->nullable();  
            $table->integer('presentationobjectid')->unsigned();                                                                                             
            $table->integer('objectcategoryid')->unsigned();                                                                                             
            $table->integer('objectmodelid')->unsigned();                                                                                             
            $table->integer('status')->nullable();            

            $table->foreign('presentationobjectid')->references('id')->on('presentationobjects');
            $table->foreign('objectcategoryid')->references('id')->on('objectcategories');
            $table->foreign('objectmodelid')->references('id')->on('objectmodels');
        });
        Schema::create('rates', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->text('description')->nullable();
            $table->integer('status')->nullable();            
        });

        Schema::create('valuedobjects', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('objectid')->unsigned();            
            $table->integer('rateid')->unsigned();      
            $table->double('amount', 8, 2)->nullable();      
            $table->dateTime('fromtime')->nullable();
            $table->dateTime('totime')->nullable();
            $table->dateTime('modificationdate')->nullable();
            
            $table->foreign('objectid')->references('id')->on('objects');
            $table->foreign('rateid')->references('id')->on('rates');

        });
        Schema::create('subjecttypes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');            
        });
        Schema::create('identitydocuments', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('length')->nullable();
        });
        Schema::create('identitydocumento4subjecttypes', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('identitydocumentid')->unsigned();            
            $table->integer('subjecttypeid')->unsigned();  
            $table->foreign('identitydocumentid')->references('id')->on('identitydocuments');
            $table->foreign('subjecttypeid')->references('id')->on('subjecttypes');
        });
        Schema::create('subjectrolcategories', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->text('description')->nullable();
        });
        Schema::create('subjects', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->integer('identitydocumentid')->unsigned();            
            $table->integer('identitynumber')->nullable();            
            $table->string('firstname')->nullable();
            $table->string('secondname')->nullable();
            $table->dateTime('birthday')->nullable();
            $table->string('address')->nullable();
            $table->string('phone')->nullable();
            $table->foreign('identitydocumentid')->references('id')->on('identitydocuments');

        });
        Schema::create('businesssubjects', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('subjectid')->unsigned();
            $table->integer('identitydocumentid')->unsigned();            
            $table->integer('identitynumber')->nullable();            
            $table->string('businessname')->nullable();            
            $table->integer('subjectrolcategoryid')->unsigned();            
            $table->string('startdate')->nullable();
            $table->string('address')->nullable();
            $table->string('phone')->nullable();
            $table->string('cyty')->nullable();
            $table->integer('status')->nullable();
            $table->string('email')->nullable();
            $table->foreign('identitydocumentid')->references('id')->on('identitydocuments');
            $table->foreign('subjectrolcategoryid')->references('id')->on('subjectrolcategories');
            $table->foreign('subjectid')->references('id')->on('subjects');
        });
        Schema::create('positions', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->nullable();
            $table->integer('status');
        });
        Schema::create('employeds', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('businesssubjectid')->unsigned();            
            $table->integer('positionid')->unsigned();   
            $table->foreign('businesssubjectid')->references('id')->on('businesssubjects');
            $table->foreign('positionid')->references('id')->on('positions');
                 
        });

        Schema::create('transactiontypes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');            
            $table->text('description')->nullable();                    
        });
        Schema::create('vouchertypes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('shortname')->nullable();            
            $table->string('name');            
            $table->text('description')->nullable();                    
        });
        Schema::create('voucherserials', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('number')->nullable();
            $table->integer('ownerid')->nullable();
            $table->integer('vouchertypeid')->unsigned();  
             
            $table->foreign('vouchertypeid')->references('id')->on('vouchertypes');
               
        });
        Schema::create('vouchers', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('vouchertypeid')->unsigned();  
            $table->integer('serialid');
            $table->string('number')->nullable();
            $table->string('serialnumber')->nullable();
            $table->string('code')->nullable();

            $table->foreign('vouchertypeid')->references('id')->on('vouchertypes');
               
        });
        Schema::create('transactions', function (Blueprint $table) {
            $table->increments('id');
            $table->dateTime('time')->nullable();
            $table->integer('voucherid')->unsigned();  
            $table->integer('externalsubjectid');  
            $table->integer('transactiontypeid')->unsigned();  
            $table->dateTime('updateat')->nullable();
            $table->text('comment')->nullable();
            $table->foreign('voucherid')->references('id')->on('vouchers');
            $table->foreign('transactiontypeid')->references('id')->on('transactiontypes');
               
        });
        Schema::create('subtransactions', function (Blueprint $table) {
            $table->increments('id');
            $table->dateTime('time')->nullable();
            $table->integer('employedid')->unsigned();
            $table->double('totalmoney', 8, 2);
            $table->integer('voucherid')->unsigned();
            $table->integer('transactionid')->unsigned();
            $table->integer('transactiontypeid')->unsigned();  
            $table->dateTime('updateat')->nullable();
            $table->text('comment')->nullable();
            $table->double('igv', 8,2)->nullable();
            $table->string('applytax')->nullable();
            $table->double('interestrate', 8,2)->nullable();
            $table->double('paymentfees', 8,2)->nullable();
            $table->double('frecuencyday', 8,2)->nullable();

            $table->foreign('transactionid')->references('id')->on('transactions');
            $table->foreign('employedid')->references('id')->on('employeds');
            $table->foreign('voucherid')->references('id')->on('vouchers');
            $table->foreign('transactiontypeid')->references('id')->on('transactiontypes');
                  
        });
        Schema::create('paymentschedules', function (Blueprint $table) {
            $table->increments('id');
            $table->dateTime('time')->nullable();
            $table->integer('businesssubjectid')->unsigned();
            $table->integer('subtransactionid')->unsigned();
            $table->double('capital', 8, 2)->nullable();
            $table->double('interest', 8, 2)->nullable();
            $table->double('feeamount', 8,2)->nullable();
            $table->string('day')->nullable();
            $table->integer('status')->unsigned();
            $table->dateTime('deleted_at')->nullable();
            $table->integer('numberdues')->nullable();
            $table->foreign('businesssubjectid')->references('id')->on('businesssubjects');
            $table->foreign('subtransactionid')->references('id')->on('subtransactions');                  
        });
        Schema::create('states', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->text('description')->nullable();               
        });
        Schema::create('subtransactionstates', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('subtransactionid')->unsigned();
            $table->integer('stateid')->unsigned();
            $table->dateTime('atdate')->nullable();  
            $table->text('comment')->nullable();
            $table->foreign('stateid')->references('id')->on('states');
            $table->foreign('subtransactionid')->references('id')->on('subtransactions');

        });
        Schema::create('transactionstates', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('transactionid')->unsigned();
            $table->integer('stateid')->unsigned();
            $table->dateTime('atdate')->nullable();  
            $table->text('comment')->nullable();
            $table->foreign('stateid')->references('id')->on('states');
            $table->foreign('transactionid')->references('id')->on('transactions');

        });
        Schema::create('fees', function (Blueprint $table) {
            $table->increments('id');
            $table->string('code')->nullable();
            $table->dateTime('issedtime')->nullable();  
            $table->dateTime('limittime')->nullable();  
            $table->double('amount', 8,2)->nullable();
            $table->double('capital', 8,2)->nullable();
            $table->double('interest', 8,2)->nullable();            
            $table->integer('subtransactionid')->unsigned();
            $table->text('comment')->nullable();
            $table->integer('voucherid')->unsigned();
            $table->string('feechanged')->nullable();
            $table->double('cash', 8,2)->nullable();
            $table->string('referencespayment')->nullable();
            $table->foreign('voucherid')->references('id')->on('vouchers');
            $table->foreign('subtransactionid')->references('id')->on('subtransactions');
               
        });
        Schema::create('feestates', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('feeid')->unsigned();
            $table->integer('stateid')->unsigned();
            $table->dateTime('atdate')->nullable();  
            $table->text('comment')->nullable();
            $table->foreign('feeid')->references('id')->on('fees');
            $table->foreign('stateid')->references('id')->on('states');
               
        });
        Schema::create('subtransactiondetails', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('subtransactionid')->unsigned();
            $table->integer('valuedobjectid')->unsigned();
            $table->integer('objectid')->unsigned();        
            $table->double('amount', 8,2)->nullable();
            $table->double('unitprice', 8,2)->nullable();
            $table->double('totalmoney', 8,2)->nullable();
            $table->double('secondaryamount', 8,2)->nullable();
            $table->double('unitpricereal', 8,2)->nullable();       

            $table->foreign('subtransactionid')->references('id')->on('subtransactions');
            $table->foreign('valuedobjectid')->references('id')->on('valuedobjects');
            $table->foreign('objectid')->references('id')->on('objects');
               
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // Schema::dropIfExists('general');
    }
}
