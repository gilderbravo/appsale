INSERT INTO bdsisventa.correlatives (id,seriebill,numberbill,numberticket,serieticket,numbernote,serienote,seriefee,numberfee) VALUES (1,'FA01','00000000','00000000','BA01','00000000','001','F01','00000000');
INSERT INTO bdsisventa.brandobjects (id,name,status) VALUES (1,'Generico',1);
INSERT INTO bdsisventa.identitydocuments (id,name,length) VALUES (1,'DNI','8'),(2,'RUC','11');
INSERT INTO bdsisventa.objectcategories (id,name,description,parendid,levelnumber,status) VALUES (1,'Generico','Generico',0,1,1);
INSERT INTO bdsisventa.positions (id,name,status) VALUES (1,'Administrador',1),(2,'Caja',1),(3,'Ventas',1);
INSERT INTO bdsisventa.presentationobjects (id,name,description,status) VALUES (1,'Unidad','unidades',1);
INSERT INTO bdsisventa.propertyobjects (id,name,description) VALUES (1,'propiedad Generico','Generico');
INSERT INTO bdsisventa.rates (id,name,description,status) VALUES (1,'Precio de venta','relacion para ventas ',1),(2,'Precio de compra','precio de compra',1);
INSERT INTO bdsisventa.states (id,name,description) VALUES (1,'Pendiente',NULL),(2,'Aceptado',NULL),(3,'Rechazado',NULL),(4,'Anulado','para comprobantes anulados'),(5, 'Generado', 'Generado el Cronograma de pagos'),(6, 'Pagado', 'para finalizar el proceso de los pagos');;
INSERT INTO bdsisventa.subjectrolcategories (id,name,description) VALUES (1,'Cliente','Cliente para ventas'),(2,'Empleados','Para Empleados');
INSERT INTO bdsisventa.subjecttypes (id,name) VALUES (1,'persona natural'),(2,'persona juridica');
INSERT INTO bdsisventa.transactiontypes (id,name,description) VALUES (1,'Ventas',''),(2,'Compras','');
INSERT INTO bdsisventa.vouchertypes (id,shortname,name,description) VALUES (1,'Boleta','Boleta','Para ventas con Boleta'),(2,'Factura','Factura','Para Ventas factura'),(3,'Nota Venta','Nota Venta','Para Ventas con Nota de Venta'),(4,'Pagos','Pagos','Para los comprobantes de pago');
INSERT INTO bdsisventa.objectmodels (id,brandobjectid,propertyobjectid,name,status,description) VALUES (1,1,1,'Gerenerico',1,'Modelo Generico');
INSERT INTO bdsisventa.objects (id, code, barcode, name, description, presentationobjectid, objectcategoryid, objectmodelid, status) VALUES (1, '', '', 'Prestamos de Dinero', 'Producto generico para prestamos de dinero', 1, 1, 1, 1);
INSERT INTO bdsisventa.valuedobjects (id, objectid, rateid, amount, fromtime, totime, modificationdate) VALUES (1, 1, 1, 0, '2020-03-15 12:11:10', '2020-03-15 12:11:10', '2020-03-15 12:11:10'),(2, 1, 2, 0, '2020-03-15 12:11:10', '2020-03-15 12:11:10', '2020-03-15 12:11:10');

CREATE TABLE bdsisventa.cashboxs (
	id INT auto_increment NOT NULL ,
	employedid INT NULL,
	openingdate DATETIME NULL,
	initamount DOUBLE NULL,
	stateid INT NULL,
	finalamount DOUBLE NULL,
	closingdate DATETIME NULL,
	totalpaymentamount DOUBLE NULL,
	totalexpenseamount DOUBLE NULL,
	totalfeeamount DOUBLE NULL,
	ticketing  varchar(250) NULL,
	PRIMARY KEY (`id`)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8mb4
COLLATE=utf8mb4_general_ci;

