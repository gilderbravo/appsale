<?php

namespace App\Repositories;
use Illuminate\Database\Eloquent\Model;
use App\Model\Objectcategories;
use DB;

class CategoryObjectRepository extends BaseRepository
{
    public function __construct(){
        $this->model=new Objectcategories;
    }
    public function listcategory($params){
        $query=DB::table('objectcategories')->where('status','=','1')->get();
        return $query;    
    }

    public function saveCategoryObject($params){                
        try{           
           $isnew=true;
           $categoryObject= new Objectcategories;
           if(  array_key_exists("id",$params) ){
               $isnew=false;
               $categoryObject = $this->getById($params['id']);
           }
           $categoryObject->name=$params['name'];
           $categoryObject->description=$params['description'];
           $categoryObject->parendid=0;
           $categoryObject->levelnumber=1;
           $categoryObject->status=1;           
           if($isnew) $categoryObject->save();
           else $categoryObject->update(); 
           
       }catch(Exception $e){
           throw new Exception("R_ERROR:".$e->getMessage());
       }
           return $categoryObject;        
   }
    public function delete($params){
    }
}
