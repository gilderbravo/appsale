<?php

namespace App\Repositories;

use Illuminate\Database\Eloquent\Model;
use App\Model\Objectmodels;
use DB;
class ObjectModelRepository extends BaseRepository
{
    public function __construct(){
        $this->model=new Objectmodels;
    }
    public function listObjectModel($params){
        $query=DB::table('objectmodels as om')
        ->join('brandobjects as bo','bo.id','=','om.brandobjectid')
        ->join('propertyobjects as po','po.id','=','om.propertyobjectid')
        ->select('po.name as property','po.id as propertyobjectid','bo.id as brandobjectid','bo.name as brand','om.id','om.name','om.description','om.status')
        ->where('om.status','=','1')->get();
        return $query;    
    }

    public function saveObjectModel($params){                
        try{           
           $isnew=true;
           $objectModel= new Objectmodels;
           if(  array_key_exists("id",$params) ){
               $isnew=false;
               $objectModel = $this->getById($params['id']);
           }
           $objectModel->brandobjectid=$params['brandobjectid'];
           $objectModel->propertyobjectid=$params['propertyobjectid'];
           $objectModel->name=$params['name'];
           $objectModel->status=1;    
           $objectModel->description=$params['description'];
       
           if($isnew) $objectModel->save();
           else $objectModel->update(); 
           
       }catch(Exception $e){
           throw new Exception("R_ERROR:".$e->getMessage());
       }
           return $objectModel;        
   }
    public function delete($params){
    }
}
