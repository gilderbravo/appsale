<?php

namespace App\Repositories;

use Illuminate\Database\Eloquent\Model;
use App\Model\Subtransactions;
use App\Model\Transactions;
use App\Model\Subtransactionstates;
use App\Model\Subtransactiondetails;
use App\Model\Transactionstates;
use App\Model\Vouchers;
use App\Model\Correlatives;
use App\Model\Paymentschedules;
use App\Model\Fees;

use Illuminate\Support\Facades\Auth;
use DateTime;
use DB;

class ReportRepository extends BaseRepository
{
    public function __construct(){
        $this->subtransaction=new Subtransactions;
        $this->transaction=new Transactions;
        $this->subtransactionstate=new Subtransactionstates;
        $this->subtransactiondetail=new Subtransactiondetails;
        $this->transactionstate=new Transactionstates;
        $this->voucher=new Vouchers;
        $this->correlatives=new Correlatives;
        $this->paymentschedule=new Paymentschedules;
        $this->fee=new Fees;
    }

    public function listReportpayments($params){
        $sql="select u.name as systemuser,s.transactiontypeid,f.amount AS cash,f.limittime,  DATE_FORMAT(STR_TO_DATE(f.limittime,'%Y-%m-%d'), '%d/%m/%Y') as date,vt.name as vouchertype,  
            v.code,sts.stateid,  CONCAT(sb.name,' ',sb.firstname,' ',sb.secondname ) as client , f.capital,vf.code as codefee,f.comment,f.referencespayment,f.id as feeid,f.accountant,s.season 
            ,(case when  STR_TO_DATE(f.limittime ,'%Y-%m-%d') = STR_TO_DATE(s.time ,'%Y-%m-%d') then 'Nuevos' else 'Frecuentes' end ) type 
            from fees f 
            inner join subtransactions s on f.subtransactionid=s.id 
            inner join transactions bt on 	s.transactionid= bt.id 
            inner join businesssubjects bus on 	bt.externalsubjectid = bus.id 
            inner join subjects sb on sb.id=bus.subjectid 
            inner join subtransactionstates sts on sts.subtransactionid=s.id 
            INNER JOIN vouchers v ON s.voucherid=v.id 
            INNER JOIN vouchertypes vt ON v.vouchertypeid=vt.id 
            inner JOIN users u on u.id=f.employedid  
            INNER JOIN vouchers vf ON f.voucherid =vf.id 
            where s.transactiontypeid in (1,3) AND sts.stateid != 4 ";
        if (array_key_exists('filtro',$params)){
            if(!empty($params['filtro'])){
                $sql.= " AND (vf.code LIKE '%$params[filtro]%' OR v.code LIKE '%$params[filtro]%' OR sb.name LIKE '%$params[filtro]%' OR sb.firstname LIKE '%$params[filtro]%' OR sb.secondname LIKE '%$params[filtro]%' or u.name LIKE '%$params[filtro]%' ) ";
            }
        }
        if (array_key_exists('season',$params)){
            if(!empty($params['season'])){
                if ($params['season'] == 'JuntaMas' ) {
                    $sql.= " and (s.season = '$params[season]' or s.season is null ) and vt.id=3 ";
                }else{
                    $sql.= " AND s.season=('$params[season]') ";
                }
                
            }else{
                $sql.= " AND (s.season in ('Creditos General' , 'Credito General' ,'Credito Comercial') or s.season is null)    ";
            }
        }
        if (array_key_exists('dateinit',$params)){
            if(!empty($params['dateinit'])){
                $dateinit=explode("/",$params['dateinit']);
                $dateend=explode("/",$params['dateend']);
                $sql.= "and STR_TO_DATE(f.limittime ,'%Y-%m-%d') between '$dateinit[2]-$dateinit[1]-$dateinit[0]' and '$dateend[2]-$dateend[1]-$dateend[0]'  ";
            }
        }
        $sql.=" order by f.id desc ";
        if(array_key_exists('count',$params)){
            if(!$params['count'] && array_key_exists('limit',$params)){
                $sql.=" LIMIT $params[limit], $params[xpagina] ";
            }
        }
        $query=DB::select($sql);
        return $query;
    }
    public function listReportTransactionCloses($params){
        $sql="select t.code, t.issedtime,t.amount,t.capital,t.interest,u.name,v.code as codeorigin,
            CONCAT(sb.name,' ',sb.firstname,' ',sb.secondname ) as client,( t.amount + interest) as total 
            ,s.id as subtransactionid,t.id,sb.identitynumber 
            from transactioncloses t inner join subtransactions s on s.id = t.subtransactionid
            inner join transactions bt on 	s.transactionid= bt.id 
            inner join businesssubjects bus on 	bt.externalsubjectid = bus.id 
            inner join subjects sb on sb.id=bus.subjectid 
            inner join  vouchers v ON s.voucherid=v.id inner JOIN users u on u.id=t.employedid where ";
       
        if (array_key_exists('dateinit',$params)){
            if(!empty($params['dateinit'])){
                $dateinit=explode("/",$params['dateinit']);
                $dateend=explode("/",$params['dateend']);
                $sql.= " STR_TO_DATE(t.issedtime ,'%Y-%m-%d') between '$dateinit[2]-$dateinit[1]-$dateinit[0]' and '$dateend[2]-$dateend[1]-$dateend[0]'  ";
            }
        }
        $sql.=" order by t.id desc ";
        if(array_key_exists('count',$params)){
            if(!$params['count'] && array_key_exists('limit',$params)){
                $sql.=" LIMIT $params[limit], $params[xpagina] ";
            }
        }
        $query=DB::select($sql);
        return $query;
    }
    public function deletefee($params){
        try{     
            DB::beginTransaction();
            $userloginid = Auth::id();
    
            if (array_key_exists("feeid",$params)) {
                $myArray = explode(',', $params['referencespayment']);
                foreach($myArray as $my_Array){
                    DB::update("update paymentschedules set status = ? where id in (?) ", ["1" , $my_Array]);
                }
                // Paymentschedules::where('id', 'in', $params['referencespayment'])->update(['status' => 1]);
                // DB::update("update paymentschedules set status = ? where id in (?) ", ["1" , $params['referencespayment'] ]);

                $fee = $this->fee->findOrFail($params['feeid']);                
                $fee->delete();
    
                DB::commit(); 
            }   
            
        }catch(Exception $e){
            DB::rollback();
           throw new Exception("R_ERROR:".$e->getMessage());
       }catch (Throwable $e) {
            DB::rollback();
            throw new Exception("R_ERROR:".$e->getMessage());
       }
           return 1;    
    }
    
    public function updateCreditReversal($params){
        try{     
            DB::beginTransaction();
            $userloginid = Auth::id();
    
            if (array_key_exists("subtransactionid",$params)) {
                DB::update("update transactioncloses set isreversal = 1,modify_date=current_timestamp,user_reversal=? where id in (?) ", [ $userloginid,$params['id']]);
                DB::update("update subtransactionstates set stateid = 5 where subtransactionid in (?) ", [ $params['subtransactionid']]);
                DB::commit(); 
                
            }   
            
        }catch(Exception $e){
            DB::rollback();
           throw new Exception("R_ERROR:".$e->getMessage());
       }catch (Throwable $e) {
            DB::rollback();
            throw new Exception("R_ERROR:".$e->getMessage());
       }
           return 1;    
    }
}
