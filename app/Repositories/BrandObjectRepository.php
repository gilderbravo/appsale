<?php
namespace App\Repositories;
use Illuminate\Database\Eloquent\Model;
use App\Model\Brandobjects;
use DB;

class BrandObjectRepository extends BaseRepository
{
    public function __construct(){
        $this->model=new Brandobjects;
    }
    public function allBrandObject($params){
        $query=DB::table('brandobjects')->where('status','=','1')->get();
        return $query;    
    }
    public function saveBrandObject($params){                
        try{           
           $isnew=true;
           $brandObject= new Brandobjects;
           if(  array_key_exists("id",$params) ){
               $isnew=false;
               $brandObject = $this->getById($params['id']);
           }
           $brandObject->name=$params['name'];           
           $brandObject->status=$params['status'];           
           if($isnew) $brandObject->save();
           else $brandObject->update(); 
           
       }catch(Exception $e){
           throw new Exception("R_ERROR:".$e->getMessage());
       }
           return $brandObject;        
   }
    public function deleteBrandObject($params){
        $brandObject= new Brandobjects;
        $brandObject = $this->getById($params->id);
        $brandObject->status=0;           
        $brandObject->update();            
        return $brandObject;
    }
}
