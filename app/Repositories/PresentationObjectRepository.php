<?php

namespace App\Repositories;
use Illuminate\Database\Eloquent\Model;
use App\Model\Presentationobjects;
use DB;

class PresentationObjectRepository extends BaseRepository
{
    public function __construct(){
        $this->model=new Presentationobjects;
    }
    public function listpresentation($params){
        $query=DB::table('presentationobjects')->where('status','=','1')->get();
        return $query;    
    }

    public function savePresentationObjects($params){                
        try{           
           $isnew=true;
           $presentationObjects= new Presentationobjects;
           if(  array_key_exists("id",$params) ){
               $isnew=false;
               $presentationObjects = $this->getById($params['id']);
           }
           $presentationObjects->name=$params['name'];
           $presentationObjects->description=$params['description'];
           $presentationObjects->status=1;           
           if($isnew) $presentationObjects->save();
           else $presentationObjects->update(); 
           
       }catch(Exception $e){
           throw new Exception("R_ERROR:".$e->getMessage());
       }
           return $presentationObjects;        
   }
    public function delete($params){
    }
}
