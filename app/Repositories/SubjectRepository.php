<?php

namespace App\Repositories;

use Illuminate\Database\Eloquent\Model;
use App\Model\Subjects;
use App\Model\Businesssubjects;
use App\Model\Identitydocuments;
use App\Model\Subjectrolcategories;
use App\Model\Positions;
use App\Model\Employeds;
use App\User;
use DateTime;
use DB;
class SubjectRepository extends BaseRepository
{
    public function __construct(){
        $this->model=new Subjects;
        $this->businesssubjects=new Businesssubjects;
        $this->subjectrolcategories=new Subjectrolcategories;
        $this->positions=new Positions;
        $this->employeed=new Employeds;
        $this->user=new User;
        $this->subject=new Subjects;
    }
    public function listClient($params){
        $sql="select bs.id,s.id as subjectid,s.name,s.firstname,s.secondname,ids.id as identitydocumentid,ids.name as document,
                    bs.identitynumber,bs.businessname,sr.id as subjectrolcategoryid,sr.name as subjectrolcategory,bs.startdate,
                    bs.address,bs.phone,bs.cyty,bs.status  
                    from businesssubjects bs 
                    inner join subjects s on s.id=bs.subjectid 
                    inner join  subjectrolcategories sr on sr.id=bs.subjectrolcategoryid
                    inner join identitydocuments ids on ids.id=bs.identitydocumentid
                    where bs.status = 1 and sr.id=1";
        if (array_key_exists('filtro',$params)){
            if(!empty($params['filtro'])){
                $sql.= " AND (s.name LIKE '%$params[filtro]%' OR s.firstname LIKE '%$params[filtro]%' OR bs.identitynumber LIKE '%$params[filtro]%' ) ";
            }
        }
        $sql.=" order by s.id desc ";
        if(array_key_exists('count',$params)){
            if(!$params['count'] && array_key_exists('limit',$params)){
                $sql.=" LIMIT $params[limit], $params[xpagina] ";
            }
        }
        
        $query=DB::select($sql);
        return $query;    
    }
    public function listProvider($params){
        $sql="select bs.id,s.id as subjectid,s.name,s.firstname,s.secondname,ids.id as identitydocumentid,ids.name as document,
                    bs.identitynumber,bs.businessname,sr.id as subjectrolcategoryid,sr.name as subjectrolcategory,bs.startdate,
                    bs.address,bs.phone,bs.cyty,bs.status  
                    from businesssubjects bs 
                    inner join subjects s on s.id=bs.subjectid 
                    inner join  subjectrolcategories sr on sr.id=bs.subjectrolcategoryid
                    inner join identitydocuments ids on ids.id=bs.identitydocumentid
                    where bs.status = 1 and sr.id=3";
        if (array_key_exists('filtro',$params)){
            if(!empty($params['filtro'])){
                $sql.= " AND (s.name LIKE '%$params[filtro]%' OR s.firstname LIKE '%$params[filtro]%' OR bs.identitynumber LIKE '%$params[filtro]%' ) ";
            }
        }
        $sql.=" order by s.id desc ";
        if(array_key_exists('count',$params)){
            if(!$params['count'] && array_key_exists('limit',$params)){
                $sql.=" LIMIT $params[limit], $params[xpagina] ";
            }
        }
        
        $query=DB::select($sql);
        return $query;    
    }
    public function saveClient($params){                               
        try{     
           DB::transaction(function() use($params){           
            $isnewsubject=true;
            $subjects= new Subjects;            
            if(  array_key_exists("subjectid",$params) ){
                $isnewsubject=false;
                $subjects = $this->subject->findOrFail($params['subjectid']);
                // $subjects = $this->getById($params['subjectid']);
            }            
            $subjects->name=$params['name'];
            $subjects->identitydocumentid=$params['identitydocumentid'];
            $subjects->identitynumber=$params['identitynumber'];            
            $subjects->firstname=array_key_exists("firstname",$params)?$params['firstname']:null;
            $subjects->secondname='';
            $subjects->birthday=array_key_exists("birthday",$params)?date('Y-m-d H:i:s', strtotime($params['birthday'])):null;
            $subjects->address=array_key_exists("address",$params)?$params['address']:null;
            $subjects->phone=array_key_exists("phone",$params)?$params['phone']:null;

            if($isnewsubject) $subjects->save();
            else $subjects->update(); 

            $isnew=true;
           $businesssubjects= new Businesssubjects;
           if(  array_key_exists("id",$params) ){
               $isnew=false;
               $businesssubjects = $this->businesssubjects->findOrFail($params['id']);
           }
           $businesssubjects->subjectid=$subjects->id;
           $businesssubjects->identitydocumentid=$params['identitydocumentid'];
           $businesssubjects->identitynumber=$params['identitynumber'];
           $businesssubjects->businessname=$params['name']." ".array_key_exists("firstname",$params)?$params['firstname']:null;
           $businesssubjects->subjectrolcategoryid=array_key_exists("subjectrolcategoryid",$params)?$params['subjectrolcategoryid']:1;
           $businesssubjects->startdate=new DateTime();
           $businesssubjects->address=array_key_exists("address",$params)?$params['address']:null;
           $businesssubjects->phone=array_key_exists("phone",$params)?$params['phone']:null;
           $businesssubjects->cyty=array_key_exists("cyty",$params)?$params['cyty']:null;
           $businesssubjects->status=1;        
            if($isnew) $businesssubjects->save();
            else $businesssubjects->update();         
         });
        
       }catch(Exception $e){
           throw new Exception("R_ERROR:".$e->getMessage());
       }
           return 1;        
   }

    public function delete($params){
    }
    public function listIdentityDocument($params){       
            $query=DB::table('identitydocuments')->get();
            return $query;    
    }
    public function listPositions($params){
        $query=DB::table('positions')->get();
        return $query;
    }

    public function listEmployeed($params){
        $sql="select bs.id,s.id as subjectid,s.name,s.firstname,s.secondname,bs.email,ids.id as identitydocumentid,ids.name as document,
                    bs.identitynumber,bs.businessname,sr.id as subjectrolcategoryid,sr.name as subjectrolcategory,bs.startdate,
                    bs.address,bs.phone,bs.cyty,bs.status,e.id as employedid,e.positionid,u.id as userid 
                    from businesssubjects bs 
                    inner join subjects s on s.id=bs.subjectid 
                    inner join  subjectrolcategories sr on sr.id=bs.subjectrolcategoryid
                    inner join identitydocuments ids on ids.id=bs.identitydocumentid
                    inner join employeds e on e.businesssubjectid=bs.id 
                    left join users u on u.employedid =e.id 
                    where bs.status = 1 AND sr.id=2 ";
        if (array_key_exists('filtro',$params)){
            if(!empty($params['filtro'])){
                $sql.= " AND (s.name LIKE '%$params[filtro]%' OR s.firstname LIKE '%$params[filtro]%' OR bs.identitynumber LIKE '%$params[filtro]%' ) ";
            }
        }
        $sql.=" order by s.id desc ";
        if(array_key_exists('count',$params)){
            if(!$params['count'] && array_key_exists('limit',$params)){
                $sql.=" LIMIT $params[limit], $params[xpagina] ";
            }
        }
        $query=DB::select($sql);
        return $query;
    }
    public function saveEmployeed($params){
        try{
            DB::transaction(function() use($params){

                if(!array_key_exists("subjectid",$params) ) {
                    $subjects = new Subjects;
                    $subjects->name = $params['name'];
                    $subjects->identitydocumentid = $params['identitydocumentid'];
                    $subjects->identitynumber = $params['identitynumber'];
                    $subjects->firstname = array_key_exists("firstname", $params) ? $params['firstname'] : null;
                    $subjects->secondname = '';
                    $subjects->birthday = array_key_exists("birthday", $params) ? date('Y-m-d H:i:s', strtotime($params['birthday'])) : null;
                    $subjects->address = array_key_exists("address", $params) ? $params['address'] : null;
                    $subjects->phone = array_key_exists("phone", $params) ? $params['phone'] : null;
                    $subjects->save();

                    $businesssubjects = new Businesssubjects;
                    $businesssubjects->subjectid = $subjects->id;
                    $businesssubjects->identitydocumentid = $params['identitydocumentid'];
                    $businesssubjects->identitynumber = $params['identitynumber'];
                    $businesssubjects->email = $params['email'];
                    $businesssubjects->businessname = $params['name'] . " " . array_key_exists("firstname", $params) ? $params['firstname'] : null;
                    $businesssubjects->subjectrolcategoryid = 2;
                    $businesssubjects->startdate = new DateTime();
                    $businesssubjects->address = array_key_exists("address", $params) ? $params['address'] : null;
                    $businesssubjects->phone = array_key_exists("phone", $params) ? $params['phone'] : null;
                    $businesssubjects->cyty = array_key_exists("cyty", $params) ? $params['cyty'] : null;
                    $businesssubjects->status = 1;
                    $businesssubjects->save();

                    $employeed=new Employeds;
                    $employeed->businesssubjectid=$businesssubjects->id;
                    $employeed->positionid=array_key_exists("positionid", $params) ? $params['positionid'] : 3;
                    $employeed->save();

                }else{
                    $subjects = $this->model->findOrFail($params["subjectid"]);
                    $subjects->name = $params['name'];
                    $subjects->identitydocumentid = $params['identitydocumentid'];
                    $subjects->identitynumber = $params['identitynumber'];
                    $subjects->firstname = array_key_exists("firstname", $params) ? $params['firstname'] : null;
                    $subjects->birthday = array_key_exists("birthday", $params) ? date('Y-m-d H:i:s', strtotime($params['birthday'])) : null;
                    $subjects->address = array_key_exists("address", $params) ? $params['address'] : null;
                    $subjects->phone = array_key_exists("phone", $params) ? $params['phone'] : null;
                    $subjects->update();

                    $businesssubjects = $this->businesssubjects->findOrFail($params["id"]);
                    $businesssubjects->subjectid = $subjects->id;
                    $businesssubjects->identitydocumentid = $params['identitydocumentid'];
                    $businesssubjects->identitynumber = $params['identitynumber'];
                    $businesssubjects->businessname = $params['name'] . " " . array_key_exists("firstname", $params) ? $params['firstname'] : null;
                    $businesssubjects->subjectrolcategoryid = 2;
                    $businesssubjects->email = $params['email'];
                    $businesssubjects->startdate = new DateTime();
                    $businesssubjects->address = array_key_exists("address", $params) ? $params['address'] : null;
                    $businesssubjects->phone = array_key_exists("phone", $params) ? $params['phone'] : null;
                    $businesssubjects->cyty = array_key_exists("cyty", $params) ? $params['cyty'] : null;
                    $businesssubjects->update();

                    $employeed=$this->employeed->findOrFail($params["employedid"]);
                    $employeed->positionid=array_key_exists("positionid", $params) ? $params['positionid'] : 3;
                    $employeed->update();
                }
            });

        }catch(Exception $e){
            throw new Exception("R_ERROR:".$e->getMessage());
        }
        return 1;
    }
    public function saveUser($params){
        try{
            DB::transaction(function() use($params){

                if(!array_key_exists("userid",$params) ) {
                    $user=new User;
                    $user->name=array_key_exists("name", $params) ? $params['name'] : null;
                    $user->email=array_key_exists("email", $params) ? $params['email'] : null;
                    $user->password=bcrypt($params['password']);
                    $user->employedid=array_key_exists("employedid", $params) ? $params['employedid'] : null;
                    $user->positionid=array_key_exists("positionid", $params) ? $params['positionid'] : null;
                    $user->save();
                }else{
                    $user = $this->user->findOrFail($params["userid"]);
                    $user->name=array_key_exists("name", $params) ? $params['name'] : null;
                    $user->email=array_key_exists("email", $params) ? $params['email'] : null;
                    $user->password=bcrypt($params['password']);
                    $user->employedid=array_key_exists("employedid", $params) ? $params['employedid'] : null;
                    $user->positionid=array_key_exists("positionid", $params) ? $params['positionid'] : null;
                    $user->update();
                }
            });

        }catch(Exception $e){
            DB::rollback();
            throw new Exception("R_ERROR:".$e->getMessage());
        }
        return 1;
    }
    public function updateUser($params){
        try{
            
            DB::transaction(function() use($params){

                DB::update('update users set email = ? , password = ?, updated_at = ? where employedid = ?', ["" , "",new DateTime() , $params["employedid"] ]);
            });

        }catch(Exception $e){
            DB::rollback();
            throw new Exception("R_ERROR:".$e->getMessage());
        }
        return 1;
    }
    public function searchdniruc($params){
        //API URL
//        $url = 'http://api.dayangels.com/api/reniec/';
        $url="https://api.reniec.cloud/dni/".$params->dni;
        //iniciamos curl
        $ch = curl_init($url);
        $data = array(
            'user' => $params->user,
            'pass' => 'demo123.',
            'dni' => $params->dni
        );
        //enviamos el json
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);

        //especificamos el tipo de contenido a enviar
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));

        //devolver respuesta en lugar de generar
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        //ejecuta la peticion post
        $result = curl_exec($ch);
        $resultado = json_decode($result);
        //close cURL resource
        curl_close($ch);
        return $resultado;
    }
}
