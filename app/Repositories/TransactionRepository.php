<?php

namespace App\Repositories;

use Illuminate\Database\Eloquent\Model;
use App\Model\Subtransactions;
use App\Model\Transactions;
use App\Model\Subtransactionstates;
use App\Model\Subtransactiondetails;
use App\Model\Transactionstates;
use App\Model\Vouchers;
use App\Model\Correlatives;
use App\Model\Paymentschedules;
use App\Model\Cashboxs;

use Illuminate\Support\Facades\Auth;
use DateTime;
use DB;
class TransactionRepository extends BaseRepository
{
    
    public function __construct(){
        $this->subtransaction=new Subtransactions;
        $this->transaction=new Transactions;
        $this->subtransactionstate=new Subtransactionstates;
        $this->subtransactiondetail=new Subtransactiondetails;
        $this->transactionstate=new Transactionstates;
        $this->voucher=new Vouchers;
        $this->correlatives=new Correlatives;
        $this->paymentschedule=new Paymentschedules;
        $this->cashbox=new Cashboxs;
    }
    
    public function listSale($params){
        $sql="SELECT s.secundaryemployeedid,bs.businessname as secundaryemployed ,t.externalsubjectid as clientid,t.id as transactionid,s.id as subtransactionid,DATE_FORMAT(STR_TO_DATE(s.time,'%Y-%m-%d'), '%d/%m/%Y') as date,s.employedid,s.totalmoney as total,s.igv,
        v.code,v.number,v.id as voucherid,st.id as subtransactionstateid,sts.name as status,CONCAT(sb.name,' ',sb.firstname,' ', sb.secondname) as client,b.address,
        b.identitynumber,s.paymentfees,s.interestrate,s.frecuencyday,sb.phone,s.season,DATE_FORMAT(STR_TO_DATE(s.datedelivered,'%Y-%m-%d'), '%d/%m/%Y') as datedelivered,st.stateid
        FROM  transactions t 
        inner join subtransactions s on s.transactionid =t.id 
        inner join subtransactionstates st on st.subtransactionid =s.id 
        inner join vouchers  v on v.id=s.voucherid 
        inner join states  sts on sts.id=st.stateid 
        inner join businesssubjects b on b.id=t.externalsubjectid 
        inner join subjects sb on sb.id=b.subjectid
        left join employeds e on e.id=s.secundaryemployeedid 
        left join businesssubjects bs on bs.id=e.businesssubjectid 
        where  st.stateid !=4 and s.comment='".$params['comment']."'";
        if (array_key_exists('filtro',$params)){
            if(!empty($params['filtro'])){
                $sql.= " AND (b.businessname LIKE '%$params[filtro]%' OR v.code LIKE '%$params[filtro]%' OR b.identitynumber LIKE '%$params[filtro]%' ) ";            
            }
        }    
        if (array_key_exists('transactiontypeid',$params)){
            if(!empty($params['transactiontypeid'])){
                $sql.= " AND s.transactiontypeid = '".$params['transactiontypeid']."'  ";            
            }
        }else{
            $sql.= " AND s.transactiontypeid = 1 "; 
        }    
        if (array_key_exists('dateinit',$params)){
            if(!empty($params['dateinit'])){
                $dateinit=explode("/",$params['dateinit']);
                $dateend=explode("/",$params['dateend']);
                $sql.= "and STR_TO_DATE(s.time ,'%Y-%m-%d') between '$dateinit[2]-$dateinit[1]-$dateinit[0]' and '$dateend[2]-$dateend[1]-$dateend[0]'  ";
            }
        }
        $sql.=" order by s.id desc ";
        if(array_key_exists('count',$params)){
            if(!$params['count'] && array_key_exists('limit',$params)){
                $sql.=" LIMIT $params[limit], $params[xpagina] ";
            }
        }        
        $query=DB::select($sql);
        return $query;   
    }
public function listSaleTicket($params){
        $sql="SELECT s.secundaryemployeedid,bs.businessname as secundaryemployed ,t.externalsubjectid as clientid,t.id as transactionid,s.id as subtransactionid,DATE_FORMAT(STR_TO_DATE(s.time,'%Y-%m-%d'), '%d/%m/%Y') as date,s.employedid,s.totalmoney as total,s.igv,
        v.code,v.number,v.id as voucherid,st.id as subtransactionstateid,sts.name as status,CONCAT(sb.name,' ',sb.firstname,' ', sb.secondname) as client,b.address,
        b.identitynumber,s.paymentfees,s.interestrate,s.frecuencyday,sb.phone,s.season,DATE_FORMAT(STR_TO_DATE(s.datedelivered,'%Y-%m-%d'), '%d/%m/%Y') as datedelivered,st.stateid
        FROM  transactions t 
        inner join subtransactions s on s.transactionid =t.id 
        inner join subtransactionstates st on st.subtransactionid =s.id 
        inner join vouchers  v on v.id=s.voucherid 
        inner join states  sts on sts.id=st.stateid 
        inner join businesssubjects b on b.id=t.externalsubjectid 
        inner join subjects sb on sb.id=b.subjectid
        left join employeds e on e.id=s.secundaryemployeedid 
        left join businesssubjects bs on bs.id=e.businesssubjectid 
        where  st.stateid !=4  AND s.transactiontypeid = 11 ";
        if (array_key_exists('filtro',$params)){
            if(!empty($params['filtro'])){
                $sql.= " AND (b.businessname LIKE '%$params[filtro]%' OR v.code LIKE '%$params[filtro]%' OR b.identitynumber LIKE '%$params[filtro]%' ) ";            
            }
        }    
          
        if (array_key_exists('dateinit',$params)){
            if(!empty($params['dateinit'])){
                $dateinit=explode("/",$params['dateinit']);
                $dateend=explode("/",$params['dateend']);
                $sql.= "and STR_TO_DATE(s.time ,'%Y-%m-%d') between '$dateinit[2]-$dateinit[1]-$dateinit[0]' and '$dateend[2]-$dateend[1]-$dateend[0]'  ";
            }
        }
        $sql.=" order by s.id desc ";
        if(array_key_exists('count',$params)){
            if(!$params['count'] && array_key_exists('limit',$params)){
                $sql.=" LIMIT $params[limit], $params[xpagina] ";
            }
        }        
        $query=DB::select($sql);
        return $query;   
    }
    
    public function listSubtransactiondetails($params){
        $query=DB::select('SELECT s.id,s.subtransactionid ,s.valuedobjectid ,s.objectid ,s.amount ,s.unitprice as unitpricesale ,s.totalmoney ,
        s.unitpricereal,o.name as product
        FROM subtransactiondetails s inner join objects o on o.id=s.objectid 
        WHERE s.subtransactionid =:subtransactionid',['subtransactionid'=>$params['subtransactionid']]);
        return $query;    
    }
    public function correlativesnumber($params){       
        $correlatives = $this->correlatives->findOrFail(1);
        $num=((int)$correlatives->numbernote)+1;
        $correlatives->numbernote =sprintf("%'.08d", $num);
        $numberdoc =sprintf('%1$s-%2$08d',$correlatives->serienote, $num);
        $correlatives->update();  
        return $numberdoc;
        
    }
    public function saveSale($params){
        $response=['sdf'=>4];
        try{     
            DB::beginTransaction();
            $userloginid = Auth::id();
 
            if (array_key_exists("subtransactionid",$params)) {
                $transaction = $this->transaction->findOrFail($params['transactionid']);
                $transaction->externalsubjectid=array_key_exists("clientid",$params)?$params['clientid']:null;
                $transaction->updateat=array_key_exists("date",$params)?date('Y-m-d H:i:s', strtotime($params['date'])):null;
                $transaction->update();

                $subtransaction = $this->subtransaction->findOrFail($params['subtransactionid']);
                $subtransaction->employedid =$userloginid;
                $subtransaction->secundaryemployeedid =array_key_exists("secundaryemployeedid",$params)?$params['secundaryemployeedid']:$userloginid;
                $subtransaction->totalmoney =array_key_exists("total",$params)?$params['total']:null ;
                $subtransaction->updateat =array_key_exists("date",$params)?date('Y-m-d H:i:s', strtotime($params['date'])):null ;
                $subtransaction->igv = array_key_exists("igv",$params)?$params['igv']:null ;
                $subtransaction->season = array_key_exists("season",$params)?$params['season']:null ;
                $subtransaction->applytax = 0;                
                $subtransaction->update();
  
                $details=$params["details"];
                if(count($details)>0){
                    foreach($details as $item){
                        $subtransactiondetail=$this->subtransactiondetail->findOrFail($item->id);
                        $subtransactiondetail->amount=$item->amount;
                        $subtransactiondetail->unitprice=$item->unitpricesale;
                        $subtransactiondetail->totalmoney=$item->totalmoney	;
                        $subtransactiondetail->secondaryamount=$item->totalmoney;
                        $subtransactiondetail->unitpricereal=$item->unitpricereal;
                        $subtransactiondetail->update();                        
                    }
                }

            }else {
                $correlatives= new Correlatives;
                if(array_key_exists("transactiontypeid",$params)){
                    $correlatives = $this->correlatives->findOrFail(1);
                    $num=((int)$correlatives->numbernote)+1;
                    $correlatives->numbernote =sprintf("%'.08d", $num);
                    $numberdoc =sprintf('%1$s-%2$08d',$correlatives->serienote, $num);
                    $correlatives->update();
                }else{
                    $correlatives = $this->correlatives->findOrFail(1);
                    $num=((int)$correlatives->numberticket)+1;
                    $correlatives->numberticket =sprintf("%'.08d", $num);
                    $numberdoc =sprintf('%1$s-%2$08d',$correlatives->serieticket, $num);
                    $correlatives->update(); 
                }
                  

                $voucher= new Vouchers;
                $voucher->vouchertypeid=array_key_exists("transactiontypeid",$params)?$params['transactiontypeid']:1;
                $voucher->serialid=1;
                $voucher->number=array_key_exists("transactiontypeid",$params)?$correlatives->numbernote:$correlatives->numberticket;            
                $voucher->serialnumber=array_key_exists("transactiontypeid",$params)?$correlatives->serienote:$correlatives->serieticket;
                $voucher->code=$numberdoc;
                $voucher->save();

                $transaction=new Transactions;
                $transaction->time=array_key_exists("date",$params)?date('Y-m-d H:i:s', strtotime($params['date'])):null;;
                $transaction->voucherid=$voucher->id;
                $transaction->externalsubjectid=array_key_exists("clientid",$params)?$params['clientid']:null;
                $transaction->transactiontypeid=array_key_exists("transactiontypeid",$params)?$params['transactiontypeid']:1;
                $transaction->updateat=array_key_exists("date",$params)?date('Y-m-d H:i:s', strtotime($params['date'])):null;
                $transaction->comment=array_key_exists("comment",$params)?$params['comment']:null;
                $transaction->parentid=0;
                $transaction->save();

                $transactionstate=new Transactionstates;
                $transactionstate->transactionid=$transaction->id;
                $transactionstate->stateid=1;
                $transactionstate->atdate=array_key_exists("date",$params)?date('Y-m-d H:i:s', strtotime($params['date'])):null;
                $transactionstate->comment="Generado";
                $transactionstate->save();

                $subtransaction=new Subtransactions;
                $subtransaction->time = array_key_exists("date",$params)?date('Y-m-d H:i:s', strtotime($params['date'])):null;
                $subtransaction->employedid =$userloginid;
                $subtransaction->secundaryemployeedid =array_key_exists("secundaryemployeedid",$params)?$params['secundaryemployeedid']:$userloginid;
                $subtransaction->totalmoney =array_key_exists("total",$params)?$params['total']:null ;
                $subtransaction->voucherid = $voucher->id;
                $subtransaction->transactionid = $transaction->id;
                $subtransaction->transactiontypeid = array_key_exists("transactiontypeid",$params)?$params['transactiontypeid']:1;
                $subtransaction->updateat =array_key_exists("date",$params)?date('Y-m-d H:i:s', strtotime($params['date'])):null ;
                $subtransaction->comment = array_key_exists("comment",$params)?$params['comment']:null ;
                $subtransaction->igv = array_key_exists("igv",$params)?$params['igv']:null ;
                $subtransaction->season = array_key_exists("season",$params)?$params['season']:null ;
                $subtransaction->applytax = 0;                
                $subtransaction->save();

                $subtransactionstate=new Subtransactionstates;
                $subtransactionstate->subtransactionid = $subtransaction->id;
                $subtransactionstate->stateid =1 ;
                $subtransactionstate->atdate = array_key_exists("date",$params)?date('Y-m-d H:i:s', strtotime($params['date'])):null ;;
                $subtransactionstate->comment = "pendiente state";
                $subtransactionstate->save();
  
                $details=$params["details"];
                if(count($details)>0){
                    foreach($details as $item){
                        $subtransactiondetail=new Subtransactiondetails;
                        $subtransactiondetail->subtransactionid=$subtransaction->id;
                        $subtransactiondetail->valuedobjectid=$item->valuedobjectid;
                        $subtransactiondetail->objectid=$item->objectid;   
                        $subtransactiondetail->amount=$item->amount;
                        $subtransactiondetail->unitprice=$item->unitpricesale;
                        $subtransactiondetail->totalmoney=$item->totalmoney	;
                        $subtransactiondetail->secondaryamount=$item->totalmoney;
                        $subtransactiondetail->unitpricereal=$item->unitpricereal;
                        $subtransactiondetail->save();                        
                    }
                }

            }  
            DB::commit();   
    //    });
            $response=[
                'transactionid'=>$transaction->id,
                'subtransactionid'=>$subtransaction->id,
                'subtransactionstateid'=>$subtransactionstate->id ,
                'transactionstateid'=>$transactionstate->id ,
                'code'=>$voucher->code
            ];

            return $response;
        }catch(Exception $e){
            DB::rollback();
           throw new Exception("R_ERROR:".$e->getMessage());
       }catch (Throwable $e) {
            DB::rollback();
            throw new Exception("R_ERROR:".$e->getMessage());
       }

   }
   public function delete($params){
        try{     
            DB::beginTransaction();
            $userloginid = Auth::id();
 
            if (array_key_exists("subtransactionstateid",$params)) {
                
                $subtransactionstate = $this->subtransactionstate->findOrFail($params['subtransactionstateid']);                
                $subtransactionstate->stateid =4;
                $subtransactionstate->atdate =new DateTime();
                $subtransactionstate->update();
                DB::commit(); 
            }   
            
        }catch(Exception $e){
            DB::rollback();
           throw new Exception("R_ERROR:".$e->getMessage());
       }catch (Throwable $e) {
            DB::rollback();
            throw new Exception("R_ERROR:".$e->getMessage());
       }
           return 1;    
    }

    public function savePaymentSchedule($params){                               
        try{     
            DB::beginTransaction();
            $userloginid = Auth::id();
 
            if (array_key_exists("subtransactionid",$params)) {
                
                $subtransaction = $this->subtransaction->findOrFail($params['subtransactionid']);
                $subtransaction->employedid =$userloginid;
                $subtransaction->interestrate =array_key_exists("tasa",$params)?$params['tasa']:null ;
                $subtransaction->paymentfees =array_key_exists("fees",$params)?$params['fees']:null ;
                $subtransaction->frecuencyday = array_key_exists("frecuencyday",$params)?$params['frecuencyday']:null ;
                $subtransaction->update();
                $subtransactionstate=$this->subtransactionstate->findOrFail($params['subtransactionstateid']);
                $subtransactionstate->stateid =5;
                $subtransactionstate->update();
                Paymentschedules::where('subtransactionid', '=', $params['subtransactionid'])->update(['deleted_at' => new DateTime()]);
                $details=$params["list"];
                if(count($details)>0){
                    $i = 0;
                    foreach($details as $item){
                        $payments=new Paymentschedules;
                        $payments->time=date('Y-m-d H:i:s', strtotime($item->time));
                        $payments->businesssubjectid=$params['businesssubjectid'];
                        $payments->subtransactionid=$subtransaction->id	;
                        $payments->capital=$item->capital;
                        $payments->interest = $item->interest;
                        $payments->feeamount = $item->feeamount; 
                        $payments->day=$item->day;
                        $payments->status=1;
                        $payments->deleted_at=null;
                        $payments->numberdues=$i+1;
                        $payments->save();
                        $i++;        
                    }
                }
                DB::commit(); 
            }   
            
        }catch(Exception $e){
            DB::rollback();
           throw new Exception("R_ERROR:".$e->getMessage());
       }catch (Throwable $e) {
            DB::rollback();
            throw new Exception("R_ERROR:".$e->getMessage());
       }
           return 1;          
   }
   public function saveCashbox($params){                               
        try{     
            DB::beginTransaction();
            $userloginid = Auth::id();

                if (array_key_exists("id",$params)) {
                    
                    $cashbox = $this->cashbox->findOrFail($params['id']);
                    $cashbox->employedid =$userloginid;
                    $cashbox->initamount =array_key_exists("initamount",$params)?$params['initamount']:null ;
                    $cashbox->stateid = 8;
                    $cashbox->finalamount = array_key_exists("finalamount",$params)?$params['finalamount']:null ;
                    $cashbox->totalpaymentamount = array_key_exists("totalpaymentamount",$params)?$params['totalpaymentamount']:null ;
                    $cashbox->totalfeeamount = array_key_exists("totalfeeamount",$params)?$params['totalfeeamount']:null ;
                    $cashbox->totalexpenseamount = array_key_exists("finalamount",$params)?$params['totalexpenseamount']:null ;
                    $cashbox->closingdate = array_key_exists("closingdate",$params)?date('Y-m-d H:i:s', strtotime($params['closingdate'])):null;
                    $cashbox->ticketing = array_key_exists("ticketing",$params)?$params['ticketing']:null ;
                    $cashbox->update();
                    
                    DB::commit(); 
                }else {
                
                    $cashbox= new Cashboxs;
                    $cashbox->employedid =$userloginid;
                    $cashbox->openingdate =array_key_exists("openingdate",$params)?date('Y-m-d H:i:s', strtotime($params['openingdate'])):null;
                    $cashbox->initamount =array_key_exists("initamount",$params)?$params['initamount']:null ;
                    $cashbox->stateid = 7;
                    $cashbox->finalamount = null ;
                    $cashbox->closingdate = null;
                    $cashbox->save();
                    DB::commit(); 
                }
                
            }catch(Exception $e){
                DB::rollback();
            throw new Exception("R_ERROR:".$e->getMessage());
        }catch (Throwable $e) {
                DB::rollback();
                throw new Exception("R_ERROR:".$e->getMessage());
        }
            return 1;          
    }
    public function listCashbox($params){
        $sql="SELECT c.ticketing,c.id,c.totalpaymentamount,c.totalfeeamount,c.totalexpenseamount,s.name as statename,c.employedid,DATE_FORMAT(STR_TO_DATE(c.openingdate,'%Y-%m-%d'), '%d/%m/%Y') as openingdate,c.initamount,c.stateid,c.finalamount,DATE_FORMAT(STR_TO_DATE(c.closingdate,'%Y-%m-%d'), '%d/%m/%Y') as closingdate
        FROM  cashboxs c
        inner join states s on c.stateid=s.id 
        where  s.id in (7,8)";
        if (array_key_exists('filtro',$params)){
            if(!empty($params['filtro'])){
                $sql.= " AND (c.closingdate LIKE '%$params[filtro]%' OR c.initamount LIKE '%$params[filtro]%' OR s.name LIKE '%$params[filtro]%' ) ";            
            }
        }    
        if (array_key_exists('dateinit',$params)){
            if(!empty($params['dateinit'])){
                $dateinit=explode("/",$params['dateinit']);
                $dateend=explode("/",$params['dateend']);
                $sql.= "and STR_TO_DATE(c.openingdate ,'%Y-%m-%d') between '$dateinit[2]-$dateinit[1]-$dateinit[0]' and '$dateend[2]-$dateend[1]-$dateend[0]'  ";
            }
        }
        $sql.=" order by c.id desc ";
        if(array_key_exists('count',$params)){
            if(!$params['count'] && array_key_exists('limit',$params)){
                $sql.=" LIMIT $params[limit], $params[xpagina] ";
            }
        }    
        // var_dump($sql);    
        $query=DB::select($sql);
        return $query;   
    }
    public function saveExpenseAndPayment($params){
        $response=['sdf'=>4];
        try{     
            DB::beginTransaction();
            $userloginid = Auth::id();
 
            if (array_key_exists("subtransactionid",$params)) {
                $transaction = $this->transaction->findOrFail($params['transactionid']);
                $transaction->externalsubjectid=1;
                $transaction->comment=array_key_exists("commenttransaction",$params)?$params['commenttransaction']:null;
                $transaction->updateat=array_key_exists("date",$params)?date('Y-m-d H:i:s', strtotime($params['date'])):null;
                $transaction->update();

                $subtransaction = $this->subtransaction->findOrFail($params['subtransactionid']);
                $subtransaction->employedid =$userloginid;
                $subtransaction->totalmoney =array_key_exists("total",$params)?$params['total']:null ;
                $subtransaction->updateat =array_key_exists("date",$params)?date('Y-m-d H:i:s', strtotime($params['date'])):null ;
                $subtransaction->igv = array_key_exists("igv",$params)?$params['igv']:null ;
                $subtransaction->applytax = 0;                
                $subtransaction->update();

            }else {
                $correlatives= new Correlatives;
                if(array_key_exists("transactiontypeid",$params)){
                    $correlatives = $this->correlatives->findOrFail(1);
                    $num=((int)$correlatives->numberpayment)+1;
                    $correlatives->numberpayment =sprintf("%'.08d", $num);
                    $numberdoc =sprintf('%1$s-%2$08d',$correlatives->seriepayment, $num);
                    $correlatives->update();
                }else{
                    $correlatives = $this->correlatives->findOrFail(1);
                    $num=((int)$correlatives->numberexpense)+1;
                    $correlatives->numberexpense =sprintf("%'.08d", $num);
                    $numberdoc =sprintf('%1$s-%2$08d',$correlatives->serieexpense, $num);
                    $correlatives->update(); 
                }
                  
                $voucher= new Vouchers;
                $voucher->vouchertypeid=array_key_exists("transactiontypeid",$params)?6:5;
                $voucher->serialid=1;
                $voucher->number=array_key_exists("transactiontypeid",$params)?$correlatives->numberpayment:$correlatives->numberexpense;            
                $voucher->serialnumber=array_key_exists("transactiontypeid",$params)?$correlatives->seriepayment:$correlatives->serieexpense;
                $voucher->code=$numberdoc;
                $voucher->save();

                $transaction=new Transactions;
                $transaction->time=array_key_exists("date",$params)?date('Y-m-d H:i:s', strtotime($params['date'])):null;;
                $transaction->voucherid=$voucher->id;
                $transaction->externalsubjectid=1;
                $transaction->transactiontypeid=array_key_exists("transactiontypeid",$params)?6:5;
                $transaction->updateat=array_key_exists("date",$params)?date('Y-m-d H:i:s', strtotime($params['date'])):null;
                $transaction->comment=array_key_exists("commenttransaction",$params)?$params['commenttransaction']:null;
                
                $transaction->parentid=0;
                $transaction->save();

                $transactionstate=new Transactionstates;
                $transactionstate->transactionid=$transaction->id;
                $transactionstate->stateid=1;
                $transactionstate->atdate=array_key_exists("date",$params)?date('Y-m-d H:i:s', strtotime($params['date'])):null;
                $transactionstate->comment="Abono o Expense";
                $transactionstate->save();

                $subtransaction=new Subtransactions;
                $subtransaction->time = array_key_exists("date",$params)?date('Y-m-d H:i:s', strtotime($params['date'])):null;
                $subtransaction->employedid =$userloginid;
                $subtransaction->totalmoney =array_key_exists("total",$params)?$params['total']:null ;
                $subtransaction->voucherid = $voucher->id;
                $subtransaction->transactionid = $transaction->id;
                $subtransaction->transactiontypeid = array_key_exists("transactiontypeid",$params)?6:5;
                $subtransaction->updateat =array_key_exists("date",$params)?date('Y-m-d H:i:s', strtotime($params['date'])):null ;
                $subtransaction->comment = array_key_exists("comment",$params)?$params['comment']:null ;
                $subtransaction->igv = array_key_exists("igv",$params)?$params['igv']:null ;
                $subtransaction->applytax = 0;                
                $subtransaction->save();

                $subtransactionstate=new Subtransactionstates;
                $subtransactionstate->subtransactionid = $subtransaction->id;
                $subtransactionstate->stateid =1 ;
                $subtransactionstate->atdate = array_key_exists("date",$params)?date('Y-m-d H:i:s', strtotime($params['date'])):null ;;
                $subtransactionstate->comment = "pendiente state";
                $subtransactionstate->save();  
            }  
            DB::commit();   
    //    });
            $response=[
                'transactionid'=>$transaction->id,
                'subtransactionid'=>$subtransaction->id,
                'subtransactionstateid'=>$subtransactionstate->id ,
                'transactionstateid'=>$transactionstate->id ,
                'code'=>$voucher->code
            ];

            return $response;
        }catch(Exception $e){
            DB::rollback();
           throw new Exception("R_ERROR:".$e->getMessage());
       }catch (Throwable $e) {
            DB::rollback();
            throw new Exception("R_ERROR:".$e->getMessage());
       }

   }
   public function saveDepositAndQualification($params){
    $response=['sdf'=>4];
    try{     
        DB::beginTransaction();
        $userloginid = Auth::id();

        if (array_key_exists("subtransactionid",$params)) {
            $transaction = $this->transaction->findOrFail($params['transactionid']);
            $transaction->externalsubjectid=1;
            $transaction->comment=array_key_exists("commenttransaction",$params)?$params['commenttransaction']:null;
            $transaction->updateat=array_key_exists("date",$params)?date('Y-m-d H:i:s', strtotime($params['date'])):null;
            $transaction->update();

            $subtransaction = $this->subtransaction->findOrFail($params['subtransactionid']);
            $subtransaction->employedid =$userloginid;
            $subtransaction->totalmoney =array_key_exists("total",$params)?$params['total']:null ;
            $subtransaction->updateat =array_key_exists("date",$params)?date('Y-m-d H:i:s', strtotime($params['date'])):null ;
            $subtransaction->igv = array_key_exists("igv",$params)?$params['igv']:null ;
            $subtransaction->applytax = 0;                
            $subtransaction->update();

        }else {
            $correlatives= new Correlatives;
            if(array_key_exists("transactiontypeid",$params)){
                $correlatives = $this->correlatives->findOrFail(1);
                $num=((int)$correlatives->numberqualification)+1;
                $correlatives->numberqualification =sprintf("%'.08d", $num);
                $numberdoc =sprintf('%1$s-%2$08d',$correlatives->seriequalification, $num);
                $correlatives->update();
            }else{
                $correlatives = $this->correlatives->findOrFail(1);
                $num=((int)$correlatives->numberdeposit)+1;
                $correlatives->numberdeposit =sprintf("%'.08d", $num);
                $numberdoc =sprintf('%1$s-%2$08d',$correlatives->seriedeposit, $num);
                $correlatives->update(); 
            }
              
            $voucher= new Vouchers;
            $voucher->vouchertypeid=array_key_exists("transactiontypeid",$params)?8:9;
            $voucher->serialid=1;
            $voucher->number=array_key_exists("transactiontypeid",$params)?$correlatives->numberpayment:$correlatives->numberexpense;            
            $voucher->serialnumber=array_key_exists("transactiontypeid",$params)?$correlatives->seriepayment:$correlatives->serieexpense;
            $voucher->code=$numberdoc;
            $voucher->save();

            $transaction=new Transactions;
            $transaction->time=array_key_exists("date",$params)?date('Y-m-d H:i:s', strtotime($params['date'])):null;;
            $transaction->voucherid=$voucher->id;
            $transaction->externalsubjectid=1;
            $transaction->transactiontypeid=array_key_exists("transactiontypeid",$params)?8:9;
            $transaction->updateat=array_key_exists("date",$params)?date('Y-m-d H:i:s', strtotime($params['date'])):null;
            $transaction->comment=array_key_exists("commenttransaction",$params)?$params['commenttransaction']:null;
            
            $transaction->parentid=0;
            $transaction->save();

            $transactionstate=new Transactionstates;
            $transactionstate->transactionid=$transaction->id;
            $transactionstate->stateid=1;
            $transactionstate->atdate=array_key_exists("date",$params)?date('Y-m-d H:i:s', strtotime($params['date'])):null;
            $transactionstate->comment="Habilitacio o Deposito";
            $transactionstate->save();

            $subtransaction=new Subtransactions;
            $subtransaction->time = array_key_exists("date",$params)?date('Y-m-d H:i:s', strtotime($params['date'])):null;
            $subtransaction->employedid =$userloginid;
            $subtransaction->totalmoney =array_key_exists("total",$params)?$params['total']:null ;
            $subtransaction->voucherid = $voucher->id;
            $subtransaction->transactionid = $transaction->id;
            $subtransaction->transactiontypeid = array_key_exists("transactiontypeid",$params)?8:9;
            $subtransaction->updateat =array_key_exists("date",$params)?date('Y-m-d H:i:s', strtotime($params['date'])):null ;
            $subtransaction->comment = array_key_exists("comment",$params)?$params['comment']:null ;
            $subtransaction->igv = array_key_exists("igv",$params)?$params['igv']:null ;
            $subtransaction->applytax = 0;                
            $subtransaction->save();

            $subtransactionstate=new Subtransactionstates;
            $subtransactionstate->subtransactionid = $subtransaction->id;
            $subtransactionstate->stateid =1 ;
            $subtransactionstate->atdate = array_key_exists("date",$params)?date('Y-m-d H:i:s', strtotime($params['date'])):null ;;
            $subtransactionstate->comment = "pendiente state";
            $subtransactionstate->save();  
        }  
        DB::commit();   
//    });
        $response=[
            'transactionid'=>$transaction->id,
            'subtransactionid'=>$subtransaction->id,
            'subtransactionstateid'=>$subtransactionstate->id ,
            'transactionstateid'=>$transactionstate->id ,
            'code'=>$voucher->code
        ];

        return $response;
    }catch(Exception $e){
        DB::rollback();
       throw new Exception("R_ERROR:".$e->getMessage());
   }catch (Throwable $e) {
        DB::rollback();
        throw new Exception("R_ERROR:".$e->getMessage());
   }

}
   public function listExpenseAndPayment($params){
    
    $sql="SELECT t.comment as commenttransaction,t.externalsubjectid as clientid,t.id as transactionid,s.id as subtransactionid,DATE_FORMAT(STR_TO_DATE(s.time,'%Y-%m-%d'), '%d/%m/%Y') as date,s.employedid,s.totalmoney as total,s.igv,
    v.code,v.number,v.id as voucherid,st.id as subtransactionstateid,s.comment
    FROM  transactions t 
    inner join subtransactions s on s.transactionid =t.id 
    inner join subtransactionstates st on st.subtransactionid =s.id 
    inner join vouchers  v on v.id=s.voucherid 
    inner join states  sts on sts.id=st.stateid 
    where  st.stateid !=4 ";
    if (array_key_exists('filtro',$params)){
        if(!empty($params['filtro'])){
            $sql.= " AND (s.comment LIKE '%$params[filtro]%' OR v.code LIKE '%$params[filtro]%'  or  t.comment  LIKE '%$params[filtro]%' )";            
        }
    }    
    if (array_key_exists('transactiontypeid',$params)){
        if(!empty($params['transactiontypeid'])){
            $sql.= " AND s.transactiontypeid = '".$params['transactiontypeid']."'  ";            
        }
    }else{
        $sql.= " AND s.transactiontypeid = 5 "; 
    }
    if (array_key_exists('dateinit',$params)){
        if(!empty($params['dateinit'])){
            $dateinit=explode("/",$params['dateinit']);
            $dateend=explode("/",$params['dateend']);
            $sql.= "and STR_TO_DATE(s.time ,'%Y-%m-%d') between '$dateinit[2]-$dateinit[1]-$dateinit[0]' and '$dateend[2]-$dateend[1]-$dateend[0]'  ";
        }
    }
    $sql.=" order by s.id desc ";
    if(array_key_exists('count',$params)){
        if(!$params['count'] && array_key_exists('limit',$params)){
            $sql.=" LIMIT $params[limit], $params[xpagina] ";
        }
    }        
    $query=DB::select($sql);
    return $query;   
}
public function changestatusdisbursement($params){
    try{     
        DB::beginTransaction();
        $userloginid = Auth::id();

        if (array_key_exists("subtransactionid",$params)) {
            
            $subtransaction = $this->subtransaction->findOrFail($params['subtransactionid']);                
            $subtransaction->datedelivered = array_key_exists("datedelivered",$params)?date('Y-m-d H:i:s', strtotime($params['datedelivered'])):null ;;
            $subtransaction->update();

            $subtransactionstate = $this->subtransactionstate->findOrFail($params['subtransactionstateid']);                
            $subtransactionstate->stateid =9;
            $subtransactionstate->update();

            DB::commit(); 
        }   
        
    }catch(Exception $e){
        DB::rollback();
       throw new Exception("R_ERROR:".$e->getMessage());
   }catch (Throwable $e) {
        DB::rollback();
        throw new Exception("R_ERROR:".$e->getMessage());
   }
       return 1;    
}

public function saveSaleTicket($params){
    $response=['sdf'=>4];
    try{     
        DB::beginTransaction();
        $userloginid = Auth::id();

        if (array_key_exists("subtransactionid",$params)) {
            $transaction = $this->transaction->findOrFail($params['transactionid']);
            $transaction->externalsubjectid=array_key_exists("clientid",$params)?$params['clientid']:null;
            $transaction->updateat=new DateTime();
            $transaction->update();

            $subtransaction = $this->subtransaction->findOrFail($params['subtransactionid']);
            $subtransaction->employedid =$userloginid;
            $subtransaction->secundaryemployeedid =array_key_exists("secundaryemployeedid",$params)?$params['secundaryemployeedid']:$userloginid;
            $subtransaction->totalmoney =array_key_exists("total",$params)?$params['total']:null ;
            $subtransaction->updateat =new DateTime();
            $subtransaction->igv = array_key_exists("igv",$params)?$params['igv']:null ;
            $subtransaction->season = array_key_exists("season",$params)?$params['season']:null ;
            $subtransaction->applytax = 0;                
            $subtransaction->update();

            $details=$params["details"];

            if(count($details)>0){
                foreach($details as $item){
                    $subtransactiondetail=$this->subtransactiondetail->findOrFail($item->id);
                    $subtransactiondetail->subtransactionid=$subtransaction->id;
                    $subtransactiondetail->valuedobjectid=$item->valuedobjectid;
                    $subtransactiondetail->objectid=$item->objectid;   
                    $subtransactiondetail->amount=$item->amount;
                    $subtransactiondetail->unitprice=$item->unitpricesale;
                    $subtransactiondetail->totalmoney=$item->totalmoney	;
                    $subtransactiondetail->secondaryamount=$item->totalmoney;
                    $subtransactiondetail->unitpricereal=$item->unitpricereal;
                    $subtransactiondetail->update();                        
                }
            }

        }else {
            $correlatives= new Correlatives;
            $correlatives = $this->correlatives->findOrFail(1);
            $num=((int)$correlatives->numbersale)+1;
            $correlatives->numbersale =sprintf("%'.08d", $num);
            $numberdoc =sprintf('%1$s-%2$08d',$correlatives->seriesale, $num);
            $correlatives->update();            

            $voucher= new Vouchers;
            $voucher->vouchertypeid=11;
            $voucher->serialid=1;
            $voucher->number=$correlatives->numbersale;            
            $voucher->serialnumber=$correlatives->seriesale;
            $voucher->code=$numberdoc;
            $voucher->save();

            $transaction=new Transactions;
            $transaction->time=array_key_exists("date",$params)?date('Y-m-d H:i:s', strtotime($params['date'])):new DateTime();
            $transaction->voucherid=$voucher->id;
            $transaction->externalsubjectid=array_key_exists("clientid",$params)?$params['clientid']:null;
            $transaction->transactiontypeid=11;
            $transaction->updateat=new DateTime();
            $transaction->comment=array_key_exists("comment",$params)?$params['comment']:null;
            $transaction->parentid=0;
            $transaction->save();

            $transactionstate=new Transactionstates;
            $transactionstate->transactionid=$transaction->id;
            $transactionstate->stateid=1;
            $transactionstate->atdate=array_key_exists("date",$params)?date('Y-m-d H:i:s', strtotime($params['date'])):new DateTime();
            $transactionstate->comment="Generado";
            $transactionstate->save();

            $subtransaction=new Subtransactions;
            $subtransaction->time = array_key_exists("date",$params)?date('Y-m-d H:i:s', strtotime($params['date'])):new DateTime();
            $subtransaction->employedid =$userloginid;
            $subtransaction->secundaryemployeedid =array_key_exists("secundaryemployeedid",$params)?$params['secundaryemployeedid']:$userloginid;
            $subtransaction->totalmoney =array_key_exists("total",$params)?$params['total']:null ;
            $subtransaction->voucherid = $voucher->id;
            $subtransaction->transactionid = $transaction->id;
            $subtransaction->transactiontypeid = 11;
            $subtransaction->updateat =new DateTime() ;
            $subtransaction->comment = array_key_exists("comment",$params)?$params['comment']:null ;
            $subtransaction->igv = array_key_exists("igv",$params)?$params['igv']:null ;
            $subtransaction->season = array_key_exists("season",$params)?$params['season']:null ;
            $subtransaction->applytax = 0;                
            $subtransaction->save();

            $subtransactionstate=new Subtransactionstates;
            $subtransactionstate->subtransactionid = $subtransaction->id;
            $subtransactionstate->stateid =1 ;
            $subtransactionstate->atdate = array_key_exists("date",$params)?date('Y-m-d H:i:s', strtotime($params['date'])):new DateTime();
            $subtransactionstate->comment = "pendiente state";
            $subtransactionstate->save();

            $details=$params["details"];
            if(count($details)>0){
                foreach($details as $item){
                    $subtransactiondetail=new Subtransactiondetails;
                    $subtransactiondetail->subtransactionid=$subtransaction->id;
                    $subtransactiondetail->valuedobjectid=$item->valuedobjectid;
                    $subtransactiondetail->objectid=$item->objectid;   
                    $subtransactiondetail->amount=$item->amount;
                    $subtransactiondetail->unitprice=$item->unitpricesale;
                    $subtransactiondetail->totalmoney=$item->totalmoney	;
                    $subtransactiondetail->secondaryamount=$item->totalmoney;
                    $subtransactiondetail->unitpricereal=$item->unitpricereal;
                    $subtransactiondetail->save();                        
                }
            }

        }  
        DB::commit();   
//    });
        $response=[];
        if (array_key_exists("subtransactionid",$params)) {
            $response=[
                'transactionid'=>$transaction->id,
                'subtransactionid'=>$subtransaction->id,
                'code' => $params['code'],
            ];    
        }else{
            $response=[
                'transactionid'=>$transaction->id,
                'subtransactionid'=>$subtransaction->id,
                'subtransactionstateid'=>$subtransactionstate->id ,
                'transactionstateid'=>$transactionstate->id ,
                'code'=>$voucher->code,
            ];
        }
        

        return $response;
    }catch(Exception $e){
        DB::rollback();
       throw new Exception("R_ERROR:".$e->getMessage());
   }catch (Throwable $e) {
        DB::rollback();
        throw new Exception("R_ERROR:".$e->getMessage());
   }

}
public function listPurchase($params){
    $sql="SELECT s.secundaryemployeedid,bs.businessname as secundaryemployed ,t.externalsubjectid as clientid,t.id as transactionid,s.id as subtransactionid,DATE_FORMAT(STR_TO_DATE(s.time,'%Y-%m-%d'), '%d/%m/%Y') as date,s.employedid,s.totalmoney as total,s.igv,
    v.code,v.number,v.id as voucherid,st.id as subtransactionstateid,sts.name as status,CONCAT(sb.name,' ',sb.firstname,' ', sb.secondname) as client,b.address,
    b.identitynumber,s.paymentfees,s.interestrate,s.frecuencyday,sb.phone,s.season,DATE_FORMAT(STR_TO_DATE(s.datedelivered,'%Y-%m-%d'), '%d/%m/%Y') as datedelivered,st.stateid
    FROM  transactions t 
    inner join subtransactions s on s.transactionid =t.id 
    inner join subtransactionstates st on st.subtransactionid =s.id 
    inner join vouchers  v on v.id=s.voucherid 
    inner join states  sts on sts.id=st.stateid 
    inner join businesssubjects b on b.id=t.externalsubjectid 
    inner join subjects sb on sb.id=b.subjectid
    left join employeds e on e.id=s.secundaryemployeedid 
    left join businesssubjects bs on bs.id=e.businesssubjectid 
    where  st.stateid !=4  AND s.transactiontypeid = 2 ";
    if (array_key_exists('filtro',$params)){
        if(!empty($params['filtro'])){
            $sql.= " AND (b.businessname LIKE '%$params[filtro]%' OR v.code LIKE '%$params[filtro]%' OR b.identitynumber LIKE '%$params[filtro]%' ) ";            
        }
    }    
      
    if (array_key_exists('dateinit',$params)){
        if(!empty($params['dateinit'])){
            $dateinit=explode("/",$params['dateinit']);
            $dateend=explode("/",$params['dateend']);
            $sql.= "and STR_TO_DATE(s.time ,'%Y-%m-%d') between '$dateinit[2]-$dateinit[1]-$dateinit[0]' and '$dateend[2]-$dateend[1]-$dateend[0]'  ";
        }
    }
    $sql.=" order by s.id desc ";
    if(array_key_exists('count',$params)){
        if(!$params['count'] && array_key_exists('limit',$params)){
            $sql.=" LIMIT $params[limit], $params[xpagina] ";
        }
    }        
    $query=DB::select($sql);
    return $query;   
}
public function savePurchase($params){
    $response=['sdf'=>4];
    try{     
        DB::beginTransaction();
        $userloginid = Auth::id();

        if (array_key_exists("subtransactionid",$params)) {
            $transaction = $this->transaction->findOrFail($params['transactionid']);
            $transaction->externalsubjectid=array_key_exists("clientid",$params)?$params['clientid']:null;
            $transaction->updateat=new DateTime();
            $transaction->update();

            $subtransaction = $this->subtransaction->findOrFail($params['subtransactionid']);
            $subtransaction->employedid =$userloginid;
            $subtransaction->secundaryemployeedid =array_key_exists("secundaryemployeedid",$params)?$params['secundaryemployeedid']:$userloginid;
            $subtransaction->totalmoney =array_key_exists("total",$params)?$params['total']:null ;
            $subtransaction->updateat =new DateTime();
            $subtransaction->igv = array_key_exists("igv",$params)?$params['igv']:null ;
            $subtransaction->season = array_key_exists("season",$params)?$params['season']:null ;
            $subtransaction->applytax = 0;                
            $subtransaction->update();

            $details=$params["details"];

            if(count($details)>0){
                foreach($details as $item){
                    $subtransactiondetail=$this->subtransactiondetail->findOrFail($item->id);
                    $subtransactiondetail->subtransactionid=$subtransaction->id;
                    $subtransactiondetail->valuedobjectid=$item->valuedobjectid;
                    $subtransactiondetail->objectid=$item->objectid;   
                    $subtransactiondetail->amount=$item->amount;
                    $subtransactiondetail->unitprice=$item->unitpricesale;
                    $subtransactiondetail->totalmoney=$item->totalmoney	;
                    $subtransactiondetail->secondaryamount=$item->totalmoney;
                    $subtransactiondetail->unitpricereal=$item->unitpricereal;
                    $subtransactiondetail->update();                        
                }
            }

        }else {
            
            $voucher= new Vouchers;
            $voucher->vouchertypeid=12;
            $voucher->serialid=1;
            $voucher->number='' ;            
            $voucher->serialnumber=array_key_exists("serialnumber",$params)?$params['serialnumber']:'';
            $voucher->code=array_key_exists("code",$params)?$params['code']:'';
            $voucher->save();

            $transaction=new Transactions;
            $transaction->time=array_key_exists("date",$params)?date('Y-m-d H:i:s', strtotime($params['date'])):new DateTime();
            $transaction->voucherid=$voucher->id;
            $transaction->externalsubjectid=array_key_exists("clientid",$params)?$params['clientid']:null;
            $transaction->transactiontypeid=2;
            $transaction->updateat=new DateTime();
            $transaction->comment=array_key_exists("comment",$params)?$params['comment']:null;
            $transaction->parentid=0;
            $transaction->save();

            $transactionstate=new Transactionstates;
            $transactionstate->transactionid=$transaction->id;
            $transactionstate->stateid=1;
            $transactionstate->atdate=array_key_exists("date",$params)?date('Y-m-d H:i:s', strtotime($params['date'])):new DateTime();
            $transactionstate->comment="Generado";
            $transactionstate->save();

            $subtransaction=new Subtransactions;
            $subtransaction->time = array_key_exists("date",$params)?date('Y-m-d H:i:s', strtotime($params['date'])):new DateTime();
            $subtransaction->employedid =$userloginid;
            $subtransaction->secundaryemployeedid =array_key_exists("secundaryemployeedid",$params)?$params['secundaryemployeedid']:$userloginid;
            $subtransaction->totalmoney =array_key_exists("total",$params)?$params['total']:null ;
            $subtransaction->voucherid = $voucher->id;
            $subtransaction->transactionid = $transaction->id;
            $subtransaction->transactiontypeid = 2;
            $subtransaction->updateat =new DateTime() ;
            $subtransaction->comment = array_key_exists("comment",$params)?$params['comment']:null ;
            $subtransaction->igv = array_key_exists("igv",$params)?$params['igv']:null ;
            $subtransaction->season = array_key_exists("season",$params)?$params['season']:null ;
            $subtransaction->applytax = 0;                
            $subtransaction->save();

            $subtransactionstate=new Subtransactionstates;
            $subtransactionstate->subtransactionid = $subtransaction->id;
            $subtransactionstate->stateid =1 ;
            $subtransactionstate->atdate = array_key_exists("date",$params)?date('Y-m-d H:i:s', strtotime($params['date'])):new DateTime();
            $subtransactionstate->comment = "pendiente state";
            $subtransactionstate->save();

            $details=$params["details"];
            if(count($details)>0){
                foreach($details as $item){
                    $subtransactiondetail=new Subtransactiondetails;
                    $subtransactiondetail->subtransactionid=$subtransaction->id;
                    $subtransactiondetail->valuedobjectid=$item->valuedobjectid;
                    $subtransactiondetail->objectid=$item->objectid;   
                    $subtransactiondetail->amount=$item->amount;
                    $subtransactiondetail->unitprice=$item->unitpricesale;
                    $subtransactiondetail->totalmoney=$item->totalmoney	;
                    $subtransactiondetail->secondaryamount=$item->totalmoney;
                    $subtransactiondetail->unitpricereal=$item->unitpricereal;
                    $subtransactiondetail->save();                        
                }
            }

        }  
        DB::commit();   
//    });
        $response=[];
        if (array_key_exists("subtransactionid",$params)) {
            $response=[
                'transactionid'=>$transaction->id,
                'subtransactionid'=>$subtransaction->id,
                'code' => $params['code'],
            ];    
        }else{
            $response=[
                'transactionid'=>$transaction->id,
                'subtransactionid'=>$subtransaction->id,
                'subtransactionstateid'=>$subtransactionstate->id ,
                'transactionstateid'=>$transactionstate->id ,
                'code'=>$voucher->code,
            ];
        }
        

        return $response;
    }catch(Exception $e){
        DB::rollback();
       throw new Exception("R_ERROR:".$e->getMessage());
   }catch (Throwable $e) {
        DB::rollback();
        throw new Exception("R_ERROR:".$e->getMessage());
   }

}

}
