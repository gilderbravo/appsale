<?php

namespace App\Repositories;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use App\Model\Fees;
use App\Model\Vouchers;
use App\Model\Correlatives;
use App\Model\Paymentschedules;
use App\Model\Subtransactionstates;
use App\Model\Transactioncloses;
use DateTime;
use DB;
class FeesRepository extends BaseRepository
{
        
    public function __construct(){
        $this->paymentschedule=new Paymentschedules;
        $this->voucher=new Vouchers;
        $this->correlatives=new Correlatives;
        $this->subtransactionstate=new Subtransactionstates;
        $this->transactionclose=new Transactioncloses;
        $this->fee=new Fees;
    }
    public function listpaymentschedule($params){
        $sql="SELECT ".(array_key_exists('id',$params)?"(SELECT  DATE_FORMAT(STR_TO_DATE(f.issedtime ,'%Y-%m-%d'), '%d/%m/%Y')  FROM fees f WHERE f.referencespayment in ( p.id) ) as issedtime,":"")." p.id, DATE_FORMAT(STR_TO_DATE(p.time,'%Y-%m-%d'), '%d/%m/%Y') as time, p.businesssubjectid, p.subtransactionid, ".(array_key_exists('pendingPayment',$params)?"":" advancement,")." p.capital, p.interest, 
            (cast(p.feeamount as decimal(11,2))  - cast(p.advancement as decimal(11,2)) ) as feeamount, p.day, p.status, p.deleted_at, p.numberdues,p.advancement 
            FROM paymentschedules p  WHERE p.deleted_at is null ";
            if (array_key_exists('id',$params)){
                $sql.= " and p.id in ($params[id])  ";
            }else{
                $sql.=( array_key_exists('pendingPayment',$params)?" and p.status=1 ":"  ")." and p.subtransactionid=$params[subtransactionid] ";
            }
              $sql.=" order by id asc ";
        $query=DB::select($sql);
        return $query;   
    }
    public function listFeesClient($params){
        $sql="SELECT  s.id AS subtransactionid, b.id AS businesstransactionid, DATE_FORMAT(STR_TO_DATE(s.time,'%Y-%m-%d'), '%d/%m/%Y') as date, b.externalsubjectid,
        CONCAT(sub.name,' ',sub.firstname,' ', sub.secondname)  AS  clientname, v.id as voucherid, v.number as vouchernumber, vt.id as vouchertypeid, vt.name as vouchertype,
        st.name as state,ss.id as subtransactionstateid,bs.id as businesssubjectid,s.totalmoney, 
        v.code,st.id as stateid,sub.address,sub.identitynumber,s.frecuencyday,s.paymentfees,s.interestrate,
        (SELECT SUM(f2.amount ) FROM fees f2 WHERE f2.subtransactionid = s.id ) as totalpayment,(s.totalmoney+(s.totalmoney*s.interestrate/100)) as total,
        (SELECT  group_concat(f.referencespayment)  FROM fees f WHERE f.subtransactionid = s.id ) as referencespayment,s.transactiontypeid,tp.name as transactiontypename
        FROM  subtransactions s INNER JOIN subtransactionstates ss on ss.subtransactionid=s.id 
        INNER JOIN states st ON st.id=ss.stateid INNER JOIN transactions b ON s.transactionid = b.id
        INNER JOIN vouchers v ON s.voucherid=v.id 
        INNER JOIN vouchertypes vt ON v.vouchertypeid=vt.id 
        INNER JOIN businesssubjects bs ON b.externalsubjectid= bs.id 
        INNER JOIN subjects sub ON sub.id=bs.subjectid 
        INNER JOIN transactiontypes tp ON tp.id=s.transactiontypeid 
        Where   (vt.id in (1,2,3) )  AND ss.stateid in (5,6) "; 
        if (array_key_exists('transactiontypeid',$params)){
            if(!empty($params['transactiontypeid'])){
                $sql.= " AND s.transactiontypeid in (".$params['transactiontypeid'].") ";            
            }
        }

        if (array_key_exists('filtro',$params)){
            if(!empty($params['filtro'])){
                $sql.= " AND (bs.businessname LIKE '%$params[filtro]%' OR v.code LIKE '%$params[filtro]%' OR bs.identitynumber LIKE '%$params[filtro]%' ) ";            
            }
        }
        if (array_key_exists('dateinit',$params)){
            if(!empty($params['dateinit'])){
                $dateinit=explode("/",$params['dateinit']);
                $dateend=explode("/",$params['dateend']);
                $sql.= "and STR_TO_DATE(s.time ,'%Y-%m-%d') between '$dateinit[2]-$dateinit[1]-$dateinit[0]' and '$dateend[2]-$dateend[1]-$dateend[0]'  ";
            }
        }
        $sql.=" order by s.id desc ";
        if(array_key_exists('count',$params)){
            if(!$params['count'] && array_key_exists('limit',$params)){
                $sql.=" LIMIT $params[limit], $params[xpagina] ";
            }
        }        
        $query=DB::select($sql);
        return $query;   
    }
    public function saveFee($params){
        try{     
            DB::transaction(function() use($params){ 
             $userloginid = Auth::id();
             if (array_key_exists("subtransactionid",$params)) {

                $correlatives = $this->correlatives->findOrFail(1);
                $num=((int)$correlatives->numberfee)+1;
                $correlatives->numberfee =sprintf("%'.08d", $num);
                $numberdoc =sprintf('%1$s-%2$08d',$correlatives->seriefee, $num);
                $correlatives->update();  

                $voucher= new Vouchers;
                $voucher->vouchertypeid=4;
                $voucher->serialid=1;
                $voucher->number=$correlatives->numberfee;            
                $voucher->serialnumber=$correlatives->seriefee;
                $voucher->code=$numberdoc;
                $voucher->save();

                $fee = new Fees;
                $fee->code=$numberdoc;
                $fee->issedtime=array_key_exists("date",$params)?date('Y-m-d H:i:s', strtotime($params['date'])):null ;;
                $fee->limittime= new DateTime();
                $fee->amount=array_key_exists("totalneto",$params)?$params['totalneto']:null ;
                $fee->capital=array_key_exists("total",$params)?$params['total']:null ;
                $fee->interest=array_key_exists("interest",$params)?$params['interest']:null ;
                $fee->subtransactionid=array_key_exists("subtransactionid",$params)?$params['subtransactionid']:null ;
                $fee->comment=array_key_exists("dues",$params)?$params['dues']:1 ;
                $fee->voucherid= $voucher->id ;
                $fee->feechanged=array_key_exists("dues",$params)?$params['dues']:1  ;
                $fee->cash=array_key_exists("total",$params)?$params['total']:null ;
                $fee->employedid =$userloginid;
                $fee->referencespayment=array_key_exists("referencespayment",$params)?$params['referencespayment']:null ;
                $fee->accountant = array_key_exists("accountant",$params)?$params['accountant']:1 ;

                $fee->save();

                $details=$params["details"];
                 if(count($details)>0){
                     foreach($details as $item){
                        if($item->id!=0){
                            if (array_key_exists('transactiontypeid',$params)){
                                Paymentschedules::where('id', '=', $item->id)->update(['status' => 0,'advancement' => (isset($item->advancement)?$item->advancement:0),'feeamount'=>$item->feeamount]);
                            }else{
                                Paymentschedules::where('id', '=', $item->id)->update(
                                    isset($item->paymentcanceled)?($item->paymentcanceled==0?
                                    (['status' => 1,'advancement' => $item->advancement]):
                                    (['status' => 0,'advancement' => $item->advancement])):
                                    (['status' => 0,'advancement' => $item->advancement]));
                            }      
                        }
                        /*if ($params['paymentfees']==$item->numberdues) {
                            Subtransactionstates::where('id', '=', $params['subtransactionstateid'])->update(['stateid' => 6]);
                        }*/
                     }
                 }
             }   
         });
         
        }catch(Exception $e){
         DB::rollback();
            throw new Exception("R_ERROR:".$e->getMessage());
        }
            return 1;     
    }
    public function saveFeeClose($params){
        try{     
            DB::transaction(function() use($params){ 
             $userloginid = Auth::id();
             if (array_key_exists("subtransactionid",$params)) {
                $correlatives = $this->correlatives->findOrFail(1);
                $num=((int)$correlatives->numberpaymentclose)+1;
                $correlatives->numberpaymentclose =sprintf("%'.08d", $num);
                $numberdoc =sprintf('%1$s-%2$08d',$correlatives->seriepaymentclose, $num);
                $correlatives->update();  

                $voucher= new Vouchers;
                $voucher->vouchertypeid=7;
                $voucher->serialid=1;
                $voucher->number=$correlatives->numberpaymentclose;            
                $voucher->serialnumber=$correlatives->seriepaymentclose;
                $voucher->code=$numberdoc;
                $voucher->save();

                $transactionclose = new Transactioncloses;
                $transactionclose->code=$numberdoc;
                $transactionclose->issedtime=array_key_exists("date",$params)?date('Y-m-d H:i:s', strtotime($params['date'])):null ;;
                $transactionclose->amount=array_key_exists("totalpayment",$params)?$params['totalpayment']:0 ;
                $transactionclose->capital=array_key_exists("total",$params)?$params['total']:0 ;
                $transactionclose->interest=array_key_exists("interest",$params)?$params['interest']:0 ;
                $transactionclose->subtransactionid=array_key_exists("subtransactionid",$params)?$params['subtransactionid']:null ;
                $transactionclose->voucherid= $voucher->id ;
                $transactionclose->employedid =$userloginid;
                $transactionclose->save();

                $subtransactionstate = $this->subtransactionstate->findOrFail($params['subtransactionstateid']);                
                $subtransactionstate->stateid =9;
                $subtransactionstate->update();
               
             }   
         });
         
        }catch(Exception $e){
         DB::rollback();
            throw new Exception("R_ERROR:".$e->getMessage());
        }
            return 1;     
    }
    public function saveCreditClose($params){
        try{     
            DB::transaction(function() use($params){ 
             $userloginid = Auth::id();
             if (array_key_exists("subtransactionid",$params)) {
                $correlatives = $this->correlatives->findOrFail(1);
                $num=((int)$correlatives->numbercreditclose)+1;
                $correlatives->numbercreditclose =sprintf("%'.08d", $num);
                $numberdoc =sprintf('%1$s-%2$08d',$correlatives->seriecreditclose, $num);
                $correlatives->update();  

                $voucher= new Vouchers;
                $voucher->vouchertypeid=7;
                $voucher->serialid=1;
                $voucher->number=$correlatives->numbercreditclose;            
                $voucher->serialnumber=$correlatives->seriecreditclose;
                $voucher->code=$numberdoc;
                $voucher->save();

                $transactionclose = new Transactioncloses;
                $transactionclose->code=$numberdoc;
                $transactionclose->issedtime=array_key_exists("date",$params)?date('Y-m-d H:i:s', strtotime($params['date'])):null ;;
                $transactionclose->amount=array_key_exists("amount",$params)?$params['amount']:null ;
                $transactionclose->capital=array_key_exists("amount",$params)?$params['amount']:null ;
                $transactionclose->interest=array_key_exists("interest",$params)?$params['interest']:null ;
                $transactionclose->subtransactionid=array_key_exists("subtransactionid",$params)?$params['subtransactionid']:null ;
                $transactionclose->voucherid= $voucher->id ;
                $transactionclose->employedid =$userloginid;
                $transactionclose->save();

                $subtransactionstate = $this->subtransactionstate->findOrFail($params['subtransactionstateid']);                
                $subtransactionstate->stateid =10;
                $subtransactionstate->update();
               
             }   
         });
         
        }catch(Exception $e){
         DB::rollback();
            throw new Exception("R_ERROR:".$e->getMessage());
        }
            return 1;     
    }
    public function removepayment($params){
        try{     
            DB::transaction(function() use($params){ 
             $userloginid = Auth::id();
             if (array_key_exists("subtransactionid",$params)) {

               
                $fee = new Fees;
                $fee->code=$numberdoc;
                $fee->issedtime=array_key_exists("date",$params)?date('Y-m-d H:i:s', strtotime($params['date'])):null ;;
                $fee->limittime= new DateTime();
                $fee->amount=array_key_exists("totalneto",$params)?$params['totalneto']:null ;
                $fee->capital=array_key_exists("total",$params)?$params['total']:null ;
                $fee->interest=array_key_exists("interest",$params)?$params['interest']:null ;
                $fee->subtransactionid=array_key_exists("subtransactionid",$params)?$params['subtransactionid']:null ;
                $fee->comment=array_key_exists("dues",$params)?$params['dues']:1 ;
                $fee->voucherid= $voucher->id ;
                $fee->feechanged=array_key_exists("dues",$params)?$params['dues']:1  ;
                $fee->cash=array_key_exists("total",$params)?$params['total']:null ;
                $fee->referencespayment=array_key_exists("referencespayment",$params)?$params['referencespayment']:null ;
                $fee->save();

                $details=$params["details"];
                 if(count($details)>0){
                     foreach($details as $item){
                        Paymentschedules::where('id', '=', $item->id)->update(['status' => 0]);
                        if ($params['paymentfees']==$item->numberdues) {
                            Subtransactionstates::where('id', '=', $params['subtransactionstateid'])->update(['stateid' => 6]);
                        }
                     }
                 }
             }   
         });
         
        }catch(Exception $e){
         DB::rollback();
            throw new Exception("R_ERROR:".$e->getMessage());
        }
            return 1;     
    }
}
