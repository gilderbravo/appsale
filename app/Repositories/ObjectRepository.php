<?php

namespace App\Repositories;

use Illuminate\Database\Eloquent\Model;
use App\Model\Objects;
use App\Model\Valuedobjects;
use DateTime;
use DB;
class ObjectRepository extends BaseRepository
{
    public function __construct(){
        $this->model=new Objects;
    }

    public function listObject($params){
        $query=DB::select('select o.id,o.code,o.barcode,o.name as nameconcat,o.description,po.id as presentationobjectid ,po.name as presentation,oc.id as objectcategoryid,oc.name as category,om.id as objectmodelid,om.name as model,o.status,v.id as valuedobjectid,v.amount as unitpricesale,findamountbyidobject(o.id) as stock 
                            FROM objects o 
                            inner join objectcategories oc on oc.id=o.objectcategoryid                            
                            inner JOIN presentationobjects po  ON po.id=o.presentationobjectid
                            inner join objectmodels om on po.id = o.objectmodelid
                            INNER JOIN valuedobjects v ON v.objectid=o.id 
                            INNER JOIN rates r ON r.id=v.rateid
                            WHERE o.status =1 and r.id=2');            
            return $query;    
    }
    public function listObjectSale($params){
        $query=DB::select('select  o.id as objectid,o.name as nameconcat, po.name as  unitmeasure,vo.amount AS unitpricesale,vo.id as valuedobjectid,findamountbyidobject(o.id) as stock 
                            FROM objects o 
                            LEFT  JOIN presentationobjects po  ON po.id=o.presentationobjectid
                            INNER JOIN valuedobjects vo ON vo.objectid=o.id 
                            INNER JOIN rates r ON r.id=vo.rateid
                            WHERE r.id=:ratesales',['ratesales'=>1]);            
            return $query;    
    }
    
    public function saveObject($params){                
        try{           
           $isnew=true;
           $object= new Objects;
           if(  array_key_exists("id",$params) ){
               $isnew=false;
               $object = $this->getById($params['id']);
           }
           $object->code=array_key_exists("code",$params)?$params['code']:null;
           $object->barcode=array_key_exists("barcode",$params)?$params['barcode']:null;
           $object->name=$params['name'];
           $object->description=array_key_exists("description",$params)?$params['description']:null;

           $object->presentationobjectid=array_key_exists("presentationobjectid",$params)?$params['presentationobjectid']:1;
           $object->objectcategoryid=array_key_exists("objectcategoryid",$params)?$params['objectcategoryid']:1;
           $object->objectmodelid=array_key_exists("objectmodelid",$params)?$params['objectmodelid']:1;
           $object->status=1;                      
           if($isnew) $object->save();
           else $object->update(); 

        //    para guardar el precio de compra y venta
           $valuedobjectsale= new Valuedobjects;
           $today = new DateTime();
           if(  array_key_exists("valuedobjectsaleid",$params) ){
                    $valuedobjectsale = $this->getById($params['valuedobjectsaleid']);
                    $valuedobjectsale->objectid=$object->id;
                    $valuedobjectsale->rateid=1;
                    $valuedobjectsale->amount=array_key_exists("pricesale",$params)?$params['pricesale']:0;
                    $valuedobjectsale->modificationdate=$today;
                    $valuedobjectsale->update(); 
           }else{
                    $valuedobjectsale->objectid=$object->id;
                    $valuedobjectsale->rateid=1;
                    $valuedobjectsale->amount=array_key_exists("pricesale",$params)?$params['pricesale']:0;
                    $valuedobjectsale->fromtime=$today;
                    $valuedobjectsale->totime=$today;
                    $valuedobjectsale->modificationdate=$today;                                          
                    $valuedobjectsale->save();
           }       
           $valuedobjectpurchase= new Valuedobjects;
           if(  array_key_exists("valuedobjectpurchaseid",$params) ){
                    $valuedobjectpurchase = $this->getById($params['valuedobjectpurchaseid']);
                    $valuedobjectpurchase->objectid=$object->id;
                    $valuedobjectpurchase->rateid=2;
                    $valuedobjectpurchase->amount=array_key_exists("pricepurchase",$params)?$params['pricepurchase']:0;
                    $valuedobjectpurchase->modificationdate=$today;
                    $valuedobjectpurchase->update(); 
            }else{
                    $valuedobjectpurchase->objectid=$object->id;
                    $valuedobjectpurchase->rateid=2;
                    $valuedobjectpurchase->amount=array_key_exists("pricepurchase",$params)?$params['pricepurchase']:0;
                    $valuedobjectpurchase->fromtime=$today;
                    $valuedobjectpurchase->totime=$today;
                    $valuedobjectpurchase->modificationdate=$today;                                          
                    $valuedobjectpurchase->save();
            }               

       }catch(Exception $e){
           throw new Exception("R_ERROR:".$e->getMessage());
       }
           return $object;        
   }

    public function delete($params){
    }
}
