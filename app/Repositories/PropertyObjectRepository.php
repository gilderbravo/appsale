<?php

namespace App\Repositories;
use Illuminate\Database\Eloquent\Model;
use App\Model\Propertyobjects;
use DB;

class PropertyObjectRepository extends BaseRepository
{
    public function __construct(){
        $this->model=new Propertyobjects;
    }
    public function listProperty($params){
        $query=DB::table('propertyobjects')->get();
        return $query;    
    }

    public function savePropertyObjects($params){                
        try{           
           $isnew=true;
           $propertyobjects= new Propertyobjects;
           if(  array_key_exists("id",$params) ){
               $isnew=false;
               $propertyobjects = $this->getById($params['id']);
           }
           $propertyobjects->name=$params['name'];
           $propertyobjects->description=$params['description'];             
           if($isnew) $propertyobjects->save();
           else $propertyobjects->update(); 
           
       }catch(Exception $e){
           throw new Exception("R_ERROR:".$e->getMessage());
       }
           return $propertyobjects;        
   }
    public function delete($params){
    }
}
