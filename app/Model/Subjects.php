<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Subjects extends Model
{
    //
    public $timestamps=false;
    protected $fillable = [
        'name', 'identitydocumentid', 'identitynumber', 'firstname', 'secondname', 'birthday', 'address', 'phone',
    ];

}
