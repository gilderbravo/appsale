<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Businesssubjects extends Model
{
    //
    public $timestamps=false;
    protected $fillable = [
        'subjectid', 'identitydocumentid', 'identitynumber', 'businessname', 'subjectrolcategoryid',
        'startdate', 'address', 'phone','cyty','status',
    ];

}
