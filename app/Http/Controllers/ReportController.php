<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use App\Services\ReportService;

class ReportController extends Controller
{
    private $service;
    public function __construct( ReportService $service ){
        $this->service = $service;
        $this->middleware('auth');
    }
    public function indexPayments(){
        return view("reportpayments.index");
    }
    public function indeReportcloses(){
        return view("reportcloses.index");
    }
    
    public function indexBalancesheet(){
        return view("balancesheet.index");
    }
    public function deletefee(){
        $postData = file_get_contents("php://input");
        $request = json_decode($postData); 
        $request = (array) $request;            
               
        $list=$this->service->deletefee($request);
        return response()->json($list);
    }
    public function updateCreditReversal(){
        $postData = file_get_contents("php://input");
        $request = json_decode($postData); 
        $request = (array) $request;            
               
        $list=$this->service->updateCreditReversal($request);
        return response()->json($list);
    }
    
    public function listReportpayments(){
        $postData = file_get_contents("php://input");
        $request = json_decode($postData);
        $request = (array) $request;
        if(!array_key_exists('all',$request)){
            $request['pagina'] = isset($request['pagina'])?(int)$request['pagina']:1;
            $request['xpagina'] = 10;
            $request['count'] = false;
            $request['limit'] = ((int)$request['pagina']-1)*(int)$request['xpagina'];
        }
        $list=$this->service->listReportpayments($request);
        return response()->json($list);
    }
    public function listReportTransactionCloses(){
        $postData = file_get_contents("php://input");
        $request = json_decode($postData);
        $request = (array) $request;      
        if(!array_key_exists('all',$request)){
            $request['pagina'] = isset($request['pagina'])?(int)$request['pagina']:1;
            $request['xpagina'] = 10;
            $request['count'] = false;
            $request['limit'] = ((int)$request['pagina']-1)*(int)$request['xpagina'];
        }  
        $list=$this->service->listReportTransactionCloses($request);
        return response()->json($list);
    }
    public function listCashboxSummary(){
        $postData = file_get_contents("php://input");
        $request = json_decode($postData);
        $request = (array) $request;
        if(!array_key_exists('all',$request)){
            $request['pagina'] = isset($request['pagina'])?(int)$request['pagina']:1;
            $request['xpagina'] = 10;
            $request['count'] = false;
            $request['limit'] = ((int)$request['pagina']-1)*(int)$request['xpagina'];
        }
        $list=$this->service->listCashboxSummary($request);
        return response()->json($list);
    }

}
