<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use App\Services\TransactionService;


class TransactionController extends Controller
{
    private $transactionService;
    public function __construct( TransactionService $transactionService ){
        $this->transactionService = $transactionService;               
        $this->middleware('auth');
    }
    public function index(){
        return view("sale.index");
    }
    public function creditsale(){
        return view("credit.index");
    }
    public function fixedterm(){
        return view("fixedterm.index");
    }
    public function savingmoney(){
        return view("savingmoney.index");
    }
    public function indexPurchase(){
        return view("purchase.index");
    }
    public function indexExpense(){
        return view("expense.index");
    }
    public function indexPayment(){
        return view("payment.index");
    }
    public function indexCashbox(){
        return view("cashbox.index");
    }
    public function indexDeposit(){
        return view("deposit.index");
    }
    public function indexQualification(){
        return view("qualification.index");
    }
    public function listSale(){
        $postData = file_get_contents("php://input");
        $request = json_decode($postData); 
        $request = (array) $request;            
        if(!array_key_exists('all',$request)){
            $request['pagina'] = isset($request['pagina'])?(int)$request['pagina']:1;
            $request['xpagina'] = 5;
            $request['count'] = false;
            $request['limit'] = ((int)$request['pagina']-1)*(int)$request['xpagina'];
        }          
        $list=$this->transactionService->listSale($request);
        return response()->json($list);
    }
    public function listSaleTicket(){
        $postData = file_get_contents("php://input");
        $request = json_decode($postData); 
        $request = (array) $request;            
        if(!array_key_exists('all',$request)){
            $request['pagina'] = isset($request['pagina'])?(int)$request['pagina']:1;
            $request['xpagina'] = 5;
            $request['count'] = false;
            $request['limit'] = ((int)$request['pagina']-1)*(int)$request['xpagina'];
        }          
        $list=$this->transactionService->listSaleTicket($request);
        return response()->json($list);
    }
    public function listSubtransactiondetails(){
        $postData = file_get_contents("php://input");
        $request = json_decode($postData); 
        $request = (array) $request;                
        $listdetails=$this->transactionService->listSubtransactiondetails($request);
        return response()->json($listdetails);
    }
    public function saveSale(){
        $postData = file_get_contents("php://input");
        $request = json_decode($postData);   
        $request = (array) $request;  
        $sale=$this->transactionService->saveSale($request);              
        return response()->json($sale);
    }
    public function saveSaleTicket(){
        $postData = file_get_contents("php://input");
        $request = json_decode($postData);   
        $request = (array) $request;  
        $sale=$this->transactionService->saveSaleTicket($request);              
        return response()->json($sale);
    }
    
    public function savePurchase(){
        $postData = file_get_contents("php://input");
        $request = json_decode($postData);   
        $request = (array) $request;  
        $sale=$this->transactionService->savePurchase($request);              
        return response()->json($sale);            
    }

    public function listPurchase(){
        $postData = file_get_contents("php://input");
        $request = json_decode($postData); 
        $request = (array) $request;            
        if(!array_key_exists('all',$request)){
            $request['pagina'] = isset($request['pagina'])?(int)$request['pagina']:1;
            $request['xpagina'] = 5;
            $request['count'] = false;
            $request['limit'] = ((int)$request['pagina']-1)*(int)$request['xpagina'];
        }          
        $list=$this->transactionService->listPurchase($request);
        return response()->json($list);
    }



    public function allBrandObject(){
        $postData = file_get_contents("php://input");
        $request = json_decode($postData);                 
        $brandObject=$this->brandObjectService->allBrandObject($request);

        return response()->json($brandObject);
    }
    public function saveBrandObject(){
        $postData = file_get_contents("php://input");
        $request = json_decode($postData);  
        $request = (array) $request;            
        $brandObject=$this->brandObjectService->saveBrandObject($request);
        return response()->json($brandObject);
       
    }
    public function deletesale(){
        $postData = file_get_contents("php://input");
        $request = json_decode($postData); 
        $request = (array) $request;            
               
        $list=$this->transactionService->delete($request);
        return response()->json($list);
    }
    public function changestatusdisbursement(){
        $postData = file_get_contents("php://input");
        $request = json_decode($postData); 
        $request = (array) $request;            
               
        $list=$this->transactionService->changestatusdisbursement($request);
        return response()->json($list);
    }
    public function deleteBrandObject(){
        $result="";         
        try{
            $postData = file_get_contents("php://input");
            $request = json_decode($postData);  
            $brandObject=$this->brandObjectService->deleteBrandObject($request); 
            $result=$brandObject;
        }catch(Exception $e){
            $result=$e->getMessage();
        }   
        
        return response()->json($result);              
    }

    public function allAction(Request $request){
        try{
            $user = $this->getUser();
            $post = $request->getContent();
            $params = json_decode($post);
            $params = (array) $params;
            $params['iduser'] = $user->getId();
            if(isset($params['pagina'])){
            $params['pagina'] = isset($params['pagina'])?$params['pagina']:1;
            $params['xpagina'] = 15;
            $params['count'] = false;
            $params['limit'] = ($params['pagina']-1)*$params['xpagina'];
            }
            $list= (array) $this->get('questionService')->all($params);
            return new JsonResponse($list);
        } catch (Exception $e){
                throw new \Exception($e->getMessage());
        }
    }
    public function savePaymentSchedule(){
        $postData = file_get_contents("php://input");
        $request = json_decode($postData);   
        $request = (array) $request;  
        $payment=$this->transactionService->savePaymentSchedule($request);              
        return response()->json($payment);
    }
    public function saveCashbox(){
        $postData = file_get_contents("php://input");
        $request = json_decode($postData);   
        $request = (array) $request;  
        $sale=$this->transactionService->saveCashbox($request);              
        return response()->json($sale);
    }
    public function saveExpenseAndPayment(){
        $postData = file_get_contents("php://input");
        $request = json_decode($postData);   
        $request = (array) $request;  
        $sale=$this->transactionService->saveExpenseAndPayment($request);              
        return response()->json($sale);
    }
    public function saveDepositAndQualification(){
        $postData = file_get_contents("php://input");
        $request = json_decode($postData);   
        $request = (array) $request;  
        $sale=$this->transactionService->saveDepositAndQualification($request);              
        return response()->json($sale);
    }
    public function listExpenseAndPayment(){
        $postData = file_get_contents("php://input");
        $request = json_decode($postData); 
        $request = (array) $request;    
               
        if(!array_key_exists('all',$request)){
            $request['pagina'] = isset($request['pagina'])?(int)$request['pagina']:1;
            $request['xpagina'] = 5;
            $request['count'] = false;
            $request['limit'] = ((int)$request['pagina']-1)*(int)$request['xpagina'];
        }          
        $list=$this->transactionService->listExpenseAndPayment($request);
        return response()->json($list);
    }
    public function listCashbox(){
        $postData = file_get_contents("php://input");
        $request = json_decode($postData); 
        $request = (array) $request;            
        if(!array_key_exists('all',$request)){
            $request['pagina'] = isset($request['pagina'])?(int)$request['pagina']:1;
            $request['xpagina'] = 5;
            $request['count'] = false;
            $request['limit'] = ((int)$request['pagina']-1)*(int)$request['xpagina'];
        }          
        $list=$this->transactionService->listCashbox($request);
        return response()->json($list);
    }
}
