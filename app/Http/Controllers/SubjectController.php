<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use App\Services\SubjectService;


class SubjectController extends Controller
{
    private $subjectService;

    public function __construct( SubjectService $subjectService){
        $this->subjectService = $subjectService;       
        $this->middleware('auth');
    }
    public function indexClient(){
        return view("client.index");
    }
    public function indexProvider(){
        return view("provider.index");
    }
    public function indexEmployeed(){
        return view("employeed.index");
    }
    public function listClient(){
        // var_dump( file_get_contents('php://input'));

        $postData = file_get_contents("php://input");
        $request = json_decode($postData);
        $request = (array) $request;
        if(!array_key_exists('all',$request)){
            $request['pagina'] = isset($request['pagina'])?(int)$request['pagina']:1;
            $request['xpagina'] = 5;
            $request['count'] = false;
            $request['limit'] = ((int)$request['pagina']-1)*(int)$request['xpagina'];
        }
        $client=$this->subjectService->listClient($request);

        return response()->json($client);
    }
    public function listProvider(){
        $postData = file_get_contents("php://input");
        $request = json_decode($postData);
        $request = (array) $request;
        if(!array_key_exists('all',$request)){
            $request['pagina'] = isset($request['pagina'])?(int)$request['pagina']:1;
            $request['xpagina'] = 5;
            $request['count'] = false;
            $request['limit'] = ((int)$request['pagina']-1)*(int)$request['xpagina'];
        }
        $client=$this->subjectService->listProvider($request);

        return response()->json($client);
    }
    public function searchdniruc(){
        $postData = file_get_contents("php://input");
        $request = json_decode($postData);
        $client=$this->subjectService->searchdniruc($request);

        return response()->json($client);
    }
    public function listIdentityDocument(){
        $postData = file_get_contents("php://input");
        $request = json_decode($postData);                 
        $identitydocument=$this->subjectService->listIdentityDocument($request);
        return response()->json($identitydocument);
    }
    public function listPositions(){
        $postData = file_get_contents("php://input");
        $request = json_decode($postData);
        $identitydocument=$this->subjectService->listPositions($request);
        return response()->json($identitydocument);
    }

    public function saveClient(){
        $postData = file_get_contents("php://input");
        $request = json_decode($postData);  
        $request = (array) $request;            
        $client=$this->subjectService->saveClient($request);
        return response()->json($client);       
    }
    public function saveUser(){
        $postData = file_get_contents("php://input");
        $request = json_decode($postData);
        $request = (array) $request;
        $data=$this->subjectService->saveUser($request);
        return response()->json($data);
    }
    public function updateUser(){
        $postData = file_get_contents("php://input");
        $request = json_decode($postData);
        $request = (array) $request;
        $data=$this->subjectService->updateUser($request);
        return response()->json($data);
    }
    public function saveEmployeed(){
        $postData = file_get_contents("php://input");
        $request = json_decode($postData);
        $request = (array) $request;
        $employeed=$this->subjectService->saveEmployeed($request);
        return response()->json($employeed);
    }
    public function listEmployeed(){
        $postData = file_get_contents("php://input");
        $request = json_decode($postData);
        $request = (array) $request;
        if(!array_key_exists('all',$request)){
            $request['pagina'] = isset($request['pagina'])?(int)$request['pagina']:1;
            $request['xpagina'] = 5;
            $request['count'] = false;
            $request['limit'] = ((int)$request['pagina']-1)*(int)$request['xpagina'];
        }
        $employeed=$this->subjectService->listEmployeed($request);

        return response()->json($employeed);
    }
}
