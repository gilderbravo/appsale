<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use App\Services\ObjectService;
use App\Services\BrandObjectService;
use App\Services\CategoryObjectService;
use App\Services\PropertyObjectService;
use App\Services\PresentationObjectService;
use App\Services\ObjectModelService;

class ObjectController extends Controller
{
    private $objectService;
    private $brandObjectService;
    private $categoryObjectService;
    private $propertyObjectService;
    private $presentationObjectService;
    private $objectModelService;
    
    public function __construct( ObjectService $objectService,BrandObjectService $brandObjectService,CategoryObjectService $categoryObjectService,PropertyObjectService  $propertyObjectService,PresentationObjectService $presentationObjectService,ObjectModelService $objectModelService){
        $this->objectService = $objectService;       
        $this->brandObjectService = $brandObjectService;
        $this->categoryObjectService = $categoryObjectService;   
        $this->propertyObjectService = $propertyObjectService;   
        $this->presentationObjectService = $presentationObjectService;
        $this->objectModelService = $objectModelService;
        $this->middleware('auth');
    }
    public function index(){
        return view("object.index");
    }
    public function indexProperty(){
        return view("propertyObjects.index");
    }
    public function indexPresentation(){
        return view("presentationObjects.index");
    }
    public function indexBrand(){
        return view("brandObject.index");
    }
    public function indexCategory(){
        return view("categoryObjects.index");
    }
    public function indexObjectModel(){
        return view("objectModels.index");
    }
    public function allBrandObject(){
        $postData = file_get_contents("php://input");
        $request = json_decode($postData);                 
        $brandObject=$this->brandObjectService->allBrandObject($request);

        return response()->json($brandObject);
    }
    public function listcategory(){
        $postData = file_get_contents("php://input");
        $request = json_decode($postData);                 
        $categoryObject=$this->categoryObjectService->listcategory($request);

        return response()->json($categoryObject);
    }
    public function saveCategoryObjects(){
        $postData = file_get_contents("php://input");
        $request = json_decode($postData);  
        $request = (array) $request;            
        $categoryObject=$this->categoryObjectService->saveCategoryObject($request);
        return response()->json($categoryObject);
       
    }

    public function listProperty(){
        $postData = file_get_contents("php://input");
        $request = json_decode($postData);                 
        $propertyObject=$this->propertyObjectService->listProperty($request);

        return response()->json($propertyObject);
    }
    public function savePropertyObjects(){
        $postData = file_get_contents("php://input");
        $request = json_decode($postData);
        $request = (array) $request;
        $propertyObject=$this->propertyObjectService->savePropertyObjects($request);
        return response()->json($propertyObject);       
    }

    public function listObject(){
        $postData = file_get_contents("php://input");
        $request = json_decode($postData);                 
        $object=$this->objectService->listObject($request);

        return response()->json($object);
    }
    public function listObjectSale(){
        $postData = file_get_contents("php://input");
        $request = json_decode($postData);                 
        $object=$this->objectService->listObjectSale($request);

        return response()->json($object);
    }
    
    public function saveObject(){
        $postData = file_get_contents("php://input");
        $request = json_decode($postData);  
        $request = (array) $request;            
        $object=$this->objectService->saveObject($request);
        return response()->json($object);       
    }

    public function listObjectModel(){
        $postData = file_get_contents("php://input");
        $request = json_decode($postData);                 
        $objectModel=$this->objectModelService->listObjectModel($request);

        return response()->json($objectModel);
    }
    public function saveObjectModel(){
        $postData = file_get_contents("php://input");
        $request = json_decode($postData);  
        $request = (array) $request;            
        $objectModel=$this->objectModelService->saveObjectModel($request);
        return response()->json($objectModel);       
    }

    public function listpresentation(){
        $postData = file_get_contents("php://input");
        $request = json_decode($postData);                 
        $presentationObject=$this->presentationObjectService->listpresentation($request);

        return response()->json($presentationObject);
    }
    public function savePresentationObjects(){
        $postData = file_get_contents("php://input");
        $request = json_decode($postData);  
        $request = (array) $request;            
        $presentationObject=$this->presentationObjectService->savePresentationObjects($request);
        return response()->json($presentationObject);       
    }

    public function saveBrandObject(){
        $postData = file_get_contents("php://input");
        $request = json_decode($postData);  
        $request = (array) $request;            
        $brandObject=$this->brandObjectService->saveBrandObject($request);
        return response()->json($brandObject);
       
    }
    public function deleteBrandObject(){
        $result="";         
        try{
            $postData = file_get_contents("php://input");
            $request = json_decode($postData);  
            $brandObject=$this->brandObjectService->deleteBrandObject($request); 
            $result=$brandObject;
        }catch(Exception $e){
            $result=$e->getMessage();
        }   
        
        return response()->json($result);              
    }

    public function allAction(Request $request){
        try{
            $user = $this->getUser();
            $post = $request->getContent();
            $params = json_decode($post);
            $params = (array) $params;
            $params['iduser'] = $user->getId();
            if(isset($params['pagina'])){
            $params['pagina'] = isset($params['pagina'])?$params['pagina']:1;
            $params['xpagina'] = 15;
            $params['count'] = false;
            $params['limit'] = ($params['pagina']-1)*$params['xpagina'];
            }
            $list= (array) $this->get('questionService')->all($params);
            return new JsonResponse($list);
        } catch (Exception $e){
                throw new \Exception($e->getMessage());
        }
    }
}
