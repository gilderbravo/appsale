<?php

namespace App\Http\Controllers\Auth;

use App\User;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\RegistersUsers;
use App\Model\Employeds;
use App\Model\Businesssubjects;
use App\Model\Subjects;

/**
 * Class RegisterController
 * @package %%NAMESPACE%%\Http\Controllers\Auth
 */
class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Show the application registration form.
     *
     * @return \Illuminate\Http\Response
     */
    public function showRegistrationForm()
    {
        return view('adminlte::auth.register');
    }

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|min:6|confirmed',
            'terms' => 'required',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {

        $subject=Subjects::create([
            'name' => $data['name'],
            'identitydocumentid' => 1,
            'identitynumber' => '00000000',
            'firstname' => '',
            'secondname' => '',
            'birthday' => null,
            'address' => '',
            'phone' => '',
        ]);
        $businesssubject=Businesssubjects::create([
            'subjectid' => $subject->id,
            'identitydocumentid' => 1,
            'identitynumber' => '00000000',
            'businessname' =>  $data['name'],
            'subjectrolcategoryid' => 2,
            'startdate' => null,
            'address' => '',
            'phone' => '',
            'cyty' => '',
            'status' => '1',
        ]);
        $employed=Employeds::create([
            'businesssubjectid' => $businesssubject->id,
            'positionid' => 1,
        ]);
        $users=User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
            'employedid' => $employed->id,
        ]);
        return $users;
    }
}
