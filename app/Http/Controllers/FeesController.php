<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use App\Services\FeesService;


class FeesController extends Controller
{
    private $feesService;
    public function __construct( FeesService $feesService ){
        $this->feesService = $feesService;               
        $this->middleware('auth');
    }
    public function index(){
        return view("fees.index");
    }
    public function indexfeespayment(){
        return view("feespayment.index");
    }
    public function indexfeespaymentclose(){
        return view("feespaymentclose.index");
    }
    public function indexfeescreditclose(){
        return view("feescreditclose.index");
    }
    public function listpaymentschedule(){
        $postData = file_get_contents("php://input");
        $request = json_decode($postData); 
        $request = (array) $request;            
        if(!array_key_exists('all',$request)){
            $request['pagina'] = isset($request['pagina'])?(int)$request['pagina']:1;
            $request['xpagina'] = 5;
            $request['count'] = false;
            $request['limit'] = ((int)$request['pagina']-1)*    (int)$request['xpagina'];
        }          
        $list=$this->feesService->listpaymentschedule($request);
        return response()->json($list);
    }

    public function listFeesClient(){
        $postData = file_get_contents("php://input");
        $request = json_decode($postData); 
        $request = (array) $request;            
        if(!array_key_exists('all',$request)){
            $request['pagina'] = isset($request['pagina'])?(int)$request['pagina']:1;
            $request['xpagina'] = 5;
            $request['count'] = false;
            $request['limit'] = ((int)$request['pagina']-1)*    (int)$request['xpagina'];
        }          
        $list=$this->feesService->listFeesClient($request);
        return response()->json($list);
    }
    public function saveFee(){
        $postData = file_get_contents("php://input");
        $request = json_decode($postData);   
        $request = (array) $request;  
        $fee=$this->feesService->saveFee($request);              
        return response()->json($fee);
    }
    public function saveFeeClose(){
        $postData = file_get_contents("php://input");
        $request = json_decode($postData);   
        $request = (array) $request;  
        $fee=$this->feesService->saveFeeClose($request);              
        return response()->json($fee);
    }
    public function saveCreditClose(){
        $postData = file_get_contents("php://input");
        $request = json_decode($postData);   
        $request = (array) $request;  
        $fee=$this->feesService->saveCreditClose($request);              
        return response()->json($fee);
    }
    public function removepayment(){
        $postData = file_get_contents("php://input");
        $request = json_decode($postData);   
        $request = (array) $request;  
        $fee=$this->feesService->removepayment($request);              
        return response()->json($fee);
    }
}
