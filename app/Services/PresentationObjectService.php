<?php

namespace App\Services;
use App\Repositories\PresentationObjectRepository;

class PresentationObjectService
{
    private $repository;
    public function __construct(PresentationObjectRepository $repository){
        $this->repository=$repository;        
    }
    public function listpresentation($params){
        $presentationRepository= $this->repository->listpresentation($params);        
            return $presentationRepository;        
    }
    public function savePresentationObjects($params){                         
        $presentationRepository=$this->repository->savePresentationObjects($params);    
    }
    public function delete($params){
    }
    
}
