<?php

namespace App\Services;

use App\Repositories\ReportRepository;
use App\Repositories\TransactionRepository;
use DateTime;
class ReportService
{
    private $repository;
    private $transactionrepository;
    public function __construct(ReportRepository $repository,TransactionRepository $transactionrepository){
        $this->repository=$repository;
        $this->transactionrepository=$transactionrepository;
    }

    public function listReportpayments($params){
        $list= $this->repository->listReportpayments($params);
        $pagina = isset($params['pagina'])?$params['pagina']:0;
        if(!is_null($params) && $pagina>0){
            $params['count']=true;
            $totalfeeamount=0;
            $listreport=$this->repository->listReportpayments($params);
            foreach($listreport as $item){
                if ($item->accountant != 0) {
                    $totalfeeamount=$totalfeeamount+$item->cash;
                }
            }
            $total = sizeof($listreport);
            $response=array(
                'list'=>$list,
                'xpagina'=>$params['xpagina'],
                'pagina'=>$params['pagina'],
                'total'=>(int) $total,
                'totalfeeamount'=>$totalfeeamount,
            );
        }else{
            $response = $list;
        }
        return $response;
    }
    public function listReportTransactionCloses($params){        
        $list= $this->repository->listReportTransactionCloses($params);
        $pagina = isset($params['pagina'])?$params['pagina']:0;
        if(!is_null($params) && $pagina>0){
            $params['count']=true;
            $totalamount=0;
            $listreport=$this->repository->listReportTransactionCloses($params);
            foreach($listreport as $item){
                    $totalamount=$totalamount+$item->total;                
            }
            $total = sizeof($listreport);
            $response=array(
                'list'=>$list,
                'xpagina'=>$params['xpagina'],
                'pagina'=>$params['pagina'],
                'total'=>(int) $total,
                'totalamount'=>$totalamount,
            );
        }else{
            $response = $list;
        }
        return $response;
        
    }
    public function deletefee($params){
        $report=$this->repository->deletefee($params);
        return $report;
    }
    public function updateCreditReversal($params){
        $report=$this->repository->updateCreditReversal($params);
        return $report;
    }
    public function listCashboxSummary($params){
        $params['sumtotal'] = true;
        $totalfeeamount=0;
        $totalpaymentamount=0;
        $totalexpenseamount=0;
        $totaldebitamount=0;
        $initamount=0;
        $money='';
        $totalqualificationamount=0;
//  sumar todos lo necesario para el cuadre de caja
        $list= $this->repository->listReportpayments($params); 
        foreach($list as $item){
            if ($item->accountant != 0) {
                $totalfeeamount=$totalfeeamount+$item->cash;
            }
        }
        // var_dump($totalfeeamount);
        $listexpense= $this->transactionrepository->listExpenseAndPayment($params); 
        foreach($listexpense as $item){
            $totalexpenseamount=$totalexpenseamount+$item->total;
        }
        // var_dump($totalexpenseamount);
        $params['transactiontypeid'] = 6;
        
        $listpayment= $this->transactionrepository->listExpenseAndPayment($params); 
        
        foreach($listpayment as $item2){
            $totalpaymentamount=$totalpaymentamount+$item2->total;
        }
        //-------------------------.-- depositos diarias
        $params['transactiontypeid'] = 9;
        
        $listdebit= $this->transactionrepository->listExpenseAndPayment($params); 
        
        foreach($listdebit as $item3){
            $totaldebitamount=$totaldebitamount+$item3->total;
        }
        //---------------hiabitlitracio de gerencia
        $params['transactiontypeid'] = 8;
        
        $listqualification= $this->transactionrepository->listExpenseAndPayment($params); 
        
        foreach($listqualification as $item4){
            $totalqualificationamount=$totalqualificationamount+$item4->total;
        }
        //----------caja init
        $listcashbox= $this->transactionrepository->listCashbox($params); 
        
        foreach($listcashbox as $item5){
            $initamount=$initamount+$item5->initamount;
            $money=$item5->ticketing;
        }
        // var_dump($totalpaymentamount);
        $response=[
            'totalfeeamount'=>$totalfeeamount,
            'totalpaymentamount'=>$totalpaymentamount,
            'totalexpenseamount'=>$totalexpenseamount ,
            'totalqualificationamount'=>$totalqualificationamount ,
            'totaldebitamount'=>$totaldebitamount ,
            'initamount'=>$initamount ,
            'money'=>$money ,
            
        ];
        
        return $response;
    }
}
