<?php

namespace App\Services;
use App\Repositories\PropertyObjectRepository;

class PropertyObjectService
{
   
    private $repository;
    public function __construct(PropertyObjectRepository $repository){
        $this->repository=$repository;        
    }
    public function listProperty($params){
        $propertyRepository= $this->repository->listProperty($params);        
            return $propertyRepository;        
    }
    public function savePropertyObjects($params){                         
        $propertyRepository=$this->repository->savePropertyObjects($params);    
    }
    public function delete($params){
    }
    
}
