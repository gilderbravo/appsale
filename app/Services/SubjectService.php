<?php

namespace App\Services;

use App\Repositories\SubjectRepository;
use App\Utils\SearchDNIRUC;
class SubjectService
{
   
    private $repository;
    public function __construct(SubjectRepository $repository){
            $this->repository=$repository;        
    }
    public function listClient($params){

        $list= $this->repository->listClient($params);
        $pagina = isset($params['pagina'])?$params['pagina']:0;
        if(!is_null($params) && $pagina>0){
            $params['count']=true;
            $total = sizeof($this->repository->listClient($params));
            $response=array(
                'list'=>$list,
                'xpagina'=>$params['xpagina'],
                'pagina'=>$params['pagina'],
                'total'=>(int) $total,
            );
        }else{
            $response = $list;
        }
        return $response;
    }
    public function listProvider($params){

        $list= $this->repository->listProvider($params);
        $pagina = isset($params['pagina'])?$params['pagina']:0;
        if(!is_null($params) && $pagina>0){
            $params['count']=true;
            $total = sizeof($this->repository->listProvider($params));
            $response=array(
                'list'=>$list,
                'xpagina'=>$params['xpagina'],
                'pagina'=>$params['pagina'],
                'total'=>(int) $total,
            );
        }else{
            $response = $list;
        }
        return $response;
    }
    public function searchdniruc($params){
//        $subjectRepository= $this->repository->searchdniruc($params);

        if( ($params->type=="2"&&strlen($params->number)!=11)||($params->type=="1"&&strlen($params->number)!=8) ){
            throw new \Exception("$params->number no valido");
        }

        $searchDNIRUC=new SearchDNIRUC();
        if($params->type=="1"){
            $data=$searchDNIRUC->consultarJNE($params->number);
            if(!$data) $data=$searchDNIRUC->consultarDNIDSDInformaticos($params->number);

        }else{
            $data=$searchDNIRUC->consultarSUNAT($params->number);
            if(!$data) $data=$searchDNIRUC->consultarConsultaderucinfo($params->number);
            if(!$data) $data=$searchDNIRUC->consultarTechnology($params->number);
        }

        if(!$data) throw new \Exception("SUNAT O JNE no responde");

        $response=[
            'identitynumber'=>$data['documento'],
            'nombres'=>$data['nombres'],
            'apellido_paterno'=>$data['apellido_paterno'],
            'apellido_materno'=>$data['apellido_materno'],
            'fecha_nacimiento'=>$data['fecha_nacimiento'] ,
            'address'=>$data['Direccion'],

        ];
        return $response;

//        return $subjectRepository;
    }
    public function listIdentityDocument($params){
        $subjectRepository= $this->repository->listIdentityDocument($params);        
            return $subjectRepository;        
    }
    public function listPositions($params){
        $repository= $this->repository->listPositions($params);
        return $repository;
    }
    public function saveClient($params){
        $subjectRepository=$this->repository->saveClient($params);
    }
    public function saveUser($params){
        $subjectRepository=$this->repository->saveUser($params);
    }
    public function updateUser($params){
        $subjectRepository=$this->repository->updateUser($params);
    }
    public function delete($params){
    }
    public function listEmployeed($params){

        $list= $this->repository->listEmployeed($params);
        $pagina = isset($params['pagina'])?$params['pagina']:0;
        if(!is_null($params) && $pagina>0){
            $params['count']=true;
            $total = sizeof($this->repository->listEmployeed($params));
            $response=array(
                'list'=>$list,
                'xpagina'=>$params['xpagina'],
                'pagina'=>$params['pagina'],
                'total'=>(int) $total,
            );
        }else{
            $response = $list;
        }
        return $response;
    }
    public function saveEmployeed($params){
        $subjectRepository=$this->repository->saveEmployeed($params);
    }


}
