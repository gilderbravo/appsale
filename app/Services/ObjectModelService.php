<?php

namespace App\Services;
use App\Repositories\ObjectModelRepository;


class ObjectModelService
{
   
    private $repository;
    public function __construct(ObjectModelRepository $repository){
        $this->repository=$repository;        
    }
    public function listObjectModel($params){
        $objectModelRepository= $this->repository->listObjectModel($params);        
            return $objectModelRepository;        
    }
    public function saveObjectModel($params){                         
        $objectModelRepository=$this->repository->saveObjectModel($params);    
    }
    public function delete($params){
    }
    
}
