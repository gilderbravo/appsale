<?php

namespace App\Services;
use App\Repositories\TransactionRepository;
use DateTime;
class TransactionService
{
   
    private $repository;
    public function __construct(TransactionRepository $repository){
            $this->repository=$repository;        
    }

    public function listSale($params){
        $list= $this->repository->listSale($params); 
        $totalmount=0;    
        foreach($list as $item){
            $totalmount=$totalmount+$item->total;
        }
        $pagina = isset($params['pagina'])?$params['pagina']:0;
        
        if(!is_null($params) && $pagina>0){
            $params['count']=true;
            $total = sizeof($this->repository->listSale($params));                       
            $response=array(
                'list'=>$list,
                'xpagina'=>$params['xpagina'],                
                'pagina'=>$params['pagina'],
                'total'=>(int) $total,
            );            
        }else{
            $response = $list;
        }
        return $response;          
    }
    public function listSaleTicket($params){
        $list= $this->repository->listSaleTicket($params); 
        $totalmount=0;    
        foreach($list as $item){
            $totalmount=$totalmount+$item->total;
        }
        $pagina = isset($params['pagina'])?$params['pagina']:0;
        
        if(!is_null($params) && $pagina>0){
            $params['count']=true;
            $total = sizeof($this->repository->listSaleTicket($params));                       
            $response=array(
                'list'=>$list,
                'xpagina'=>$params['xpagina'],                
                'pagina'=>$params['pagina'],
                'total'=>(int) $total,
            );            
        }else{
            $response = $list;
        }
        return $response;          
    }

    public function listPurchase($params){
        $list= $this->repository->listPurchase($params); 
        $totalmount=0;    
        foreach($list as $item){
            $totalmount=$totalmount+$item->total;
        }
        $pagina = isset($params['pagina'])?$params['pagina']:0;
        
        if(!is_null($params) && $pagina>0){
            $params['count']=true;
            $total = sizeof($this->repository->listPurchase($params));                       
            $response=array(
                'list'=>$list,
                'xpagina'=>$params['xpagina'],                
                'pagina'=>$params['pagina'],
                'total'=>(int) $total,
            );            
        }else{
            $response = $list;
        }
        return $response;          
    }

    public function listSubtransactiondetails($params){
        $list= $this->repository->listSubtransactiondetails($params);        
        return $list;   
    }
    public function saveSale($params){            
        $subjectRepository=$this->repository->saveSale($params);
        return $subjectRepository;
    }
    public function saveSaleTicket($params){            
        $subjectRepository=$this->repository->saveSaleTicket($params);
        return $subjectRepository;
    }
    public function savePurchase($params){            
        $subjectRepository=$this->repository->savePurchase($params);
        return $subjectRepository;
    }
    public function delete($params){
        $subjectRepository=$this->repository->delete($params);
        return $subjectRepository;
    }
    public function changestatusdisbursement($params){
        $repository=$this->repository->changestatusdisbursement($params);
        return $repository;
    }
    public function savePaymentSchedule($params){            
        $subjectRepository=$this->repository->savePaymentSchedule($params);
        return $subjectRepository;
    }
    public function listCashbox($params){
        $list= $this->repository->listCashbox($params); 
        $pagina = isset($params['pagina'])?$params['pagina']:0;
        if(!is_null($params) && $pagina>0){
            $params['count']=true;
            $total = sizeof($this->repository->listCashbox($params));                       
            $response=array(
                'list'=>$list,
                'xpagina'=>$params['xpagina'],                
                'pagina'=>$params['pagina'],
                'total'=>(int) $total,
            );            
        }else{
            $response = $list;
        }
        return $response;          
    }
    public function saveCashbox($params){            
        $subjectRepository=$this->repository->saveCashbox($params);
        return $subjectRepository;
    }
    public function listExpenseAndPayment($params){
        
        $list= $this->repository->listExpenseAndPayment($params); 
        $pagina = isset($params['pagina'])?$params['pagina']:0;
        if(!is_null($params) && $pagina>0){
            $params['count']=true;
            $total = sizeof($this->repository->listExpenseAndPayment($params));                       
            $response=array(
                'list'=>$list,
                'xpagina'=>$params['xpagina'],                
                'pagina'=>$params['pagina'],
                'total'=>(int) $total,
            );            
        }else{
            $response = $list;
        }
        return $response;          
    }
    public function saveExpenseAndPayment($params){            
        $subjectRepository=$this->repository->saveExpenseAndPayment($params);
        return $subjectRepository;
    }
    public function saveDepositAndQualification($params){            
        $subjectRepository=$this->repository->saveDepositAndQualification($params);
        return $subjectRepository;
    }
}
