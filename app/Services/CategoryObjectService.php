<?php

namespace App\Services;
use App\Repositories\CategoryObjectRepository;

class CategoryObjectService
{
   
    private $repository;

    public function __construct(CategoryObjectRepository $repository){
            $this->repository=$repository;        
    }
    public function listcategory($params){
        $categoryRepository= $this->repository->listcategory($params);        
            return $categoryRepository;        
    }
    public function saveCategoryObject($params){                         
        $categoryObject=$this->repository->saveCategoryObject($params);    
    }
    public function delete($params){
    }
    
}
