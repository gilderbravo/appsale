<?php

namespace App\Services;

use App\Repositories\BrandObjectRepository;

class BrandObjectService
{   
    private $repository;
    public function __construct(BrandObjectRepository $repository){
            $this->repository=$repository;        
    }
    public function allBrandObject($params){
        $objectRepository= $this->repository->allBrandObject($params);        
            return $objectRepository;        
    }
    public function saveBrandObject($params){                         
            $book=$this->repository->saveBrandObject($params);    
    }
    public function deleteBrandObject($params){
        return $this->repository->deleteBrandObject($params);
    } 
}
