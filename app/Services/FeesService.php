<?php

namespace App\Services;
use App\Repositories\FeesRepository;

class FeesService
{
   
    private $repository;
    public function __construct(FeesRepository $repository){
        $this->repository=$repository;        
}


    public function listpaymentschedule($params){
        $list= $this->repository->listpaymentschedule($params); 
        $pagina = isset($params['pagina'])?$params['pagina']:0;
        if(!is_null($params) && $pagina>0){
            $params['count']=true;
            $total = sizeof($this->repository->listpaymentschedule($params));                       
            $response=array(
                'list'=>$list,
                'xpagina'=>$params['xpagina'],                
                'pagina'=>$params['pagina'],
                'total'=>(int) $total,
            );            
        }else{
            $response = $list;
        }
        return $response;   
    }
    public function listFeesClient($params){
        $list= $this->repository->listFeesClient($params); 
        $pagina = isset($params['pagina'])?$params['pagina']:0;
        if(!is_null($params) && $pagina>0){
            $params['count']=true;
            $total = sizeof($this->repository->listFeesClient($params));                       
            $response=array(
                'list'=>$list,
                'xpagina'=>$params['xpagina'],                
                'pagina'=>$params['pagina'],
                'total'=>(int) $total,
            );            
        }else{
            $response = $list;
        }
        return $response;   
    }
    public function saveFee($params){            
        $subjectRepository=$this->repository->saveFee($params);
    }
    public function saveFeeClose($params){            
        $subjectRepository=$this->repository->saveFeeClose($params);
    }
    public function saveCreditClose($params){            
        $subjectRepository=$this->repository->saveCreditClose($params);
    }
    public function removepayment($params){            
        $subjectRepository=$this->repository->removepayment($params);
    }
}
