<?php

namespace App\Services;

use App\Repositories\ObjectRepository;

class ObjectService
{   
    private $repository;
    public function __construct(ObjectRepository $repository){
            $this->repository=$repository;        
    }
    public function listObject($params){
        $objectRepository= $this->repository->listObject($params);        
            return $objectRepository;        
    }
    public function listObjectSale($params){
        $objectRepository= $this->repository->listObjectSale($params);        
            return $objectRepository;        
    }
    
    public function saveObject($params){
        $objectRepository=$this->repository->saveObject($params);
    }
    public function delete($params){
    }
    
}
