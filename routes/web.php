<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

// objects
Route::get('/object/index','ObjectController@index') ;
Route::post('object/saveObject', 'ObjectController@saveObject');
Route::post('object/listObject', 'ObjectController@listObject');
Route::post('object/listObjectSale', 'ObjectController@listObjectSale');
// Subjects
Route::get('/client/index','SubjectController@indexClient') ;
Route::get('/provider/index','SubjectController@indexProvider') ;
Route::post('subject/saveClient', 'SubjectController@saveClient');
Route::post('subject/listClient', 'SubjectController@listClient');
Route::post('subject/listProvider', 'SubjectController@listProvider');
Route::post('subject/updateUser', 'SubjectController@updateUser');
Route::post('subject/listIdentityDocument', 'SubjectController@listIdentityDocument');
Route::post('subject/searchdniruc', 'SubjectController@searchdniruc');

Route::get('/employeed/index','SubjectController@indexEmployeed') ;
Route::post('subject/saveEmployeed', 'SubjectController@saveEmployeed');
Route::post('subject/listEmployeed', 'SubjectController@listEmployeed');
Route::post('subject/listPositions', 'SubjectController@listPositions');
Route::post('subject/saveUser', 'SubjectController@saveUser');

// ventasa
Route::get('/sale/index','TransactionController@index') ;
Route::post('transaction/changestatusdisbursement', 'TransactionController@changestatusdisbursement');
Route::post('transaction/saveSale', 'TransactionController@saveSale');
Route::post('transaction/saveSaleTicket', 'TransactionController@saveSaleTicket');
Route::post('transaction/deletesale', 'TransactionController@deletesale');
Route::post('transaction/listSale', 'TransactionController@listSale');
Route::post('transaction/listSaleTicket', 'TransactionController@listSaleTicket');
Route::post('transaction/listSubtransactiondetails', 'TransactionController@listSubtransactiondetails');
Route::post('transaction/savePaymentSchedule', 'TransactionController@savePaymentSchedule');
Route::post('transaction/saveExpenseAndPayment', 'TransactionController@saveExpenseAndPayment');
Route::post('transaction/saveDepositAndQualification', 'TransactionController@saveDepositAndQualification');
Route::post('transaction/saveCashbox', 'TransactionController@saveCashbox');
Route::post('transaction/listCashbox', 'TransactionController@listCashbox');
Route::post('transaction/listExpenseAndPayment', 'TransactionController@listExpenseAndPayment');
Route::post('transaction/listPaymentSchedule', 'TransactionController@listPaymentSchedule');

Route::get('/credit/index', 'TransactionController@creditsale');
Route::get('/savingmoney/index', 'TransactionController@savingmoney');
Route::get('/fixedterm/index', 'TransactionController@fixedterm');

Route::get('/expense/index', 'TransactionController@indexExpense');
Route::get('/payment/index', 'TransactionController@indexPayment');
Route::get('/deposit/index', 'TransactionController@indexDeposit');
Route::get('/qualification/index', 'TransactionController@indexQualification');
Route::get('/cashbox/index', 'TransactionController@indexCashbox');

// compras
Route::get('/purchase/index','TransactionController@indexPurchase') ;
Route::post('transaction/savePurchase', 'TransactionController@savePurchase');
Route::post('transaction/listPurchase', 'TransactionController@listPurchase');

// presentation
Route::get('/presentationObjects/index','ObjectController@indexPresentation') ;
Route::post('object/savePresentationObjects', 'ObjectController@savePresentationObjects');
Route::post('object/listpresentation', 'ObjectController@listpresentation');
// propiedades
Route::get('/propertyObjects/index','ObjectController@indexProperty') ;
Route::post('object/savePropertyObjects', 'ObjectController@savePropertyObjects');
Route::post('object/listproperty', 'ObjectController@listproperty');
// categorias
Route::get('/categoryObjects/index','ObjectController@indexCategory') ;
Route::post('object/saveCategoryObjects', 'ObjectController@saveCategoryObjects');
Route::post('object/listcategory', 'ObjectController@listcategory');
// marcas
Route::get('/brandObject/index','ObjectController@indexBrand') ;
Route::post('object/allBrandObject', 'ObjectController@allBrandObject');
Route::post('object/saveBrandObject', 'ObjectController@saveBrandObject');
Route::post('object/deleteBrandObject', 'ObjectController@deleteBrandObject');
// modelos
Route::get('/objectModel/index','ObjectController@indexObjectModel') ;
Route::post('object/saveObjectModel', 'ObjectController@saveObjectModel');
Route::post('object/listObjectModel', 'ObjectController@listObjectModel');
// Fees
Route::get('/fees/index','FeesController@index') ;
Route::get('/feespayment/index','FeesController@indexfeespayment') ;
Route::get('/feespaymentclose/index','FeesController@indexfeespaymentclose') ;
Route::get('/feescreditclose/index','FeesController@indexfeescreditclose') ;

Route::post('fees/listpaymentschedule', 'FeesController@listpaymentschedule');
Route::post('fees/listFeesClient', 'FeesController@listFeesClient');
Route::post('fees/saveFee', 'FeesController@saveFee');
Route::post('fees/saveFeeClose', 'FeesController@saveFeeClose');
Route::post('fees/saveCreditClose', 'FeesController@saveCreditClose');

Route::post('fees/removepayment', 'FeesController@removepayment');


// Reports
Route::get('/reportpayments/index','ReportController@indexPayments') ;
Route::get('/reportcloses/index','ReportController@indeReportcloses') ;
Route::get('/balancesheet/index','ReportController@indexBalancesheet') ;
Route::post('reports/listReportpayments', 'ReportController@listReportpayments');
Route::post('reports/listReportTransactionCloses', 'ReportController@listReportTransactionCloses');
Route::post('reports/listCashboxSummary', 'ReportController@listCashboxSummary');
Route::post('reports/deletefee', 'ReportController@deletefee');
Route::post('reports/updateCreditReversal', 'ReportController@updateCreditReversal');



Route::group(['middleware' => 'auth'], function () {
    //    Route::get('/link1', function ()    {
//        // Uses Auth Middleware
//    });

    //Please do not remove this if you want adminlte:route and adminlte:link commands to works correctly.
    #adminlte_routes
});

